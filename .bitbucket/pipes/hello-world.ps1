
# comment here 
function Get-ConventionalCommits {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)] [string]$CommitRange, 
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string]$Path='.'
    )
    $typeMap = @{
        "feat" = 2
    
        "fix" = 3
        "style" = 3
        "refactor" = 3
        "perf" = 3
        "build" = 3
        "ci" = 3
        "config" = 3
    
        "docs" = 4
        "test" = 4
        "chore" = 4
    }

     $projectRoot =  $Path #Resolve-Path $Path | 
    #                     Select-String -Pattern "\bprojects[/\\][^/\\]+[/\\][^/\\]+([/\\]|$)" | 
    #                     Select-Object -ExpandProperty Matches -First 1 | 
    #                     Select-Object -ExpandProperty Value

    $commits = Invoke-Expression -Command "git log --format=`">>>%B<<<`" $CommitRange -- $projectRoot" |
        Out-String -OutVariable gitlog |
        Select-String -AllMatches -Pattern "(?smi)>>>(?<type>\w+)(\((?<scope>\w+)\))?(?<BreakingChange>!)?:\s(?<message>.*?)<<<" |
        Select-Object -ExpandProperty Matches | 
        ForEach-Object {New-Object PSObject -Property @{ 
            Type = $_.Groups["type"].Value
            Scope = $_.Groups["scope"].Value
            Message = $_.Groups["message"].Value.Trim()
            BreakingChange = (($_.Groups["BreakingChange"].Value -eq "!") -or ($_.Groups["message"].Value -match "BREAKING CHANGE"))
            BumpType = if ($null -ne $typeMap[$_.Groups["type"].Value]) { $typeMap[$_.Groups["type"].Value] } else { 4 }
            SkipCi = $_.Groups["message"].Value -match "\[skip ci\]"
        }}
   
    return $commits
}

function Get-HighestVersionFromGitTags {
    param ([Parameter(Mandatory)] [string]$tagPrefix)
    
    $tagPattern = "(?<hash>[\w\d]+)\s(?<tag>$($tagPrefix -replace "\-", "\-")v(?<version>(?<major>\d+)(\.(?<minor>\d+))?(\.(?<patch>\d+))?(\-(?<pre>[0-9A-Za-z\-\.]+))?(\+(?<build>[0-9A-Za-z\-\.]+))?))$"

    Write-Verbose "Searching for version tags with pattern: $tagPattern"
    
    git fetch origin 'refs/tags/*:refs/tags/*'
    $version = git tag --list --format '%(objectname) %(refname:short)' | 
                Select-String -Pattern $tagPattern |
                Select-Object -ExpandProperty Matches | 
                ForEach-Object {New-Object PSObject -Property @{ 
                    Major = [int]$_.Groups["major"].Value
                    Minor = [int]$_.Groups["minor"].Value
                    Patch = [int]$_.Groups["patch"].Value
                    Pre = $_.Groups["pre"].Value
                    Build = [int]$_.Groups["build"].Value
                    Version = $_.Groups["version"].Value
                    Hash = $_.Groups["hash"].Value
                    Tag = $_.Groups["tag"].Value
                }} |
                Sort-Object Major,Minor,Patch,Pre,Build |
                Select-Object -Last 1
                
    return $version
}

function Invoke-VersionBump {
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string[]]$Path='.', 
        [Parameter(Mandatory)] [string]$GitUsername,   
        [Switch]$Force=$False
    )

    $projectRoot = $Path
    $projectId = "testappcore" #Get-ProjectId $Path
    # if ([string]::IsNullOrEmpty($projectId)) {
    #     Write-Host "Unable to determine project id. Path must be a project folder."
    #     return 
    # }

    $projectTagPrefix = "$projectId-"

    Write-Host "Looking for latest version tag with prefix '$projectTagPrefix'..."
    $latestVersion = Get-HighestVersionFromGitTags $projectTagPrefix

    if ($null -eq $latestVersion) {
        Write-Host "No existing version tag found with prefix '$projectTagPrefix'. Defaulting to version 1.0.0"
        $newVersion = "1.0.0"
    } else {
        Write-Host "Found last version tag: $latestVersion"
        Write-Host "Analyzing commits since $($latestVersion.Hash) for '$projectRoot'..."

        $commits = Get-ConventionalCommits -Path "$projectRoot" -CommitRange "$($latestVersion.Hash)..HEAD"

        $releaseBumpType = 4
        $commits |
            ForEach-Object {             
                if ($_.BreakingChange) {
                    $releaseBumpType = 1
                } elseif ($_.SkipCi) {
                    # Skip this one for detecting version bump
                } elseif ($_.BumpType -lt $releaseBumpType) {
                    $releaseBumpType = $_.BumpType
                }
            }
    
        $bumpTypes = @{ 1 = "Major"; 2 = "Minor"; 3 = "Patch"; 4 = "Build"; }
        $bump = New-Object PSObject -Property @{
            Ordinal = $releaseBumpType
            Type = $bumpTypes[$releaseBumpType]
        }

        Write-Host "Conventional commits indicate $($bump.Type) version bump is required."

        if ($bump.Ordinal -le 1) { 
            $versionParts = ($latestVersion.Major+1),0,0
        } elseif ($bump.Ordinal -le 2) {
            $versionParts = $latestVersion.Major,($latestVersion.Minor+1),0
        } elseif (($bump.Ordinal -le 3) -or $Force) {
            $versionParts = $latestVersion.Major,$latestVersion.Minor,($latestVersion.Patch+1)
        } else {
            #$versionParts = $latestVersion.Major,$latestVersion.Minor,$latestVersion.Patch
            Write-Host "Tagging is disabled for Build level version bump. Skipping."
            return
        }

        $newVersion = $versionParts -join "."
    }
    $newVersionTag = "${projectTagPrefix}v${newVersion}"

    # Set-ProjectVersion -Path $Path -Version "$newVersion" -WhatIf:$WhatIfPreference
    # Add-ChangelogEntry -Path $Path -Version "$newVersion" -Commits $commits -WhatIf:$WhatIfPreference

    if ($PSCmdlet.ShouldProcess("Committing version update and tagging release $newVersionTag", "GIT", "commit and tag")) {
        Write-Host "Committing version update and tagging release $newVersionTag"
        git config user.name $GitUsername
        git config user.email "$($GitUsername)@wrbts.ads.wrberkley.com"
        Write-Host "$(git config user.name) - $(git config user.email)"
        git pull
        git tag $newVersionTag
        git push --tags
        git add -A
        git commit -am "chore(release): $projectRoot $newVersion [skip ci]"
        git push
    }
}

$owner = $Env:BITBUCKET_REPO_OWNER
Write-Host "Hello $owner"

$Env:PROJECT_PATH = "TestAppCore"
$projectPath = $Env:PROJECT_PATH
Write-Host "Project name $projectPath"

Invoke-VersionBump -Path $projectPath -GitUsername "myguy" -WhatIf



    
