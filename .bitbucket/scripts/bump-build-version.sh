

getNextVersion() {
    source .bitbucket/scripts/functions.sh 
    local __result=$1
    
    projectPath=$projectPath getProjectNameFromPath prefixId  
    tagPrefix=$(echo $prefixId | tr -d '.' | tr [:upper:] [:lower:])- getLatestVersion lastestVersion    
    
    if [[ ! $lastestVersion ]]
    then
        echo "No latest version means no tags, new version is 1.0.0"
        local __newVersion=1.0.0
        eval $__result="'$__newVersion'"
        return 0;        
    else
        version=$lastestVersion getHashFromVersion hashValue
        echo "Hash value $hashValue"
        commitRange="$hashValue..HEAD" projectPath=$projectPath getConventialCommits commitLevel
        echo "Commit Level $commitLevel"
    fi
    local versionPattern="[[:alnum:]]+-v([[:digit:]]+)\.([[:digit:]]+)\.([[:digit:]]+).*"
    if [[ $lastestVersion =~ $versionPattern ]]
    then 
        local major=${BASH_REMATCH[1]}
        local minor=${BASH_REMATCH[2]}
        local patch=${BASH_REMATCH[3]}
    fi

    case $commitLevel in 
        1)
            major=$(( $major + 1 ))
            ;;
        2)
            minor=$(( $minor + 1 ))
            ;;
        3)
            patch=$(( $patch + 1 ))
            ;;
        *)
            echo "No version bump"
    esac
    local __newVersion="${major}.${minor}.${patch}"
    eval $__result="'$__newVersion'"
}
