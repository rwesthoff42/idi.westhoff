getLatestVersion() {
    echo "Tag prefix in function $tagPrefix"
    local tagPattern="(${tagPrefix/"\-"/"\-"}v(([[:digit:]]+)(\.([[:digit:]]+))?(\.([[:digit:]]+))?(\-([0-9A-Za-z\-\.]+))?(\+([0-9A-Za-z\-\.]+))?))\|([[:alnum:]]+)$"

    echo "Searching for version tags with pattern: $tagPattern"        
    git fetch origin 'refs/tags/*:refs/tags/*' 
    local tags=$(git tag --list --format '%(refname:short)|%(objectname)')
    local matchedTags=()   

    for t in $tags
    do     
        if [[ $t =~ $tagPattern ]]
        then 
            echo 'Match'
            # matchedTags+=(${t})   
            matchedTags+=(${BASH_REMATCH[0]})   
        fi
    done

    IFS=$'\n' 
    sortedTags=($(sort <<<"${matchedTags[*]}"))
    unset IFS

    if [[ ${#sortedTags[@]} -gt 0 ]]
    then
        local __version="${sortedTags[-1]}"
    fi

    local __localResult=$1
    eval $__localResult="'$__version'"
}

getHashFromVersion() {
    echo "version in function $version"
    [[ ! $version ]] && echo "no version" && return 1;
    local pattern="[[:alnum:]]+\|([[:alnum:]]+)"
    [[ $version =~ $pattern ]] && local __result="${BASH_REMATCH[1]}"
    local __hash=$1
    eval $__hash="'$__result'"
}

getProjectNameFromPath() {
    local __result=$1
    local pattern="projects[/\\][^/\\]+[/\\]([^/\\]+)"
    if [[ $projectPath =~ $pattern ]]
    then
        local __match="${BASH_REMATCH[1]}"
    fi
    eval $__result="'$__match'"
}

getConventialCommits() {
    [[ ! $commitRange ]] && echo "commitRange is required" && return 1;
    [[ ! $projectPath ]] && echo "projectPath is required" && return 1;
    declare -A typeMap=(
        [feat]=2          
        [fix]=3
        [style]=3
        [refactor]=3
        [perf]=3
        [build]=3
        [ci]=3
        [config]=3    
        [docs]=4
        [test]=4
        [chore]=4      
    )

    # group1 = type, group3 = scope, group4 = breakingChange, group5 = message 
    local pattern=">>>([[:alpha:]]+)(\(([[:alpha:]]+)\))*(!)*:[[:space:]](.*)<<<"
    echo "Matching commits with $pattern"
    local commits=$(git log --format=">>>%B<<<" $commitRange -- $projectPath | tr '\n' '|' | sed -e "s/|<+/</g" -e "s/<|/</g" -e "s/|</</g" -e "s/|/ /g" -e "s/<<</<<<\n/g")
    local __bumpLevel=4
    IFS=$'\n'
    for c in $commits
    do 
        if [[ $c =~ $pattern ]]
        then
            [[ ${BASH_REMATCH[4]} ]] && echo "found breaking change" && __bumpLevel=1 && break;
            local fixType=${BASH_REMATCH[1]}
            # echo "matched commit $fixType"
            local __commitLevel=${typeMap[$fixType]}
            # echo "commit level $__commitLevel"
            [[ $__commitLevel -lt $__bumpLevel ]] && __bumpLevel=$__commitLevel;       
        fi
    done
    unset IFS
    echo "Bump level $__bumpLevel"
    local __result=$1
    eval $__result="'$__bumpLevel'"
}