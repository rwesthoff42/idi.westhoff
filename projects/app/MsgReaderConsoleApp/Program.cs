﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using static MsgReader.Outlook.Storage;

namespace MsgReaderConsoleApp
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {///hi///
            Console.WriteLine("Getting XML");
            var destination = @"C:\Users\rwesthoff\Documents\McDonalds\SampleXML_2022";

            var files = Directory.GetFiles(@"C:\users\rwesthoff\downloads\Test Package Apps - 11.18.2021", "*.msg");
            var info = Directory.CreateDirectory(destination);
            foreach (var file in files)
            {
                using (var msg = new Message(file))
                {
                    var attachments = msg.Attachments.Cast<Attachment>();
                    var xmlAttachment = attachments.First(a => a.FileName.Contains(".xml"));

                    using (var ms = new MemoryStream(xmlAttachment.Data))
                    using (var reader = new StreamReader(ms))
                    {
                        var xml = reader.ReadToEnd();
                        XDocument doc = XDocument.Parse(xml);
                        var bytes = Encoding.UTF8.GetBytes(doc.ToString());
                        using (var stream = new FileStream(Path.Combine(destination, xmlAttachment.FileName), FileMode.OpenOrCreate))
                        {
                            await stream.WriteAsync(bytes);
                            await stream.FlushAsync();
                        }
                    }
                }
            }

            Console.Write("DONE!!");
        }
    }
}
