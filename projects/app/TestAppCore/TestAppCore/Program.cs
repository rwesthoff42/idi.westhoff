﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace TestAppCore
{
    class Program
    {
        private static string FormatDefaultEmail(string defaultEmail, long id)
        {
            var index = defaultEmail.IndexOf("@") > 0 ? defaultEmail.IndexOf("@") : 0;
            var usePlus = defaultEmail.IndexOf("+") == -1;
            return defaultEmail.Insert(index, $"{(usePlus ? "+" : "")}{id}");
        }

        private static bool MissingInsurableValues(string buildingValue, string contentsValue)
        {
            return (!int.TryParse(buildingValue, out var building) || building <= 0)
                && (!int.TryParse(contentsValue, out var contents) || contents <= 0);
        }

        private static string MD5Hash(string input)
        {
            // var hash = MD5Hash($"{mapping.OperatorEid}office{parts[0]}{parts[1]}".ToLower().Replace(" ", ""));
            using var md5 = MD5.Create();
            var hashBytes = md5.ComputeHash(Encoding.ASCII.GetBytes(input));

            // Convert the byte array to hexadecimal string
            var sb = new StringBuilder();
            foreach (var t in hashBytes)
            {
                sb.Append(t.ToString("X2"));
            }
            return sb.ToString();
        }

        static async Task Main(string[] args)
        {

            // var phone = "00000001987654321";
            // Console.WriteLine(phone[^10..^0]);

            // var middle = "company";
            // Console.WriteLine(middle[..1]);


            //Console.WriteLine(MD5Hash("e0016392warehouse344east"));
            //Console.WriteLine(Math.Round(12.3654841, 4));
            //Console.WriteLine(FormatDefaultEmail("idi_development@gmail.com", 12325465));
            //Console.WriteLine(FormatDefaultEmail("idi_development+bridge@gmail.com", 12325465));

            //Console.WriteLine(System.Web.HttpUtility.UrlEncode(FormatDefaultEmail("idi_development@gmail.com", 12325465)));

            //Console.WriteLine(MissingInsurableValues(null, null));
            //Console.WriteLine(MissingInsurableValues("0", "0"));
            //Console.WriteLine(MissingInsurableValues("0", null));
            //Console.WriteLine(MissingInsurableValues(null, "0"));
            //Console.WriteLine(MissingInsurableValues("50", null));
            //Console.WriteLine(MissingInsurableValues(null, "50"));
            //Console.WriteLine(MissingInsurableValues("0", "50"));
            //Console.WriteLine(MissingInsurableValues("50", "0"));
            //Console.WriteLine(MissingInsurableValues("50", "50"));


            //var uri = new Uri(
            //     new Uri("http://something.intrepid.com/////"),
            //    $"/api/document/65146516351/access-key/6986916516351").ToString();

            //Console.WriteLine(uri);

            //var uri2 = new Uri(
            //     new Uri("http://something.intrepid.com"),
            //    $"/api/document/65146516351/access-key/6986916516351").ToString();

            //Console.WriteLine(uri2);

            //DateTime? effectiveDate = null;
            //Console.WriteLine(effectiveDate.HasValue ? effectiveDate.Value.ToString("s") : DateTime.Now.Date.ToString("s"));
            //effectiveDate = new DateTime(2022, 3, 1);
            //Console.WriteLine(effectiveDate.HasValue ? effectiveDate.Value.ToString("s") : DateTime.Now.Date.ToString("s"));


            //var defaultEmail = "idi_developmentgmail.com"; 
            //var index = defaultEmail.IndexOf("@") > 0 ? defaultEmail.IndexOf("@") : 0;
            //var newEmail = defaultEmail.Insert(index, $"+{Guid.NewGuid().ToString("N")}");
            //Console.WriteLine(newEmail);
            //var runs = 50;

            //var timing = new AsyncTiming();

            //await timing.TestInternal(runs);

            //await timing.TestExternal(runs);

            //for (int i = 0; i < 100; i++)
            //{
            //    Console.WriteLine(DateTime.Now.ToString().GetHashCode().ToString("x"));
            //}

            //var value = new AsyncTiming().testInt;
            //var listOfStrings = new List<string> { "23", "455564", "abc" };
            //var policyholderIds = listOfStrings.Select(s =>
            //{
            //    int.TryParse(s, out var id);
            //    return id;
            //}).Distinct().ToList();

            //IEnumerable<string> x = new List<string>() as IEnumerable<string>;
            //x.ToList();
            //Console.WriteLine(string.Join(" | ", policyholderIds));
            //var baseUrl = "https://devmarketapi.wrbts.ads.wrberkley.com";


            //var builder = new UriBuilder();
            //builder.Host = baseUrl;
            //builder.Path = "ping";

            //Console.WriteLine(builder.Uri);
            //Uri.TryCreate(new Uri(baseUrl), "ping", out Uri result);
            //Console.WriteLine(new Uri(baseUrl));
            //var builder2 = new UriBuilder();
            //builder2.Host = baseUrl + "/";
            //builder2.Path = "ping";

            //Console.WriteLine(builder2.Uri);

            //Console.WriteLine(new Uri(Path.Combine(baseUrl, "ping")));
            //Console.WriteLine(new Uri(Path.Combine(baseUrl + "/", "ping")));


            //McDonalds
            //new McDXmlQuestionInsert().CreateInsertStatements();
            //new McDonaldsXml().Validate();
            //new McDonaldsXml().CreatePostmanJsonDataFile();

            //Console.ReadKey();

            //            var json = @"{
            //        ""52075"": {
            //            ""Roles"": [
            //                ""user"",
            //                ""admin""
            //            ]
            //        },
            //        ""90210"": {
            //            ""Roles"": [
            //                ""billing"",
            //                ""admin""
            //            ]
            //    },
            //        ""roles"": [
            //            ""idi_employee""
            //        ],
            //        ""cfam"": {
            //            ""roles"": [
            //                ""admin""
            //            ]
            //}
            //    }";
            //            var serializerSettings = new JsonSerializerSettings
            //            {
            //                Error = (obj, args) => { args.ErrorContext.Handled = args.ErrorContext.Path == "roles"; }
            //            };
            //            var roles = JsonConvert.DeserializeObject<Dictionary<string, AccountAccessClaimValueModel>>(((object)json).ToString(), serializerSettings);


            //var testList = new List<String>
            //{
            //    "thing1",
            //    null 
            //};

            //testList.ForEach(s =>
            //{
            //    var char1 = s?[..1];

            //    switch (char1)
            //    {
            //        case "t":
            //            System.Diagnostics.Debug.WriteLine($"hit case 't' {s}");
            //            break;
            //        default:
            //            System.Diagnostics.Debug.WriteLine($"hit default case 't' {s}");

            //            break;
            //    }
            //});
        }
    }



    public class AccountAccessClaimValueModel
    {
        public List<string> Roles { get; set; }
    }
}