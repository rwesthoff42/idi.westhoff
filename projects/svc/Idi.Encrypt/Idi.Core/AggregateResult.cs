﻿using System.Collections.Generic;
using System.Linq;

namespace Idi.Core
{
    public enum AggregateResultState { AllSucceeded, AllFailed, PartialFailure }

    /// <summary>
    /// Use this class as a return value for functions that perform batch operations -- example, updating multiple tables, where the function wants to continue when an error occurs
    /// </summary>
    public class AggregateResult<TResult>
    {
        public AggregateResultState OperationState
        {
            get
            {
                var successCount = SuccessResults.Count();

                if (successCount == AllResults.Count)
                    return AggregateResultState.AllSucceeded; // assumes that zero results == success

                return successCount == 0 ? AggregateResultState.AllFailed : AggregateResultState.PartialFailure;
            }
        }

        public List<OperationResult<TResult>> AllResults { get; } = new List<OperationResult<TResult>>();

        public IEnumerable<OperationResult<TResult>> FailureResults => AllResults.Where(r => !r.Succeeded);
        public IEnumerable<OperationResult<TResult>> SuccessResults => AllResults.Where(r => r.Succeeded);
    }
}
