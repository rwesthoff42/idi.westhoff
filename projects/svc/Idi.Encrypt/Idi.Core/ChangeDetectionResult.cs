﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Idi.Core
{
    public class ChangeDetectionResult<T>
    {
        public T Result { get; set; }

        public bool HasChanged { get; set; }
    }
}
