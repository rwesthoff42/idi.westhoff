﻿using System;

namespace Idi.Core.DateUtilities
{
    public static class DateUtility
    {
        /// <summary>
        /// Converts to Date from a 7-digit Julian date.
        /// Does not support Julian Dates with the 2 digit year format
        /// Example: Convert 2021001 to a DateTime of Jan 1, 2021
        /// Example: Convert 2021033 to a DateTime of Feb 2, 2021
        /// </summary>
        /// <param name="julianDate"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Thrown when the expected 7 digits are not present.</exception>
        public static DateTime ToDateFromJulianDate(int julianDate)
        {
            var julianString = julianDate.ToString();
            if (julianString.Length != 7)
            {
                throw new ArgumentException($"Julian Date Format Error - Expected 7 digits. Example format: 2021001 (Jan 1, 2021) - got this: { julianString }. Note: Does not support Julian Dates with the 2 digit year format.");
            }
            var year = Convert.ToInt32(julianString.Substring(0, 4));
            var dayOfYear = Convert.ToInt32(julianString.Substring(4, 3).TrimStart('0'));
            return new DateTime(year, 1, 1).AddDays(dayOfYear - 1);
        }

        /// <summary>
        /// This converts a datetime to a Julian Date with a 4 digit year.
        /// Does not support a 2 digit year format.
        /// Example: Convert a DateTime of Jan 1, 2021 to 2021001
        /// Example: Convert a DateTime of Feb 2, 2021 to 2021033
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static int ToJulianDate(this DateTime dateTime)
        {
            return Convert.ToInt32($"{dateTime.Year}{dateTime.DayOfYear.ToString("000")}");
        }
    }
}