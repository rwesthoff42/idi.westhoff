﻿namespace Idi.Core.Encryption
{
    public enum EncryptionFormat
    {
        NineDigitNationalIdentifierWithLastFourDigitsInClearText = 0,
        AlphaNumeric = 1,
        Numeric = 2,
        ShortDate = 3,
        JulianDate = 4,
        Currency = 5,
        CreditCard = 6
    }
}