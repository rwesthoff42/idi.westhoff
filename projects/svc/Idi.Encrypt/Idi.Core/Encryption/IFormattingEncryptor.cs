﻿namespace Idi.Core.Encryption
{
    public interface IFormattingEncryptor
    {
        T Encrypt<T>(T value, EncryptionFormat format);
        T Decrypt<T>(T value, EncryptionFormat format);
    }
}