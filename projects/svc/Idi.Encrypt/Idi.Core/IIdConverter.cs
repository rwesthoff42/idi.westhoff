﻿using System.Security.Cryptography.X509Certificates;

namespace Idi.Core
{
    public interface IIdConverter
    {
        string Encrypt(int id);
        string Encrypt(int? id);
        int Decrypt(string id);
        long Decryptlong(string id);
        int? DecryptNullable(string id);
    }
}