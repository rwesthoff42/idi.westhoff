﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Idi.Core.Logging
{
    /// <summary>
    /// Represents a    
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public sealed class ActivityScope : IDisposable
    {
        /// <summary>
        /// Provides a separate instance of Stack of ActivityScope
        /// </summary>
        private static readonly AsyncLocal<Stack<ActivityScope>> ActivityStackAsyncLocal = new AsyncLocal<Stack<ActivityScope>>();

        private static Stack<ActivityScope> ActivityStack
        {
            get
            {
                if (ActivityStackAsyncLocal.Value == null)
                    return ActivityStackAsyncLocal.Value = new Stack<ActivityScope>();

                return ActivityStackAsyncLocal.Value;
            }
        }

        /// <summary>
        /// Creates a new scope, forcing a specific parent Id
        /// You probably want to use <see cref="StartNew"/> instead of this method, unless the parent scope
        /// does not exist.  You probably want this method if you are creating a scope where the parent was created
        /// on the other end of an inter-process call or web service call.
        ///  </summary>
        /// <returns></returns>
        public static ActivityScope Restore(string id, string parentId)
        {
            var scope = new ActivityScope(id, parentId);
            ActivityStack.Push(scope);
            return scope;
        }

        /// <summary>
        /// Starts a new scope.  Make sure to either enclose the scope in using statement, or Dispose it when you are finished.
        /// </summary>
        /// <returns></returns>
        /// <remarks>You might wonder why a factory and not a ctor.  </remarks>
        public static ActivityScope StartNew()
        {
            string parentId = Current?.Id;
            return Restore(Guid.NewGuid().ToString(), parentId);
        }

        /// <summary>
        /// Gets the current Activity or null if no scope has been defined.
        /// </summary>
        public static ActivityScope Current
        {
            get
            {
                if (ActivityStackAsyncLocal.Value != null && ActivityStackAsyncLocal.Value.Count > 0)
                    return ActivityStackAsyncLocal.Value.Peek();
                return null;
            }
        }

        internal ActivityScope(string id, string parentId)
        {
            Id = id;
            ParentId = parentId;
        }

        /// <summary>
        /// Returns the Guid id that was created for this scope
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Returns the Guid id of this scope's immediate parent, or null if there is no parent.
        /// </summary>
        public string ParentId { get; private set; }

        /// <summary>
        /// Ends the scope.  If there are child scopes, they are released as well
        /// </summary>
        public void Dispose()
        {
            if (Current == null)
                return;

            // validate that the stack contains this scope 
            if (ActivityStack.All(s => s.Id != this.Id))
                return;

            // pop this scope and any children below it
            ActivityScope currentScope = ActivityStack.Pop();
            while (ActivityStack.Count > 0 && currentScope.Id != this.Id)
                currentScope = ActivityStack.Pop();
        }
    }
}
