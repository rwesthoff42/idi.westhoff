using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;

namespace Idi.Core
{
    public class OperationResult
    {
        public bool Succeeded { get; set; }

        /// <summary>
        /// Optional message to go with the result.  Can be null.  If the operation was unsuccessful, and there is no exception, this should be the error message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// If the failure was an exception, add the exception instance here, otherwise leave it null.
        /// </summary>
        public Exception Error { get; set; }
    }

    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed. Suppression is OK here.")]
    public class OperationResult<T> : OperationResult
    {
        /// <summary>
        /// Contains the results of a successful call.  Can be null
        /// </summary>
        public T Result { get; set; }

        public OperationResult() { }

        public OperationResult(OperationResult copyFrom, T result)
        {
            foreach (var propInf in copyFrom.GetType().GetProperties())
            {
                propInf.SetValue(this, propInf.GetValue(copyFrom));
            }
            Result = result;
        }
    }
}