using System.Threading.Tasks;

namespace Idi.Core.Rules
{
    /// <summary>
    /// Represents a rule that can be evaluated on a data element.  
    /// </summary>
    /// <typeparam name="TSubject">The type of data element that is being evaluated.</typeparam>
    public interface IRule<TSubject>
    {
        Task<RuleResult> EvaluateAsync(TSubject subject);
    }
}