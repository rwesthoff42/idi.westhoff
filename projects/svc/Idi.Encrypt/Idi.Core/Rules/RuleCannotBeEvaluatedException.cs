﻿using System;
using System.Runtime.Serialization;

namespace Idi.Core.Rules
{
    /// <summary>
    /// Thrown when a <see cref="IRule{TSubject}"/> is unable to evaluate because necessary conditions are not met (so the rule is not able to indicate pass or fail)
    /// </summary>
    public class RuleCannotBeEvaluatedException: Exception
    {
        public RuleCannotBeEvaluatedException(string message) : base(message) { }
        public RuleCannotBeEvaluatedException() { }
        public RuleCannotBeEvaluatedException(string message, Exception innerException) : base(message, innerException) { }
        protected RuleCannotBeEvaluatedException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
