namespace Idi.Core.Rules
{
    public enum RuleOutcome
    {
        /// <summary> The rule did not run </summary>
        NotExecuted,

        /// <summary> 
        /// The rule executed, but the data did not meet the rule criteria, so the rule is not considered triggered/signaled.  
        /// For validation rules, this means that the element passed the validation because the rule did not signal a failure.
        /// For workflows, this means that the 
        ///  </summary>
        NotSignaled,

        /// <summary>
        /// The rule exeucted, and the data met the rules conditions.  
        /// For validation rules, this means the data failed validation because the rule was searching for a condition and it found that condition.
        /// </summary>
        Signaled
    }
}