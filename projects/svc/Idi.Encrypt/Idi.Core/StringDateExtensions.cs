﻿using System;

namespace Idi.Core
{
    public static class StringDateExtensions
    {
        public static bool IsEquivalentToDate(this string stringDate, DateTime? compareDate)
        {
            if (compareDate.HasValue && DateTime.TryParse(stringDate, out DateTime date))
            {
                return date == compareDate.Value;
            }

            return false;
        }
    }
}