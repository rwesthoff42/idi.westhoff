﻿namespace Idi.Core.Validation
{
    public class ValidationMessage
    {
        /// <summary>
        /// Id of the item that validated the rule, or null if not applicable
        /// </summary>
        public int? Id { get; set; }

        public string RuleName { get; set; }
        public string Message { get; set; }

        public override string ToString() => $"Item Id: {Id} Rule Name: {RuleName} Message: {Message}";
    }
}