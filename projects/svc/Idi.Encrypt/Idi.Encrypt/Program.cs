﻿using Idi.Core;
using Idi.Core.Encryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Idi.Encrypt
{///
    class Program
    {//
        static void Main(string[] args)
        {
            var utility = new EncryptionUtility();
            var converter = new SessionEncryptingIdConverter(utility);

            //var decrypted = converter.Decryptlong("U18aT73jt2SQH5ScoFquAMbtqcSLiKBi0jIpu9f8x5qklKzHwhoosQdXlj2P05ed");
            //Console.WriteLine(decrypted);

          
            var x = converter.Decrypt("V5Ntj5evvBdtUNtDIMXsWg");
            Console.WriteLine(x);
            //var encrypted = converter.Encrypt(3845);
            //Console.WriteLine(encrypted);

            //var encrypted2 = converter.Encrypt(7460);
            //Console.WriteLine(encrypted2);

            Console.ReadKey();

        }
    }
}
