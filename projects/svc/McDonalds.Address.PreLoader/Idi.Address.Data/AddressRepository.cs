﻿using Idi.Mcdonalds.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Idi.Address.Data
{
    public class AddressRepository
    {
        private readonly AddressContext _context;

        public AddressRepository(AddressContext context)
        {
            _context = context;
        }

        public async Task<bool> AddFloodDataAsync(long addressId, FloodMapping mapping)
        {
            var floodZone = new FloodZone
            {
                AddressId = addressId,
                CreatedBy = "rwesthoff-manual",
                CreatedDate = DateTime.UtcNow,
                MaxFloodZone = mapping.FloodZoneFixed,
                MinimumDistance = mapping.MinDist,
                Response = mapping.JsonTxt
            };
            await _context.AddAsync(floodZone);

            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> AddBurManualRecordAsync(long addressId)
        {
            var address = await _context.Addresses
                .Include(a => a.PropertySearchResults)
                .FirstOrDefaultAsync(a => a.Id == addressId);

            var defaultMcdBurData = new BurEnrichment
            {
                PropertySearchResultId = address.PropertySearchResults.ToList().First().Id,
                Bceg = "4",
                ConstructionType = "2",
                GroupIiSymbol = address.StateCode == "MS" ? "M" : "A",
                NumberOfStories = 1,
                ReportType = "MANUAL",
                Sprinkler = false,
                SquareFootage = 2000,
                SurveyDate = null,
                CreatedBy = "initial_load",
                CreatedDate = DateTime.UtcNow
            };

            await _context.AddAsync(defaultMcdBurData);
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<List<Address>> GetAddressesAsync()
        {
            return await _context.Addresses
                .Include(a => a.FloodZones)
                .ThenInclude(f => f.FloodZoneJson)
                .Include(a => a.PropertyEnrichments)
                .Include(a => a.PropertyRisks)
                .ThenInclude(a => a.PropertyRiskPerils)
                .Include(a => a.PropertySearchResults)
                .ThenInclude(p => p.LcrEnrichments)
                .Include(a => a.PropertySearchResults).ThenInclude(p => p.BurEnrichments)
                .Include(a => a.PropertySearchResults).ThenInclude(p => p.PropertyOccupants)
                .ToListAsync();
        }

        public async Task<bool> AddAddress(Address address)
        {
            var entity = _context.Addresses.Add(address);

            return await SaveAsync();
        }

        public async Task<bool> SaveAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<List<FloodZone>> GetFloodZones()
        {
            return await _context.FloodZones.Include(f => f.FloodZoneJson).ToListAsync();
        }

        public async Task<List<FloodZoneMapping>> GetFloodZoneMappings()
        {
            return await _context.FloodZoneMappings.ToListAsync();
        }

        public async Task<int> UpdateFloodZones(List<FloodZone> floodZones)
        {
            _context.UpdateRange(floodZones);
            return await _context.SaveChangesAsync();
        }
    }
}
