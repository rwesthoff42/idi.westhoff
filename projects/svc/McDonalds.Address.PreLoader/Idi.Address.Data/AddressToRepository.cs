﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Idi.Address.Data
{
    public class AddressToRepository
    { 
    
        private readonly AddressToContext _context;

        public AddressToRepository(AddressToContext context)
        {
            _context = context;
        }

       
        public async Task<List<Address>> GetAddressesAsync()
        {
            return await _context.Addresses.ToListAsync();
        }

        public async Task<bool> AddAddress(Address address)
        {
            var entity = _context.Addresses.Add(address);

            return await SaveAsync();
        }

        public async Task<bool> SaveAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<Address> FindByHash(string hash)
        {
            return await _context.Addresses.FirstOrDefaultAsync(a => a.Hash == hash);
        }

        public async Task<Address> GetById(long id)
        {
            return await _context.Addresses.FirstOrDefaultAsync(a => a.Id == id);
        }
    }
}
