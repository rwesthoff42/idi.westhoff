# run this script to scaffold the dbcontext and entity Models
# set config in ./efConfig.Json

# prerquisites to run 
# Use Nuget managaer or commands below
# --ef cli tool 
# dotnet tool update --global dotnet-ef

# --MS design package
# dotnet add package Microsoft.EntityFrameworkCore.Design



$appsettings = Get-Content -Raw -Path "$PSScriptRoot/efConfig.json"

$config = ConvertFrom-Json -InputObject $appsettings

Write-Host "Running dotnet ef scaffold"
Write-Host ""

# run the scaffold
dotnet ef dbcontext scaffold $config.ConnectionString Microsoft.EntityFrameworkCore.SqlServer --project $PSScriptRoot -o GeneratedContext --context $config.ContextName --no-onconfiguring --namespace $config.Namespace --force