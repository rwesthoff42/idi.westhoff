﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Idi.Address.Data
{
    public partial class AddressContext : DbContext, IAddressContext
    {
        public AddressContext()
        {
        }

        public AddressContext(DbContextOptions<AddressContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<BurEnrichment> BurEnrichments { get; set; }
        public virtual DbSet<FloodZone> FloodZones { get; set; }
        public virtual DbSet<FloodZoneJson> FloodZoneJsons { get; set; }
        public virtual DbSet<FloodZoneMapping> FloodZoneMappings { get; set; }
        public virtual DbSet<JudicialProfileMapping> JudicialProfileMappings { get; set; }
        public virtual DbSet<LcrEnrichment> LcrEnrichments { get; set; }
        public virtual DbSet<PropertyEnrichment> PropertyEnrichments { get; set; }
        public virtual DbSet<PropertyOccupant> PropertyOccupants { get; set; }
        public virtual DbSet<PropertyRisk> PropertyRisks { get; set; }
        public virtual DbSet<PropertyRiskPeril> PropertyRiskPerils { get; set; }
        public virtual DbSet<PropertySearchResult> PropertySearchResults { get; set; }
        public virtual DbSet<RegionMapping> RegionMappings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("WRBTS\\rwesthoff")
                .HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Address>(entity =>
            {
                entity.ToTable("Address", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AddressLine1)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.County)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FipsCountyCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Hash)
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.Latitude).HasColumnType("decimal(9, 6)");

                entity.Property(e => e.Longitude).HasColumnType("decimal(9, 6)");

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StateCode)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ZipCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BurEnrichment>(entity =>
            {
                entity.ToTable("BurEnrichment", "dbo");

                entity.Property(e => e.Bceg)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ConstructionType)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GroupIiSymbol)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReportType)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SquareFootage).HasColumnType("decimal(12, 2)");

                entity.Property(e => e.SurveyDate).HasColumnType("date");

                entity.HasOne(d => d.PropertySearchResult)
                    .WithMany(p => p.BurEnrichments)
                    .HasForeignKey(d => d.PropertySearchResultId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BurEnrichment_PropertySearchResult");
            });

            modelBuilder.Entity<FloodZone>(entity =>
            {
                entity.ToTable("FloodZone", "dbo");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MaxFloodZone)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.MinimumDistance).HasColumnType("decimal(38, 20)");

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Response).IsUnicode(false);

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.FloodZones)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FloodZone_Address");
            });

            modelBuilder.Entity<FloodZoneJson>(entity =>
            {
                entity.HasKey(e => e.FloodZoneId);

                entity.ToTable("FloodZoneJson", "dbo");

                entity.Property(e => e.FloodZoneId).ValueGeneratedNever();

                entity.Property(e => e.Json).IsUnicode(false);

                entity.HasOne(d => d.FloodZone)
                    .WithOne(p => p.FloodZoneJson)
                    .HasForeignKey<FloodZoneJson>(d => d.FloodZoneId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FloodZoneJson_FloodZone");
            });

            modelBuilder.Entity<FloodZoneMapping>(entity =>
            {
                entity.ToTable("FloodZoneMapping", "dbo");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FloodZone)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.FloodZoneShort)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<JudicialProfileMapping>(entity =>
            {
                entity.ToTable("JudicialProfileMapping", "dbo");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Profile)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.StateCode)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ZipCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LcrEnrichment>(entity =>
            {
                entity.ToTable("LcrEnrichment", "dbo");

                entity.Property(e => e.BuildingBg1).HasColumnType("decimal(6, 4)");

                entity.Property(e => e.BuildingBg2).HasColumnType("decimal(6, 4)");

                entity.Property(e => e.BuildingCauseOfLoss).HasColumnType("decimal(6, 4)");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PersonalPropertyBg1).HasColumnType("decimal(6, 4)");

                entity.Property(e => e.PersonalPropertyBg2).HasColumnType("decimal(6, 4)");

                entity.Property(e => e.PersonalPropertyCauseOfLoss).HasColumnType("decimal(6, 4)");

                entity.Property(e => e.PropertyRatingType)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ReportType)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.PropertySearchResult)
                    .WithMany(p => p.LcrEnrichments)
                    .HasForeignKey(d => d.PropertySearchResultId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LcrEnrichment_PropertySearchResult");
            });

            modelBuilder.Entity<PropertyEnrichment>(entity =>
            {
                entity.ToTable("PropertyEnrichment", "dbo");

                entity.Property(e => e.ConfidenceLevel)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GroupIiZone)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProtectionClass)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TerritoryCodeAuto)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TerritoryCodeAutoOverride)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TerritoryCodeLiability)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TerritoryCodeLiabilityOverride)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TerritoryCodeProperty)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TerritoryCodePropertyOverride)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.PropertyEnrichments)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PropertyEnrichment_Address");
            });

            modelBuilder.Entity<PropertyOccupant>(entity =>
            {
                entity.ToTable("PropertyOccupant", "dbo");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.PropertySearchResult)
                    .WithMany(p => p.PropertyOccupants)
                    .HasForeignKey(d => d.PropertySearchResultId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PropertyOccupant_PropertySearchResult");
            });

            modelBuilder.Entity<PropertyRisk>(entity =>
            {
                entity.ToTable("PropertyRisk", "dbo");

                entity.Property(e => e.AvailableCapacity).HasColumnType("decimal(30, 15)");

                entity.Property(e => e.BuildingTiv)
                    .HasColumnType("decimal(30, 15)")
                    .HasColumnName("BuildingTIV");

                entity.Property(e => e.BusinessIncomeTiv)
                    .HasColumnType("decimal(30, 15)")
                    .HasColumnName("BusinessIncomeTIV");

                entity.Property(e => e.ContentsTiv)
                    .HasColumnType("decimal(30, 15)")
                    .HasColumnName("ContentsTIV");

                entity.Property(e => e.Context).IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalTiv)
                    .HasColumnType("decimal(30, 15)")
                    .HasColumnName("TotalTIV");

                entity.Property(e => e.TotalUsage).HasColumnType("decimal(30, 15)");

                entity.Property(e => e.TotalWeightedRiskScore).HasColumnType("decimal(30, 15)");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.PropertyRisks)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PropertyRisk_Address");
            });

            modelBuilder.Entity<PropertyRiskPeril>(entity =>
            {
                entity.ToTable("PropertyRiskPeril", "dbo");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Peril)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Risk).HasColumnType("decimal(9, 4)");

                entity.HasOne(d => d.PropertyRisk)
                    .WithMany(p => p.PropertyRiskPerils)
                    .HasForeignKey(d => d.PropertyRiskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PropertyRiskPeril_PropertyRisk");
            });

            modelBuilder.Entity<PropertySearchResult>(entity =>
            {
                entity.ToTable("PropertySearchResult", "dbo");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LocationDescription)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MatchTypeCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RiskId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.PropertySearchResults)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PropertySearchResult_Address");
            });

            modelBuilder.Entity<RegionMapping>(entity =>
            {
                entity.ToTable("RegionMapping", "dbo");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Region)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StateCode)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
