﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Address.Data
{
    public partial class FloodZone
    {
        public int Id { get; set; }
        public long AddressId { get; set; }
        public string Response { get; set; }
        public string MaxFloodZone { get; set; }
        public decimal? MinimumDistance { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual Address Address { get; set; }
        public virtual FloodZoneJson FloodZoneJson { get; set; }
    }
}
