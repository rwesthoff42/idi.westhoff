﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Address.Data
{
    public partial class FloodZoneJson
    {
        public int FloodZoneId { get; set; }
        public string Json { get; set; }

        public virtual FloodZone FloodZone { get; set; }
    }
}
