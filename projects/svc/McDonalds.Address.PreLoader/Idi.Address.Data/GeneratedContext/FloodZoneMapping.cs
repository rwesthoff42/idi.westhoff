﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Address.Data
{
    public partial class FloodZoneMapping
    {
        public int Id { get; set; }
        public string FloodZone { get; set; }
        public string FloodZoneShort { get; set; }
        public int Rank { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
