﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Address.Data
{
    public partial class PropertyEnrichment
    {
        public int Id { get; set; }
        public long AddressId { get; set; }
        public string ConfidenceLevel { get; set; }
        public string ProtectionClass { get; set; }
        public string TerritoryCodeProperty { get; set; }
        public string TerritoryCodeLiability { get; set; }
        public string TerritoryCodeAuto { get; set; }
        public string GroupIiZone { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string TerritoryCodeAutoOverride { get; set; }
        public string TerritoryCodeLiabilityOverride { get; set; }
        public string TerritoryCodePropertyOverride { get; set; }

        public virtual Address Address { get; set; }
    }
}
