﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Address.Data
{
    public partial class PropertyOccupant
    {
        public int Id { get; set; }
        public int PropertySearchResultId { get; set; }
        public int? OccupantId { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual PropertySearchResult PropertySearchResult { get; set; }
    }
}
