﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Address.Data
{
    public partial class PropertyRisk
    {
        public PropertyRisk()
        {
            PropertyRiskPerils = new HashSet<PropertyRiskPeril>();
        }

        public int Id { get; set; }
        public long AddressId { get; set; }
        public string Context { get; set; }
        public decimal? BuildingTiv { get; set; }
        public decimal? BusinessIncomeTiv { get; set; }
        public decimal? ContentsTiv { get; set; }
        public decimal? TotalTiv { get; set; }
        public decimal TotalWeightedRiskScore { get; set; }
        public decimal TotalUsage { get; set; }
        public decimal AvailableCapacity { get; set; }
        public bool BudgetDataException { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual Address Address { get; set; }
        public virtual ICollection<PropertyRiskPeril> PropertyRiskPerils { get; set; }
    }
}
