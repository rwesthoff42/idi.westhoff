﻿using Idi.AddressApi.Client.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace McDonalds.Addess.Preloader.Console
{
    public class AddressApiClient
    {
        private readonly HttpClient _client;

        public AddressApiClient(HttpClient client)
        {
            _client = client;
        }

        public async Task<AddressResponse> AddNewAddressAsync(AddressRequest request)
        {
            var response = new AddressResponse();
            try
            {
                var httpResponse = await _client.PostAsync("address", JsonContent.Create(request, new MediaTypeHeaderValue("application/json")));
                if (httpResponse.IsSuccessStatusCode)
                {
                    response = await httpResponse.Content.ReadFromJsonAsync<AddressResponse>();
                }
                else
                {
                    response.Success = false;
                    response.Errors = new List<string> { await httpResponse.Content.ReadAsStringAsync() };
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Address Api call failed");
                response.Success = false;
                response.Errors = new List<string> { "Address Api call Failed.", ex.GetBaseException().Message };
            }
            return response;
        }

        public async Task<string> GetByIdAsync(long id)
        {
            var response = await _client.GetAsync($"address/{id}");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }

            return "Did not find";
        }
    }   

}
