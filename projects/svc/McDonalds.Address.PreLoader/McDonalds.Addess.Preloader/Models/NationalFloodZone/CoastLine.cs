﻿using System.Text.Json.Serialization;

namespace Idi.Address.Models.Dtos
{
    public class CoastLine
    {
        [JsonPropertyName("distkm")]
        public decimal? DistanceInKm { get; set; }

        /// <summary>
        /// Not sure what this means? I think it's the key of a kvp
        /// </summary>
        [JsonPropertyName("ogc_fid")]
        public int OgcFid { get; set; }
    }
}
