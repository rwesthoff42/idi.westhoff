﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Idi.Address.Models.Dtos
{
    public class Elevation
    {
        //[JsonPropertyName("propertyelevation")]
        //public decimal? PropertyElevation { get; set; }

        [JsonPropertyName("flood.basefloodelevation")]
        public IEnumerable<BaseFloodElevation> BaseFloodElevations { get; set; }

        [JsonPropertyName("coastline")]
        public IEnumerable<CoastLine> CoastLines { get; set; }

        [JsonPropertyName("waterbody")]
        public IEnumerable<WaterBody> WaterBodies { get; set; }

        [JsonPropertyName("stormsurge")]
        public StormSurge StormSurges { get; set; }
    }
}
