﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Idi.Address.Models.Dtos
{
    public class FloodDataResult
    {
        [JsonPropertyName("flood.s_fld_haz_ar")]
        public IEnumerable<FloodZoneHazard> FloodZoneHazards { get; set; }

        [JsonPropertyName("elevation")]
        public Elevation Elevation { get; set; }
    }
}
