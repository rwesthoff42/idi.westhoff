﻿using Idi.AddressApi.Client.Models;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace McDonalds.Addess.Preloader.Console
{
    public class TestService : BackgroundService
    {
        private readonly IAddressApiClient _client;

        public TestService(IAddressApiClient client)
        {
            _client = client;
        }
        protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var x = await _client.GetAddressByIdAsync("4685419687");
        }
    }
}
