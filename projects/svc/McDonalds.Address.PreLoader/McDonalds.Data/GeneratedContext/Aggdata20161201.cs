﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class Aggdata20161201
    {
        public int? StoreNumber { get; set; }
        public string StoreType { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int? ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string PlayPlace { get; set; }
        public string DriveThrough { get; set; }
        public string ArchCard { get; set; }
        public string FreeWifi { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public string GeoAccuracy { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string County { get; set; }
    }
}
