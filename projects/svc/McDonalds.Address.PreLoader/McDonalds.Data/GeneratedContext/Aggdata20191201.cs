﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class Aggdata20191201
    {
        public int? StoreNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int? ZipCode { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public string StoreHours { get; set; }
        public string DriveThruHours { get; set; }
        public string Services { get; set; }
        public string DriveThrough { get; set; }
        public string Wifi { get; set; }
        public string MobileDeals { get; set; }
        public string MobileOrders { get; set; }
        public string IndoorPlayplace { get; set; }
        public string IndoorPlayground { get; set; }
        public string OutdoorPlayPlaceAvailable { get; set; }
        public string IndoorDiningAvailable { get; set; }
        public string GiftCards { get; set; }
        public string WalmartLocation { get; set; }
        public string CreateYourTasteAvailable { get; set; }
        public string Mcdelivery { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public string GeoAccuracy { get; set; }
        public string CountryCode { get; set; }
        public string County { get; set; }
    }
}
