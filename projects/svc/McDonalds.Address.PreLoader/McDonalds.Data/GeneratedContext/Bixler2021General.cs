﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class Bixler2021General
    {
        public string OperatorEid { get; set; }
        public string Ownernamefirst { get; set; }
        public string Ownernamemiddle { get; set; }
        public string Ownernamelast { get; set; }
        public string Companyname { get; set; }
        public string Companyfein { get; set; }
        public string State { get; set; }
        public string Mailingaddress1 { get; set; }
        public string Mailingaddress2 { get; set; }
        public string Mailingcity { get; set; }
        public string Mailingstcd { get; set; }
        public string Mailingzip { get; set; }
        public string Mailingcounty { get; set; }
        public string Officephone { get; set; }
        public string Cellphone { get; set; }
        public string Email { get; set; }
        public string Altemail { get; set; }
        public string Faxnumber { get; set; }
        public string Insurancecontact { get; set; }
        public string Numberofstores { get; set; }
        public string Genstoreownership { get; set; }
        public string Claimcontact { get; set; }
        public string Claimcontactname { get; set; }
        public string Claimcontactphone { get; set; }
    }
}
