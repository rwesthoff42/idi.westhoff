﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class Bixler2021Office
    {
        public string OperatorEid { get; set; }
        public string Officename { get; set; }
        public string Officeaddress1 { get; set; }
        public string Officeaddress2 { get; set; }
        public string Officecity { get; set; }
        public string Officestcd { get; set; }
        public string Officezip { get; set; }
        public string Buildingvalue { get; set; }
        public string Contentvalue { get; set; }
        public string Contents { get; set; }
        public string Sqft { get; set; }
        public string Construtiontype { get; set; }
        public string Yearbuilt { get; set; }
        public string Ownrentbuilding { get; set; }
        public string Ifown { get; set; }
        public string Desctenants { get; set; }
        public string Officehas { get; set; }
    }
}
