﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class Bixler2021Warehouse
    {
        public string OperatorEid { get; set; }
        public string Warehouseaddress1 { get; set; }
        public string Warehouseaddress2 { get; set; }
        public string Warehousecity { get; set; }
        public string Warehousestcd { get; set; }
        public string Warehousezip { get; set; }
        public string Warehousename { get; set; }
        public string Warehousebuildingvalue { get; set; }
        public string Warehousecontentvalue { get; set; }
        public string Warehousecontents { get; set; }
        public string Warehousesqft { get; set; }
        public string Warehouseconstrutiontype { get; set; }
        public string Warehouseyearbuilt { get; set; }
        public string Warehouseownrentbuilding { get; set; }
        public string Warehouseifown { get; set; }
        public string Warehouseofficehas { get; set; }
        public string Warehousemortgages { get; set; }
    }
}
