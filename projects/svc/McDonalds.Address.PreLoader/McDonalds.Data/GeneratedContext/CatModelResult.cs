﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class CatModelResult
    {
        public int? StoreNumber { get; set; }
        public int? CatLocNum { get; set; }
        public string GeocodePrecision { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string ZipCode { get; set; }
        public string County { get; set; }
        public decimal? ModelTiv { get; set; }
        public decimal? DistanceToCoast { get; set; }
        public decimal? DeductibleHurricane { get; set; }
        public decimal? DeductibleEq { get; set; }
        public decimal? DeductibleScs { get; set; }
        public decimal? AalHurricane { get; set; }
        public decimal? AalEq { get; set; }
        public decimal? AalScsHigh { get; set; }
        public decimal? AalScsLow { get; set; }
    }
}
