﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class CyAssumption
    {
        public int? Cy { get; set; }
        public decimal? CpTrendCumulative { get; set; }
        public decimal? GlTrendCumulative { get; set; }
        public decimal? CpCdfAy { get; set; }
        public decimal? GlCdfAy { get; set; }
        public int? StoreCount { get; set; }
    }
}
