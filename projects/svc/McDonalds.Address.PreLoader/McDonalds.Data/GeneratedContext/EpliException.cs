﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class EpliException
    {
        public double? KontrolNbr { get; set; }
        public string OperatorName { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public double? Zip { get; set; }
        public decimal? _2021Sir { get; set; }
        public decimal? _2022Sir { get; set; }
        public string ExceptionReason { get; set; }
        public string ActionRequired { get; set; }
    }
}
