﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class FloodZonesByStore
    {
        public int Id { get; set; }
        public string StoreNum { get; set; }
        public string FloodZone { get; set; }
    }
}
