﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class GeneratedXml
    {
        public int Id { get; set; }
        public string OperatorEid { get; set; }
        public string ApplicationXml { get; set; }
    }
}
