﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class LocationListWithStoreType
    {
        public int? Store { get; set; }
        public string Addr1 { get; set; }
        public string City { get; set; }
        public string PstlCd { get; set; }
        public string County { get; set; }
        public string StCd { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string SiteTypeDesc { get; set; }
        public string Unknown { get; set; }
        public string RespOwnerEid { get; set; }
    }
}
