﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class LocationType
    {
        public string SiteTypeDesc { get; set; }
        public string LocationType1 { get; set; }
    }
}
