﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class Lossruns2020
    {
        public string ClaimNumber { get; set; }
        public DateTime? LossDate { get; set; }
        public string Coverage { get; set; }
        public string AccountGroupCode { get; set; }
        public string EntityId { get; set; }
        public string OwnerId { get; set; }
        public string KontrolNumber { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerLastName { get; set; }
        public string OwnerName { get; set; }
        public string ClaimStatus { get; set; }
        public string AccidentDescription { get; set; }
        public decimal? IncurredIndemnity { get; set; }
        public decimal? IncurredTotal { get; set; }
        public decimal? PaidRecovery { get; set; }
        public decimal? PaidTotal { get; set; }
        public string PolicyYear { get; set; }
    }
}
