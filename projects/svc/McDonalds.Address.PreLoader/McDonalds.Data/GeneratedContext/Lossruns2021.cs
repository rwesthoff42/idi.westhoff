﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class Lossruns2021
    {
        public string ClaimNumber { get; set; }
        public DateTime? LossDate { get; set; }
        public int? Coverage { get; set; }
        public string AccountGroupCode { get; set; }
        public string EntityId { get; set; }
        public string OwnerId { get; set; }
        public string KontrolNumber { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerLastName { get; set; }
        public string OwnerName { get; set; }
        public string ClaimStatus { get; set; }
        public string AccidentDescription { get; set; }
        public string PolicyNumber { get; set; }
        public DateTime? PolicyStartDate { get; set; }
        public DateTime? PolicyEndDate { get; set; }
        public string Carrier { get; set; }
        public string Broker { get; set; }
        public double? IncurredIndemnity { get; set; }
        public double? IncurredTotal { get; set; }
        public double? PaidRecovery { get; set; }
        public double? PaidTotal { get; set; }
        public int? PolicyYear { get; set; }
    }
}
