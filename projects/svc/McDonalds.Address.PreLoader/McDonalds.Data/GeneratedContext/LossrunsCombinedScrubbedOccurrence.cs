﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class LossrunsCombinedScrubbedOccurrence
    {
        public string Valuation { get; set; }
        public string ClaimNumber { get; set; }
        public DateTime? LossDate { get; set; }
        public string Coverage { get; set; }
        public string AccountGroupCode { get; set; }
        public int? StoreNumber { get; set; }
        public string EntityId { get; set; }
        public string OwnerId { get; set; }
        public string KontrolNumber { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerLastName { get; set; }
        public string OwnerName { get; set; }
        public string ClaimStatus { get; set; }
        public string AccidentDescription { get; set; }
        public double? IncurredIndemnity { get; set; }
        public double? IncurredTotal { get; set; }
        public double? PaidRecovery { get; set; }
        public double? PaidTotal { get; set; }
        public int? PolicyYear { get; set; }
        public string DescriptionClean { get; set; }
        public string ClaimNumberClean { get; set; }
        public string OccurrenceKey { get; set; }
        public int? OccurrenceClaimCount { get; set; }
    }
}
