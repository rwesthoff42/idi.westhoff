﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class McdonaldsAccuarialContext : DbContext
    {
        public McdonaldsAccuarialContext()
        {
        }

        public McdonaldsAccuarialContext(DbContextOptions<McdonaldsAccuarialContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Aggdata20151201> Aggdata20151201s { get; set; }
        public virtual DbSet<Aggdata20161201> Aggdata20161201s { get; set; }
        public virtual DbSet<Aggdata20171201> Aggdata20171201s { get; set; }
        public virtual DbSet<Aggdata20181201> Aggdata20181201s { get; set; }
        public virtual DbSet<Aggdata20191201> Aggdata20191201s { get; set; }
        public virtual DbSet<Aggdata20201201> Aggdata20201201s { get; set; }
        public virtual DbSet<Bixler> Bixlers { get; set; }
        public virtual DbSet<Bixler2021General> Bixler2021Generals { get; set; }
        public virtual DbSet<Bixler2021Office> Bixler2021Offices { get; set; }
        public virtual DbSet<Bixler2021Store> Bixler2021Stores { get; set; }
        public virtual DbSet<Bixler2021Warehouse> Bixler2021Warehouses { get; set; }
        public virtual DbSet<CatModelResult> CatModelResults { get; set; }
        public virtual DbSet<CoverageMap> CoverageMaps { get; set; }
        public virtual DbSet<CyAssumption> CyAssumptions { get; set; }
        public virtual DbSet<ElpiSirlimitOverride> ElpiSirlimitOverrides { get; set; }
        public virtual DbSet<EpliException> EpliExceptions { get; set; }
        public virtual DbSet<FloodMapping> FloodMappings { get; set; }
        public virtual DbSet<FloodMappingBkup7232021> FloodMappingBkup7232021s { get; set; }
        public virtual DbSet<FloodZoneMapping> FloodZoneMappings { get; set; }
        public virtual DbSet<FloodZonesByStore> FloodZonesByStores { get; set; }
        public virtual DbSet<FranchisedStores2020> FranchisedStores2020s { get; set; }
        public virtual DbSet<GeneratedXml> GeneratedXmls { get; set; }
        public virtual DbSet<GoogleAddressDatum> GoogleAddressData { get; set; }
        public virtual DbSet<IsoGlStategroup> IsoGlStategroups { get; set; }
        public virtual DbSet<LobSummary> LobSummaries { get; set; }
        public virtual DbSet<LocationListWithStoreType> LocationListWithStoreTypes { get; set; }
        public virtual DbSet<LocationType> LocationTypes { get; set; }
        public virtual DbSet<Lossruns20142018> Lossruns20142018s { get; set; }
        public virtual DbSet<Lossruns2018> Lossruns2018s { get; set; }
        public virtual DbSet<Lossruns2019> Lossruns2019s { get; set; }
        public virtual DbSet<Lossruns2020> Lossruns2020s { get; set; }
        public virtual DbSet<Lossruns202012> Lossruns202012s { get; set; }
        public virtual DbSet<Lossruns2021> Lossruns2021s { get; set; }
        public virtual DbSet<LossrunsCombined> LossrunsCombineds { get; set; }
        public virtual DbSet<LossrunsCombinedScrubbed> LossrunsCombinedScrubbeds { get; set; }
        public virtual DbSet<LossrunsCombinedScrubbedClaimNumber> LossrunsCombinedScrubbedClaimNumbers { get; set; }
        public virtual DbSet<LossrunsCombinedScrubbedDescription> LossrunsCombinedScrubbedDescriptions { get; set; }
        public virtual DbSet<LossrunsCombinedScrubbedOccurrence> LossrunsCombinedScrubbedOccurrences { get; set; }
        public virtual DbSet<LossrunsCombinedScrubbedSummary> LossrunsCombinedScrubbedSummaries { get; set; }
        public virtual DbSet<LossrunsCurrent> LossrunsCurrents { get; set; }
        public virtual DbSet<McDonaldsLocationType> McDonaldsLocationTypes { get; set; }
        public virtual DbSet<ModelCpCoefficient> ModelCpCoefficients { get; set; }
        public virtual DbSet<ModelCpCoefficientsTest> ModelCpCoefficientsTests { get; set; }
        public virtual DbSet<ModelDataset> ModelDatasets { get; set; }
        public virtual DbSet<ModelGlCoefficient> ModelGlCoefficients { get; set; }
        public virtual DbSet<ModelGlCoefficientsTest> ModelGlCoefficientsTests { get; set; }
        public virtual DbSet<ModelResult> ModelResults { get; set; }
        public virtual DbSet<ModelResultsTest> ModelResultsTests { get; set; }
        public virtual DbSet<ModelStoreyear> ModelStoreyears { get; set; }
        public virtual DbSet<OfficeAddressMapping> OfficeAddressMappings { get; set; }
        public virtual DbSet<OldXmlLossRun> OldXmlLossRuns { get; set; }
        public virtual DbSet<OldXmlStoreinfo> OldXmlStoreinfos { get; set; }
        public virtual DbSet<OlobMap> OlobMaps { get; set; }
        public virtual DbSet<PayloadXml> PayloadXmls { get; set; }
        public virtual DbSet<PivotXmlgeneral> PivotXmlgenerals { get; set; }
        public virtual DbSet<PivotXmllossRun> PivotXmllossRuns { get; set; }
        public virtual DbSet<PivotXmlstore> PivotXmlstores { get; set; }
        public virtual DbSet<PricingRule> PricingRules { get; set; }
        public virtual DbSet<RoofScore> RoofScores { get; set; }
        public virtual DbSet<SalesBucketingByRegion> SalesBucketingByRegions { get; set; }
        public virtual DbSet<SiteSales2019V2> SiteSales2019V2s { get; set; }
        public virtual DbSet<Sitesales2019> Sitesales2019s { get; set; }
        public virtual DbSet<Sitesales2020> Sitesales2020s { get; set; }
        public virtual DbSet<StateRegion> StateRegions { get; set; }
        public virtual DbSet<StoreAddressMapping> StoreAddressMappings { get; set; }
        public virtual DbSet<StoreCrimeScore> StoreCrimeScores { get; set; }
        public virtual DbSet<StoreYearExperience> StoreYearExperiences { get; set; }
        public virtual DbSet<TieringRule> TieringRules { get; set; }
        public virtual DbSet<VotingDatum> VotingData { get; set; }
        public virtual DbSet<Vw50QuoteSummary> Vw50QuoteSummaries { get; set; }
        public virtual DbSet<VwConsolidatedLocationExposure> VwConsolidatedLocationExposures { get; set; }
        public virtual DbSet<VwGenesys50Quote> VwGenesys50Quotes { get; set; }
        public virtual DbSet<VwGenesysQuote> VwGenesysQuotes { get; set; }
        public virtual DbSet<VwGenesysQuotesAlBeforererate> VwGenesysQuotesAlBeforererates { get; set; }
        public virtual DbSet<VwGenesysQuotesVaBeforererate> VwGenesysQuotesVaBeforererates { get; set; }
        public virtual DbSet<VwLocationSale> VwLocationSales { get; set; }
        public virtual DbSet<VwMissingLossesCurrentValuation> VwMissingLossesCurrentValuations { get; set; }
        public virtual DbSet<VwStoreBeginEnd> VwStoreBeginEnds { get; set; }
        public virtual DbSet<VwStoreHistory> VwStoreHistories { get; set; }
        public virtual DbSet<VwStoreYear> VwStoreYears { get; set; }
        public virtual DbSet<VwVerifyScrub> VwVerifyScrubs { get; set; }
        public virtual DbSet<WarehouseAddressMapping> WarehouseAddressMappings { get; set; }
        public virtual DbSet<XmlAccount> XmlAccounts { get; set; }
        public virtual DbSet<XmlGeneralPrepivot> XmlGeneralPrepivots { get; set; }
        public virtual DbSet<XmlLossrunPrepivot> XmlLossrunPrepivots { get; set; }
        public virtual DbSet<XmlStore> XmlStores { get; set; }
        public virtual DbSet<XmlStorePrepivot> XmlStorePrepivots { get; set; }
        public virtual DbSet<ZipDatum> ZipData { get; set; }
        public virtual DbSet<_2021General> _2021Generals { get; set; }
        public virtual DbSet<_2021Office> _2021Offices { get; set; }
        public virtual DbSet<_2021Store> _2021Stores { get; set; }
        public virtual DbSet<_2021Warehouse> _2021Warehouses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("WRBTS\\rwesthoff")
                .HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Aggdata20151201>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("aggdata_2015-12-01", "dbo");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .HasColumnName("address");

                entity.Property(e => e.ArchCard)
                    .HasMaxLength(255)
                    .HasColumnName("arch_card");

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .HasColumnName("city");

                entity.Property(e => e.Country)
                    .HasMaxLength(255)
                    .HasColumnName("country");

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(255)
                    .HasColumnName("country_code");

                entity.Property(e => e.County)
                    .HasMaxLength(255)
                    .HasColumnName("county");

                entity.Property(e => e.DriveThrough)
                    .HasMaxLength(255)
                    .HasColumnName("drive_through");

                entity.Property(e => e.FreeWiFi)
                    .HasMaxLength(255)
                    .HasColumnName("free_wi-fi");

                entity.Property(e => e.GeoAccuracy)
                    .HasMaxLength(255)
                    .HasColumnName("geo_accuracy");

                entity.Property(e => e.Latitude)
                    .HasColumnType("decimal(38, 12)")
                    .HasColumnName("latitude");

                entity.Property(e => e.Longitude)
                    .HasColumnType("decimal(38, 12)")
                    .HasColumnName("longitude");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(255)
                    .HasColumnName("phone_number");

                entity.Property(e => e.PlayPlace)
                    .HasMaxLength(255)
                    .HasColumnName("play_place");

                entity.Property(e => e.State)
                    .HasMaxLength(255)
                    .HasColumnName("state");

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.StoreType)
                    .HasMaxLength(255)
                    .HasColumnName("store_type");

                entity.Property(e => e.ZipCode).HasColumnName("zip_code");
            });

            modelBuilder.Entity<Aggdata20161201>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("aggdata_2016-12-01", "dbo");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .HasColumnName("address");

                entity.Property(e => e.ArchCard)
                    .HasMaxLength(255)
                    .HasColumnName("arch_card");

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .HasColumnName("city");

                entity.Property(e => e.Country)
                    .HasMaxLength(255)
                    .HasColumnName("country");

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(255)
                    .HasColumnName("country_code");

                entity.Property(e => e.County)
                    .HasMaxLength(255)
                    .HasColumnName("county");

                entity.Property(e => e.DriveThrough)
                    .HasMaxLength(255)
                    .HasColumnName("drive_through");

                entity.Property(e => e.FreeWifi)
                    .HasMaxLength(255)
                    .HasColumnName("free_wifi");

                entity.Property(e => e.GeoAccuracy)
                    .HasMaxLength(255)
                    .HasColumnName("geo_accuracy");

                entity.Property(e => e.Latitude)
                    .HasColumnType("decimal(38, 12)")
                    .HasColumnName("latitude");

                entity.Property(e => e.Longitude)
                    .HasColumnType("decimal(38, 12)")
                    .HasColumnName("longitude");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(255)
                    .HasColumnName("phone_number");

                entity.Property(e => e.PlayPlace)
                    .HasMaxLength(255)
                    .HasColumnName("play_place");

                entity.Property(e => e.State)
                    .HasMaxLength(255)
                    .HasColumnName("state");

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.StoreType)
                    .HasMaxLength(255)
                    .HasColumnName("store_type");

                entity.Property(e => e.ZipCode).HasColumnName("zip_code");
            });

            modelBuilder.Entity<Aggdata20171201>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("aggdata_2017-12-01", "dbo");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .HasColumnName("address");

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .HasColumnName("city");

                entity.Property(e => e.Country)
                    .HasMaxLength(255)
                    .HasColumnName("country");

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(255)
                    .HasColumnName("country_code");

                entity.Property(e => e.County)
                    .HasMaxLength(255)
                    .HasColumnName("county");

                entity.Property(e => e.CreateYourTasteAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("create_your_taste_available");

                entity.Property(e => e.DriveThrough)
                    .HasMaxLength(255)
                    .HasColumnName("drive_through");

                entity.Property(e => e.DriveThruHours)
                    .HasMaxLength(255)
                    .HasColumnName("drive_thru_hours");

                entity.Property(e => e.GeoAccuracy)
                    .HasMaxLength(255)
                    .HasColumnName("geo_accuracy");

                entity.Property(e => e.GiftCards)
                    .HasMaxLength(255)
                    .HasColumnName("gift_cards");

                entity.Property(e => e.IndoorDiningAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_dining_available");

                entity.Property(e => e.IndoorPlayground)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_playground");

                entity.Property(e => e.IndoorPlayplace)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_playplace");

                entity.Property(e => e.Latitude)
                    .HasColumnType("decimal(38, 12)")
                    .HasColumnName("latitude");

                entity.Property(e => e.Longitude)
                    .HasColumnType("decimal(38, 12)")
                    .HasColumnName("longitude");

                entity.Property(e => e.MobileDeals)
                    .HasMaxLength(255)
                    .HasColumnName("mobile_deals");

                entity.Property(e => e.MobileOrders)
                    .HasMaxLength(255)
                    .HasColumnName("mobile_orders");

                entity.Property(e => e.OutdoorPlayPlaceAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("outdoor_play_place_available");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(255)
                    .HasColumnName("phone_number");

                entity.Property(e => e.Services)
                    .HasMaxLength(255)
                    .HasColumnName("services");

                entity.Property(e => e.State)
                    .HasMaxLength(255)
                    .HasColumnName("state");

                entity.Property(e => e.StoreHours)
                    .HasMaxLength(255)
                    .HasColumnName("store_hours");

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.WalmartLocation)
                    .HasMaxLength(255)
                    .HasColumnName("walmart_location");

                entity.Property(e => e.Wifi)
                    .HasMaxLength(255)
                    .HasColumnName("wifi");

                entity.Property(e => e.ZipCode).HasColumnName("zip_code");
            });

            modelBuilder.Entity<Aggdata20181201>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("aggdata_2018-12-01", "dbo");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .HasColumnName("address");

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .HasColumnName("city");

                entity.Property(e => e.Country)
                    .HasMaxLength(255)
                    .HasColumnName("country");

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(255)
                    .HasColumnName("country_code");

                entity.Property(e => e.County)
                    .HasMaxLength(255)
                    .HasColumnName("county");

                entity.Property(e => e.CreateYourOwn)
                    .HasMaxLength(255)
                    .HasColumnName("create_your_own");

                entity.Property(e => e.DriveThrough)
                    .HasMaxLength(255)
                    .HasColumnName("drive_through");

                entity.Property(e => e.DrivethruHours)
                    .HasMaxLength(255)
                    .HasColumnName("drivethru_hours");

                entity.Property(e => e.GeoAccuracy)
                    .HasMaxLength(255)
                    .HasColumnName("geo_accuracy");

                entity.Property(e => e.GiftCards)
                    .HasMaxLength(255)
                    .HasColumnName("gift_cards");

                entity.Property(e => e.IndoorDiningAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_dining_available");

                entity.Property(e => e.IndoorPlayground)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_playground");

                entity.Property(e => e.Latitude)
                    .HasColumnType("decimal(38, 12)")
                    .HasColumnName("latitude");

                entity.Property(e => e.Longitude)
                    .HasColumnType("decimal(38, 12)")
                    .HasColumnName("longitude");

                entity.Property(e => e.Mcdelivery)
                    .HasMaxLength(255)
                    .HasColumnName("mcdelivery");

                entity.Property(e => e.MobileDeals)
                    .HasMaxLength(255)
                    .HasColumnName("mobile_deals");

                entity.Property(e => e.MobileOrders)
                    .HasMaxLength(255)
                    .HasColumnName("mobile_orders");

                entity.Property(e => e.OutdoorPlayPlaceAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("outdoor_play_place_available");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(255)
                    .HasColumnName("phone_number");

                entity.Property(e => e.Services)
                    .HasMaxLength(255)
                    .HasColumnName("services");

                entity.Property(e => e.State)
                    .HasMaxLength(255)
                    .HasColumnName("state");

                entity.Property(e => e.Store)
                    .HasMaxLength(255)
                    .HasColumnName("store");

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.WalmartLocation)
                    .HasMaxLength(255)
                    .HasColumnName("walmart_location");

                entity.Property(e => e.Wifi)
                    .HasMaxLength(255)
                    .HasColumnName("wifi");

                entity.Property(e => e.ZipCode).HasColumnName("zip_code");
            });

            modelBuilder.Entity<Aggdata20191201>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("aggdata_2019-12-01", "dbo");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .HasColumnName("address");

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .HasColumnName("city");

                entity.Property(e => e.Country)
                    .HasMaxLength(255)
                    .HasColumnName("country");

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(255)
                    .HasColumnName("country_code");

                entity.Property(e => e.County)
                    .HasMaxLength(255)
                    .HasColumnName("county");

                entity.Property(e => e.CreateYourTasteAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("create_your_taste_available");

                entity.Property(e => e.DriveThrough)
                    .HasMaxLength(255)
                    .HasColumnName("drive_through");

                entity.Property(e => e.DriveThruHours)
                    .HasMaxLength(255)
                    .HasColumnName("drive_thru_hours");

                entity.Property(e => e.GeoAccuracy)
                    .HasMaxLength(255)
                    .HasColumnName("geo_accuracy");

                entity.Property(e => e.GiftCards)
                    .HasMaxLength(255)
                    .HasColumnName("gift_cards");

                entity.Property(e => e.IndoorDiningAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_dining_available");

                entity.Property(e => e.IndoorPlayground)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_playground");

                entity.Property(e => e.IndoorPlayplace)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_playplace");

                entity.Property(e => e.Latitude)
                    .HasColumnType("decimal(38, 12)")
                    .HasColumnName("latitude");

                entity.Property(e => e.Longitude)
                    .HasColumnType("decimal(38, 12)")
                    .HasColumnName("longitude");

                entity.Property(e => e.Mcdelivery)
                    .HasMaxLength(255)
                    .HasColumnName("mcdelivery");

                entity.Property(e => e.MobileDeals)
                    .HasMaxLength(255)
                    .HasColumnName("mobile_deals");

                entity.Property(e => e.MobileOrders)
                    .HasMaxLength(255)
                    .HasColumnName("mobile_orders");

                entity.Property(e => e.OutdoorPlayPlaceAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("outdoor_play_place_available");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(255)
                    .HasColumnName("phone_number");

                entity.Property(e => e.Services)
                    .HasMaxLength(255)
                    .HasColumnName("services");

                entity.Property(e => e.State)
                    .HasMaxLength(255)
                    .HasColumnName("state");

                entity.Property(e => e.StoreHours)
                    .HasMaxLength(255)
                    .HasColumnName("store_hours");

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.WalmartLocation)
                    .HasMaxLength(255)
                    .HasColumnName("walmart_location");

                entity.Property(e => e.Wifi)
                    .HasMaxLength(255)
                    .HasColumnName("wifi");

                entity.Property(e => e.ZipCode).HasColumnName("zip_code");
            });

            modelBuilder.Entity<Aggdata20201201>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("aggdata_2020-12-01", "dbo");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .HasColumnName("address");

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .HasColumnName("city");

                entity.Property(e => e.Country)
                    .HasMaxLength(255)
                    .HasColumnName("country");

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(255)
                    .HasColumnName("country_code");

                entity.Property(e => e.County)
                    .HasMaxLength(255)
                    .HasColumnName("county");

                entity.Property(e => e.CreateYourTasteAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("create_your_taste_available");

                entity.Property(e => e.DriveThrough)
                    .HasMaxLength(255)
                    .HasColumnName("drive_through");

                entity.Property(e => e.DriveThruHours)
                    .HasMaxLength(255)
                    .HasColumnName("drive_thru_hours");

                entity.Property(e => e.GeoAccuracy)
                    .HasMaxLength(255)
                    .HasColumnName("geo_accuracy");

                entity.Property(e => e.GiftCards)
                    .HasMaxLength(255)
                    .HasColumnName("gift_cards");

                entity.Property(e => e.IndoorDiningAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_dining_available");

                entity.Property(e => e.IndoorPlayground)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_playground");

                entity.Property(e => e.IndoorPlayplace)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_playplace");

                entity.Property(e => e.Latitude)
                    .HasColumnType("decimal(38, 12)")
                    .HasColumnName("latitude");

                entity.Property(e => e.Longitude)
                    .HasColumnType("decimal(38, 12)")
                    .HasColumnName("longitude");

                entity.Property(e => e.Mcdelivery)
                    .HasMaxLength(255)
                    .HasColumnName("mcdelivery");

                entity.Property(e => e.MobileDeals)
                    .HasMaxLength(255)
                    .HasColumnName("mobile_deals");

                entity.Property(e => e.MobileOrders)
                    .HasMaxLength(255)
                    .HasColumnName("mobile_orders");

                entity.Property(e => e.OutdoorPlayPlaceAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("outdoor_play_place_available");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(255)
                    .HasColumnName("phone_number");

                entity.Property(e => e.Services)
                    .HasMaxLength(255)
                    .HasColumnName("services");

                entity.Property(e => e.State)
                    .HasMaxLength(255)
                    .HasColumnName("state");

                entity.Property(e => e.StoreHours)
                    .HasMaxLength(255)
                    .HasColumnName("store_hours");

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.WalmartLocation)
                    .HasMaxLength(255)
                    .HasColumnName("walmart_location");

                entity.Property(e => e.Wifi)
                    .HasMaxLength(255)
                    .HasColumnName("wifi");

                entity.Property(e => e.ZipCode).HasColumnName("zip_code");
            });

            modelBuilder.Entity<Bixler>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Bixler", "dbo");

                entity.Property(e => e.LocationType)
                    .IsRequired()
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.PolicyNbr)
                    .IsRequired()
                    .HasMaxLength(67)
                    .IsUnicode(false);

                entity.Property(e => e.UnitcommonId).HasColumnName("UNITCOMMON_ID");
            });

            modelBuilder.Entity<Bixler2021General>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Bixler_2021_general", "dbo");

                entity.Property(e => e.Altemail)
                    .HasMaxLength(255)
                    .HasColumnName("ALTEMAIL");

                entity.Property(e => e.Cellphone)
                    .HasMaxLength(255)
                    .HasColumnName("CELLPHONE");

                entity.Property(e => e.Claimcontact)
                    .HasMaxLength(255)
                    .HasColumnName("CLAIMCONTACT");

                entity.Property(e => e.Claimcontactname)
                    .HasMaxLength(255)
                    .HasColumnName("CLAIMCONTACTNAME");

                entity.Property(e => e.Claimcontactphone)
                    .HasMaxLength(255)
                    .HasColumnName("CLAIMCONTACTPHONE");

                entity.Property(e => e.Companyfein)
                    .HasMaxLength(255)
                    .HasColumnName("COMPANYFEIN");

                entity.Property(e => e.Companyname)
                    .HasMaxLength(255)
                    .HasColumnName("COMPANYNAME");

                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .HasColumnName("EMAIL");

                entity.Property(e => e.Faxnumber)
                    .HasMaxLength(255)
                    .HasColumnName("FAXNUMBER");

                entity.Property(e => e.Genstoreownership)
                    .HasMaxLength(255)
                    .HasColumnName("GENSTOREOWNERSHIP");

                entity.Property(e => e.Insurancecontact)
                    .HasMaxLength(255)
                    .HasColumnName("INSURANCECONTACT");

                entity.Property(e => e.Mailingaddress1)
                    .HasMaxLength(255)
                    .HasColumnName("MAILINGADDRESS1");

                entity.Property(e => e.Mailingaddress2)
                    .HasMaxLength(255)
                    .HasColumnName("MAILINGADDRESS2");

                entity.Property(e => e.Mailingcity)
                    .HasMaxLength(255)
                    .HasColumnName("MAILINGCITY");

                entity.Property(e => e.Mailingcounty)
                    .HasMaxLength(255)
                    .HasColumnName("MAILINGCOUNTY");

                entity.Property(e => e.Mailingstcd)
                    .HasMaxLength(255)
                    .HasColumnName("MAILINGSTCD");

                entity.Property(e => e.Mailingzip)
                    .HasMaxLength(255)
                    .HasColumnName("MAILINGZIP");

                entity.Property(e => e.Numberofstores)
                    .HasMaxLength(255)
                    .HasColumnName("NUMBEROFSTORES");

                entity.Property(e => e.Officephone)
                    .HasMaxLength(255)
                    .HasColumnName("OFFICEPHONE");

                entity.Property(e => e.OperatorEid)
                    .HasMaxLength(255)
                    .HasColumnName("operator eid");

                entity.Property(e => e.Ownernamefirst)
                    .HasMaxLength(255)
                    .HasColumnName("OWNERNAMEFIRST");

                entity.Property(e => e.Ownernamelast)
                    .HasMaxLength(255)
                    .HasColumnName("OWNERNAMELAST");

                entity.Property(e => e.Ownernamemiddle)
                    .HasMaxLength(255)
                    .HasColumnName("OWNERNAMEMIDDLE");

                entity.Property(e => e.State)
                    .HasMaxLength(255)
                    .HasColumnName("STATE");
            });

            modelBuilder.Entity<Bixler2021Office>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Bixler_2021_office", "dbo");

                entity.Property(e => e.Buildingvalue)
                    .HasMaxLength(255)
                    .HasColumnName("BUILDINGVALUE");

                entity.Property(e => e.Construtiontype)
                    .HasMaxLength(255)
                    .HasColumnName("CONSTRUTIONTYPE");

                entity.Property(e => e.Contents)
                    .HasMaxLength(255)
                    .HasColumnName("CONTENTS");

                entity.Property(e => e.Contentvalue)
                    .HasMaxLength(255)
                    .HasColumnName("CONTENTVALUE");

                entity.Property(e => e.Desctenants)
                    .HasMaxLength(255)
                    .HasColumnName("DESCTENANTS");

                entity.Property(e => e.Ifown)
                    .HasMaxLength(255)
                    .HasColumnName("IFOWN");

                entity.Property(e => e.Officeaddress1)
                    .HasMaxLength(255)
                    .HasColumnName("OFFICEADDRESS1");

                entity.Property(e => e.Officeaddress2)
                    .HasMaxLength(255)
                    .HasColumnName("OFFICEADDRESS2");

                entity.Property(e => e.Officecity)
                    .HasMaxLength(255)
                    .HasColumnName("OFFICECITY");

                entity.Property(e => e.Officehas)
                    .HasMaxLength(255)
                    .HasColumnName("OFFICEHAS");

                entity.Property(e => e.Officename)
                    .HasMaxLength(255)
                    .HasColumnName("OFFICENAME");

                entity.Property(e => e.Officestcd)
                    .HasMaxLength(255)
                    .HasColumnName("OFFICESTCD");

                entity.Property(e => e.Officezip)
                    .HasMaxLength(255)
                    .HasColumnName("OFFICEZIP");

                entity.Property(e => e.OperatorEid)
                    .HasMaxLength(255)
                    .HasColumnName("operator eid");

                entity.Property(e => e.Ownrentbuilding)
                    .HasMaxLength(255)
                    .HasColumnName("OWNRENTBUILDING");

                entity.Property(e => e.Sqft)
                    .HasMaxLength(255)
                    .HasColumnName("SQFT");

                entity.Property(e => e.Yearbuilt)
                    .HasMaxLength(255)
                    .HasColumnName("YEARBUILT");
            });

            modelBuilder.Entity<Bixler2021Store>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Bixler_2021_store", "dbo");

                entity.Property(e => e.Addbuildings)
                    .HasMaxLength(255)
                    .HasColumnName("ADDBUILDINGS");

                entity.Property(e => e.Addbuildingsdesc)
                    .HasMaxLength(255)
                    .HasColumnName("ADDBUILDINGSDESC");

                entity.Property(e => e.Addstoretype)
                    .HasMaxLength(255)
                    .HasColumnName("ADDSTORETYPE");

                entity.Property(e => e.Alcoholuse)
                    .HasMaxLength(255)
                    .HasColumnName("ALCOHOLUSE");

                entity.Property(e => e.Annualsales)
                    .HasMaxLength(255)
                    .HasColumnName("ANNUALSALES");

                entity.Property(e => e.Ansulsystem)
                    .HasMaxLength(255)
                    .HasColumnName("ANSULSYSTEM");

                entity.Property(e => e.Armoredcare)
                    .HasMaxLength(255)
                    .HasColumnName("ARMOREDCARE");

                entity.Property(e => e.Autosprinkler)
                    .HasMaxLength(255)
                    .HasColumnName("AUTOSPRINKLER");

                entity.Property(e => e.Basement)
                    .HasMaxLength(255)
                    .HasColumnName("BASEMENT");

                entity.Property(e => e.Burgalarmcent)
                    .HasMaxLength(255)
                    .HasColumnName("BURGALARMCENT");

                entity.Property(e => e.Cleanhoodduck)
                    .HasMaxLength(255)
                    .HasColumnName("CLEANHOODDUCK");

                entity.Property(e => e.Companynamestore)
                    .HasMaxLength(255)
                    .HasColumnName("COMPANYNAMESTORE");

                entity.Property(e => e.Companytype)
                    .HasMaxLength(255)
                    .HasColumnName("COMPANYTYPE");

                entity.Property(e => e.Constructiontype)
                    .HasMaxLength(255)
                    .HasColumnName("CONSTRUCTIONTYPE");

                entity.Property(e => e.Delannualsales)
                    .HasMaxLength(255)
                    .HasColumnName("DELANNUALSALES");

                entity.Property(e => e.Delavgrad)
                    .HasMaxLength(255)
                    .HasColumnName("DELAVGRAD");

                entity.Property(e => e.Deldrilic)
                    .HasMaxLength(255)
                    .HasColumnName("DELDRILIC");

                entity.Property(e => e.Deldrirec)
                    .HasMaxLength(255)
                    .HasColumnName("DELDRIREC");

                entity.Property(e => e.Deldrivehrec)
                    .HasMaxLength(255)
                    .HasColumnName("DELDRIVEHREC");

                entity.Property(e => e.Delemp)
                    .HasMaxLength(255)
                    .HasColumnName("DELEMP");

                entity.Property(e => e.Delesgguestcount)
                    .HasMaxLength(255)
                    .HasColumnName("DELESGGUESTCOUNT");

                entity.Property(e => e.Delinscar)
                    .HasMaxLength(255)
                    .HasColumnName("DELINSCAR");

                entity.Property(e => e.Deliver)
                    .HasMaxLength(255)
                    .HasColumnName("DELIVER");

                entity.Property(e => e.Delmore5)
                    .HasMaxLength(255)
                    .HasColumnName("DELMORE5");

                entity.Property(e => e.Delseccar)
                    .HasMaxLength(255)
                    .HasColumnName("DELSECCAR");

                entity.Property(e => e.Delwho)
                    .HasMaxLength(255)
                    .HasColumnName("DELWHO");

                entity.Property(e => e.Delwriproc)
                    .HasMaxLength(255)
                    .HasColumnName("DELWRIPROC");

                entity.Property(e => e.Depositstobank)
                    .HasMaxLength(255)
                    .HasColumnName("DEPOSITSTOBANK");

                entity.Property(e => e.Drivethru)
                    .HasMaxLength(255)
                    .HasColumnName("DRIVETHRU");

                entity.Property(e => e.Drivethruguestcount)
                    .HasMaxLength(255)
                    .HasColumnName("DRIVETHRUGUESTCOUNT");

                entity.Property(e => e.Estguestcount)
                    .HasMaxLength(255)
                    .HasColumnName("ESTGUESTCOUNT");

                entity.Property(e => e.Fein)
                    .HasMaxLength(255)
                    .HasColumnName("FEIN");

                entity.Property(e => e.Firealarmcent)
                    .HasMaxLength(255)
                    .HasColumnName("FIREALARMCENT");

                entity.Property(e => e.Fryerreplace)
                    .HasMaxLength(255)
                    .HasColumnName("FRYERREPLACE");

                entity.Property(e => e.Haveplayplace)
                    .HasMaxLength(255)
                    .HasColumnName("HAVEPLAYPLACE");

                entity.Property(e => e.If24op)
                    .HasMaxLength(255)
                    .HasColumnName("IF24OP");

                entity.Property(e => e.Ifnonmcdops)
                    .HasMaxLength(255)
                    .HasColumnName("IFNONMCDOPS");

                entity.Property(e => e.Insured)
                    .HasMaxLength(255)
                    .HasColumnName("INSURED");

                entity.Property(e => e.Lastrebuild)
                    .HasMaxLength(255)
                    .HasColumnName("LASTREBUILD");

                entity.Property(e => e.Liqctl)
                    .HasMaxLength(255)
                    .HasColumnName("LIQCTL");

                entity.Property(e => e.Liqctlexp)
                    .HasMaxLength(255)
                    .HasColumnName("LIQCTLEXP");

                entity.Property(e => e.Liqdescsold)
                    .HasMaxLength(255)
                    .HasColumnName("LIQDESCSOLD");

                entity.Property(e => e.Liqdrivethru)
                    .HasMaxLength(255)
                    .HasColumnName("LIQDRIVETHRU");

                entity.Property(e => e.Liqempdrink)
                    .HasMaxLength(255)
                    .HasColumnName("LIQEMPDRINK");

                entity.Property(e => e.Liqempexp)
                    .HasMaxLength(255)
                    .HasColumnName("LIQEMPEXP");

                entity.Property(e => e.Liqemptrain)
                    .HasMaxLength(255)
                    .HasColumnName("LIQEMPTRAIN");

                entity.Property(e => e.Liqhostevt)
                    .HasMaxLength(255)
                    .HasColumnName("LIQHOSTEVT");

                entity.Property(e => e.Liqhostexp)
                    .HasMaxLength(255)
                    .HasColumnName("LIQHOSTEXP");

                entity.Property(e => e.Liqhoursold)
                    .HasMaxLength(255)
                    .HasColumnName("LIQHOURSOLD");

                entity.Property(e => e.Liqintcus)
                    .HasMaxLength(255)
                    .HasColumnName("LIQINTCUS");

                entity.Property(e => e.Liqintexp)
                    .HasMaxLength(255)
                    .HasColumnName("LIQINTEXP");

                entity.Property(e => e.Liqintsign)
                    .HasMaxLength(255)
                    .HasColumnName("LIQINTSIGN");

                entity.Property(e => e.Liqlicexp)
                    .HasMaxLength(255)
                    .HasColumnName("LIQLICEXP");

                entity.Property(e => e.Liqlicsup)
                    .HasMaxLength(255)
                    .HasColumnName("LIQLICSUP");

                entity.Property(e => e.Liqphtexp)
                    .HasMaxLength(255)
                    .HasColumnName("LIQPHTEXP");

                entity.Property(e => e.Liqphtid)
                    .HasMaxLength(255)
                    .HasColumnName("LIQPHTID");

                entity.Property(e => e.Liqsignexp)
                    .HasMaxLength(255)
                    .HasColumnName("LIQSIGNEXP");

                entity.Property(e => e.Liqtotsale)
                    .HasMaxLength(255)
                    .HasColumnName("LIQTOTSALE");

                entity.Property(e => e.Liqtrainexp)
                    .HasMaxLength(255)
                    .HasColumnName("LIQTRAINEXP");

                entity.Property(e => e.Lobbytreatyes)
                    .HasMaxLength(255)
                    .HasColumnName("LOBBYTREATYES");

                entity.Property(e => e.Natlstorenbr)
                    .HasMaxLength(255)
                    .HasColumnName("NATLSTORENBR");

                entity.Property(e => e.Newoperatornamefirst)
                    .HasMaxLength(255)
                    .HasColumnName("NEWOPERATORNAMEFIRST");

                entity.Property(e => e.Newoperatornamelast)
                    .HasMaxLength(255)
                    .HasColumnName("NEWOPERATORNAMELAST");

                entity.Property(e => e.Nonmcdops)
                    .HasMaxLength(255)
                    .HasColumnName("NONMCDOPS");

                entity.Property(e => e.OperatorEid)
                    .HasMaxLength(255)
                    .HasColumnName("operator eid");

                entity.Property(e => e.Operatorid)
                    .HasMaxLength(255)
                    .HasColumnName("OPERATORID");

                entity.Property(e => e.Operatornamefirst)
                    .HasMaxLength(255)
                    .HasColumnName("OPERATORNAMEFIRST");

                entity.Property(e => e.Operatornamelast)
                    .HasMaxLength(255)
                    .HasColumnName("OPERATORNAMELAST");

                entity.Property(e => e.Ownershipstart)
                    .HasMaxLength(255)
                    .HasColumnName("OWNERSHIPSTART");

                entity.Property(e => e.Parking)
                    .HasMaxLength(255)
                    .HasColumnName("PARKING");

                entity.Property(e => e.Playland)
                    .HasMaxLength(255)
                    .HasColumnName("PLAYLAND");

                entity.Property(e => e.Playplace)
                    .HasMaxLength(255)
                    .HasColumnName("PLAYPLACE");

                entity.Property(e => e.Seating)
                    .HasMaxLength(255)
                    .HasColumnName("SEATING");

                entity.Property(e => e.Seccamera)
                    .HasMaxLength(255)
                    .HasColumnName("SECCAMERA");

                entity.Property(e => e.Seccameraindoor)
                    .HasMaxLength(255)
                    .HasColumnName("SECCAMERAINDOOR");

                entity.Property(e => e.Seccameraoutdoor)
                    .HasMaxLength(255)
                    .HasColumnName("SECCAMERAOUTDOOR");

                entity.Property(e => e.Secguard)
                    .HasMaxLength(255)
                    .HasColumnName("SECGUARD");

                entity.Property(e => e.Secguardarmed)
                    .HasMaxLength(255)
                    .HasColumnName("SECGUARDARMED");

                entity.Property(e => e.Secguardcontracted)
                    .HasMaxLength(255)
                    .HasColumnName("SECGUARDCONTRACTED");

                entity.Property(e => e.Secguardunarmed)
                    .HasMaxLength(255)
                    .HasColumnName("SECGUARDUNARMED");

                entity.Property(e => e.Sqfoot)
                    .HasMaxLength(255)
                    .HasColumnName("SQFOOT");

                entity.Property(e => e.Storeaddress1)
                    .HasMaxLength(255)
                    .HasColumnName("STOREADDRESS1");

                entity.Property(e => e.Storeaddress2)
                    .HasMaxLength(255)
                    .HasColumnName("STOREADDRESS2");

                entity.Property(e => e.Storebuilt)
                    .HasMaxLength(255)
                    .HasColumnName("STOREBUILT");

                entity.Property(e => e.Storecity)
                    .HasMaxLength(255)
                    .HasColumnName("STORECITY");

                entity.Property(e => e.Storecounty)
                    .HasMaxLength(255)
                    .HasColumnName("STORECOUNTY");

                entity.Property(e => e.Storestcd)
                    .HasMaxLength(255)
                    .HasColumnName("STORESTCD");

                entity.Property(e => e.Storetype)
                    .HasMaxLength(255)
                    .HasColumnName("STORETYPE");

                entity.Property(e => e.Storezip)
                    .HasMaxLength(255)
                    .HasColumnName("STOREZIP");

                entity.Property(e => e.Verfopername)
                    .HasMaxLength(255)
                    .HasColumnName("VERFOPERNAME");

                entity.Property(e => e._24hrop)
                    .HasMaxLength(255)
                    .HasColumnName("24HROP");
            });

            modelBuilder.Entity<Bixler2021Warehouse>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Bixler_2021_warehouse", "dbo");

                entity.Property(e => e.OperatorEid)
                    .HasMaxLength(255)
                    .HasColumnName("operator eid");

                entity.Property(e => e.Warehouseaddress1)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEADDRESS1");

                entity.Property(e => e.Warehouseaddress2)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEADDRESS2");

                entity.Property(e => e.Warehousebuildingvalue)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEBUILDINGVALUE");

                entity.Property(e => e.Warehousecity)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSECITY");

                entity.Property(e => e.Warehouseconstrutiontype)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSECONSTRUTIONTYPE");

                entity.Property(e => e.Warehousecontents)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSECONTENTS");

                entity.Property(e => e.Warehousecontentvalue)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSECONTENTVALUE");

                entity.Property(e => e.Warehouseifown)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEIFOWN");

                entity.Property(e => e.Warehousemortgages)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEMORTGAGES");

                entity.Property(e => e.Warehousename)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSENAME");

                entity.Property(e => e.Warehouseofficehas)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEOFFICEHAS");

                entity.Property(e => e.Warehouseownrentbuilding)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEOWNRENTBUILDING");

                entity.Property(e => e.Warehousesqft)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSESQFT");

                entity.Property(e => e.Warehousestcd)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSESTCD");

                entity.Property(e => e.Warehouseyearbuilt)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEYEARBUILT");

                entity.Property(e => e.Warehousezip)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEZIP");
            });

            modelBuilder.Entity<CatModelResult>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("cat_model_results", "dbo");

                entity.Property(e => e.AalEq)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("aal_eq");

                entity.Property(e => e.AalHurricane)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("aal_hurricane");

                entity.Property(e => e.AalScsHigh)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("aal_scs_high");

                entity.Property(e => e.AalScsLow)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("aal_scs_low");

                entity.Property(e => e.Address1)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("address1");

                entity.Property(e => e.CatLocNum).HasColumnName("cat_loc_num");

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("city");

                entity.Property(e => e.County)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("county");

                entity.Property(e => e.DeductibleEq)
                    .HasColumnType("decimal(9, 6)")
                    .HasColumnName("deductible_eq");

                entity.Property(e => e.DeductibleHurricane)
                    .HasColumnType("decimal(9, 6)")
                    .HasColumnName("deductible_hurricane");

                entity.Property(e => e.DeductibleScs)
                    .HasColumnType("decimal(9, 6)")
                    .HasColumnName("deductible_scs");

                entity.Property(e => e.DistanceToCoast)
                    .HasColumnType("decimal(10, 6)")
                    .HasColumnName("distance_to_coast");

                entity.Property(e => e.GeocodePrecision)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("geocode_precision");

                entity.Property(e => e.ModelTiv)
                    .HasColumnType("decimal(12, 4)")
                    .HasColumnName("model_tiv");

                entity.Property(e => e.StateCode)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("state_code")
                    .IsFixedLength(true);

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("zip_code")
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<CoverageMap>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("coverage_map", "dbo");

                entity.Property(e => e.Coverage)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("coverage");

                entity.Property(e => e.CoverageCode).HasColumnName("coverage_code");

                entity.Property(e => e.CoverageDescription)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("coverage_description");
            });

            modelBuilder.Entity<CyAssumption>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("cy_assumptions", "dbo");

                entity.Property(e => e.CpCdfAy)
                    .HasColumnType("decimal(8, 6)")
                    .HasColumnName("cp_cdf_ay");

                entity.Property(e => e.CpTrendCumulative)
                    .HasColumnType("decimal(8, 6)")
                    .HasColumnName("cp_trend_cumulative");

                entity.Property(e => e.Cy).HasColumnName("cy");

                entity.Property(e => e.GlCdfAy)
                    .HasColumnType("decimal(8, 6)")
                    .HasColumnName("gl_cdf_ay");

                entity.Property(e => e.GlTrendCumulative)
                    .HasColumnType("decimal(8, 6)")
                    .HasColumnName("gl_trend_cumulative");

                entity.Property(e => e.StoreCount).HasColumnName("store_count");
            });

            modelBuilder.Entity<ElpiSirlimitOverride>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ELPiSIRLimitOverride", "dbo");

                entity.HasIndex(e => e.KontrolNbr, "UX01_ELPiLimitOverride")
                    .IsUnique()
                    .IsClustered();

                entity.Property(e => e.ActionRequiredTxt)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLine1)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CityNm)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyNm)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CurrentSiryear)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasColumnName("CurrentSIRYear")
                    .IsFixedLength(true);

                entity.Property(e => e.CurrentYearSirlimit)
                    .HasColumnType("decimal(19, 2)")
                    .HasColumnName("CurrentYearSIRLimit");

                entity.Property(e => e.ExceptionReasonTxt)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.KontrolNbr)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.OperatorNm)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PostalCd)
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.PreviousYearSirlimit)
                    .HasColumnType("decimal(19, 2)")
                    .HasColumnName("PreviousYearSIRLimit");

                entity.Property(e => e.StateCd)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<EpliException>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("EPLiExceptions", "dbo");

                entity.HasIndex(e => e.KontrolNbr, "UX01_EPLiExceptions")
                    .IsUnique()
                    .IsClustered();

                entity.Property(e => e.ActionRequired)
                    .HasMaxLength(255)
                    .HasColumnName("Action Required ");

                entity.Property(e => e.Address).HasMaxLength(255);

                entity.Property(e => e.City).HasMaxLength(255);

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(255)
                    .HasColumnName("Company Name");

                entity.Property(e => e.ExceptionReason)
                    .HasMaxLength(255)
                    .HasColumnName("Exception_Reason");

                entity.Property(e => e.KontrolNbr).HasColumnName("Kontrol Nbr");

                entity.Property(e => e.OperatorName)
                    .HasMaxLength(255)
                    .HasColumnName("Operator Name");

                entity.Property(e => e.State).HasMaxLength(255);

                entity.Property(e => e._2021Sir)
                    .HasColumnType("money")
                    .HasColumnName("2021 SIR");

                entity.Property(e => e._2022Sir)
                    .HasColumnType("money")
                    .HasColumnName("2022 SIR");
            });

            modelBuilder.Entity<FloodMapping>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("flood_mapping", "dbo");

                entity.Property(e => e.FloodZone)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("flood_zone");

                entity.Property(e => e.FloodZoneFixed)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("flood_zone_fixed")
                    .IsFixedLength(true);

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.InsertDt)
                    .HasColumnType("datetime")
                    .HasColumnName("insert_dt")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.JsonTxt)
                    .IsUnicode(false)
                    .HasColumnName("json_txt");

                entity.Property(e => e.MinDist)
                    .HasColumnType("decimal(38, 20)")
                    .HasColumnName("min_dist");

                entity.Property(e => e.StoreNum)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("store_num");
            });

            modelBuilder.Entity<FloodMappingBkup7232021>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("flood_mapping_bkup7232021", "dbo");

                entity.Property(e => e.FloodZone)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("flood_zone");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.InsertDt)
                    .HasColumnType("datetime")
                    .HasColumnName("insert_dt");

                entity.Property(e => e.JsonTxt)
                    .IsUnicode(false)
                    .HasColumnName("json_txt");

                entity.Property(e => e.MinDist)
                    .HasColumnType("decimal(38, 20)")
                    .HasColumnName("min_dist");

                entity.Property(e => e.StoreNum)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("store_num");
            });

            modelBuilder.Entity<FloodZoneMapping>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("flood_zone_mapping", "dbo");

                entity.Property(e => e.FloodZone)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("flood_zone");

                entity.Property(e => e.FloodZoneShort)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("flood_zone_short")
                    .IsFixedLength(true);

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.Rank).HasColumnName("rank");
            });

            modelBuilder.Entity<FloodZonesByStore>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("flood_zones_by_store", "dbo");

                entity.Property(e => e.FloodZone)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("flood_zone");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.StoreNum)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("store_num");
            });

            modelBuilder.Entity<FranchisedStores2020>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("FranchisedStores2020", "dbo");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("ADDRESS");

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("city");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("phone_number");

                entity.Property(e => e.State)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("STATE");

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.Zip)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("zip");

                entity.Property(e => e.Zip5).HasColumnName("zip5");
            });

            modelBuilder.Entity<GeneratedXml>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("GeneratedXML", "dbo");

                entity.Property(e => e.ApplicationXml)
                    .IsRequired()
                    .HasColumnType("xml")
                    .HasColumnName("ApplicationXML");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.OperatorEid)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("OperatorEID");
            });

            modelBuilder.Entity<GoogleAddressDatum>(entity =>
            {
                entity.ToTable("GoogleAddressData", "dbo");

                entity.Property(e => e.GoogleResponseJson).IsUnicode(false);

                entity.Property(e => e.Lat).HasColumnType("decimal(38, 12)");

                entity.Property(e => e.Long).HasColumnType("decimal(38, 12)");
            });

            modelBuilder.Entity<IsoGlStategroup>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("iso_gl_stategroups", "dbo");

                entity.Property(e => e.Abbreviation)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.State)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StateGroup)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LobSummary>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("lob_summary", "dbo");

                entity.Property(e => e.MappedLob)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("MappedLOB");

                entity.Property(e => e.PolicyYear)
                    .HasMaxLength(255)
                    .HasColumnName("Policy Year");

                entity.Property(e => e.TotalIncurred).HasColumnType("money");

                entity.Property(e => e.TotalPaid).HasColumnType("money");
            });

            modelBuilder.Entity<LocationListWithStoreType>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("location_list_with_store_type", "dbo");

                entity.Property(e => e.Addr1)
                    .HasMaxLength(255)
                    .HasColumnName("addr 1");

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .HasColumnName("city");

                entity.Property(e => e.County)
                    .HasMaxLength(255)
                    .HasColumnName("county");

                entity.Property(e => e.Latitude)
                    .HasMaxLength(255)
                    .HasColumnName("latitude");

                entity.Property(e => e.Longitude)
                    .HasMaxLength(255)
                    .HasColumnName("longitude");

                entity.Property(e => e.PstlCd)
                    .HasMaxLength(255)
                    .HasColumnName("pstl cd");

                entity.Property(e => e.RespOwnerEid)
                    .HasMaxLength(255)
                    .HasColumnName("resp owner eid");

                entity.Property(e => e.SiteTypeDesc)
                    .HasMaxLength(255)
                    .HasColumnName("site type desc");

                entity.Property(e => e.StCd)
                    .HasMaxLength(255)
                    .HasColumnName("st cd");

                entity.Property(e => e.Store).HasColumnName("Store # ");

                entity.Property(e => e.Unknown)
                    .HasMaxLength(255)
                    .HasColumnName("unknown");
            });

            modelBuilder.Entity<LocationType>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("location_type", "dbo");

                entity.Property(e => e.LocationType1)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("location_type");

                entity.Property(e => e.SiteTypeDesc)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("site type desc");
            });

            modelBuilder.Entity<Lossruns20142018>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("lossruns_2014_2018", "dbo");

                entity.Property(e => e.AccidentDescription).HasColumnName("Accident_Description");

                entity.Property(e => e.ClaimClosedDate)
                    .HasColumnType("date")
                    .HasColumnName("Claim Closed Date");

                entity.Property(e => e.ClaimOpenDate)
                    .HasColumnType("date")
                    .HasColumnName("Claim Open Date");

                entity.Property(e => e.ClaimStatus)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Status");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.IncurredIndemnity)
                    .HasColumnType("money")
                    .HasColumnName("Incurred_Indemnity");

                entity.Property(e => e.IncurredTotal)
                    .HasColumnType("money")
                    .HasColumnName("Incurred_Total");

                entity.Property(e => e.LossDate)
                    .HasColumnType("date")
                    .HasColumnName("Loss_date");

                entity.Property(e => e.LossState)
                    .HasMaxLength(255)
                    .HasColumnName("Loss State");

                entity.Property(e => e.OriginalLineOfBusinsess)
                    .HasMaxLength(255)
                    .HasColumnName("Original Line Of Businsess");

                entity.Property(e => e.PaidRecovery)
                    .HasColumnType("money")
                    .HasColumnName("Paid_Recovery");

                entity.Property(e => e.PaidTotal)
                    .HasColumnType("money")
                    .HasColumnName("Paid_Total");

                entity.Property(e => e.PolicyYear)
                    .HasMaxLength(255)
                    .HasColumnName("Policy Year");

                entity.Property(e => e.ValuationDate).HasColumnType("date");
            });

            modelBuilder.Entity<Lossruns2018>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("lossruns_2018", "dbo");

                entity.Property(e => e.AccidentDescription).HasColumnName("Accident_Description");

                entity.Property(e => e.AccountGroupCode)
                    .HasMaxLength(255)
                    .HasColumnName("Account_Group_Code");

                entity.Property(e => e.ClaimNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Number");

                entity.Property(e => e.ClaimStatus)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Status");

                entity.Property(e => e.Coverage).HasMaxLength(255);

                entity.Property(e => e.EntityId)
                    .HasMaxLength(255)
                    .HasColumnName("Entity_Id");

                entity.Property(e => e.IncurredIndemnity)
                    .HasColumnType("money")
                    .HasColumnName("Incurred_Indemnity");

                entity.Property(e => e.IncurredTotal)
                    .HasColumnType("money")
                    .HasColumnName("Incurred_Total");

                entity.Property(e => e.KontrolNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Kontrol_Number");

                entity.Property(e => e.LossDate)
                    .HasColumnType("datetime")
                    .HasColumnName("Loss_date");

                entity.Property(e => e.OwnerFirstName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_First_Name");

                entity.Property(e => e.OwnerId)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Id");

                entity.Property(e => e.OwnerLastName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Last_Name");

                entity.Property(e => e.OwnerName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Name");

                entity.Property(e => e.PaidRecovery)
                    .HasColumnType("money")
                    .HasColumnName("Paid_Recovery");

                entity.Property(e => e.PaidTotal)
                    .HasColumnType("money")
                    .HasColumnName("Paid_Total");

                entity.Property(e => e.PolicyYear)
                    .HasMaxLength(255)
                    .HasColumnName("Policy Year");
            });

            modelBuilder.Entity<Lossruns2019>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("lossruns_2019", "dbo");

                entity.Property(e => e.AccidentDescription).HasColumnName("Accident_Description");

                entity.Property(e => e.AccountGroupCode)
                    .HasMaxLength(255)
                    .HasColumnName("Account_Group_Code");

                entity.Property(e => e.ClaimNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Number");

                entity.Property(e => e.ClaimStatus)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Status");

                entity.Property(e => e.Coverage).HasMaxLength(255);

                entity.Property(e => e.EntityId)
                    .HasMaxLength(255)
                    .HasColumnName("Entity_Id");

                entity.Property(e => e.IncurredIndemnity)
                    .HasColumnType("money")
                    .HasColumnName("Incurred_Indemnity");

                entity.Property(e => e.IncurredTotal)
                    .HasColumnType("money")
                    .HasColumnName("Incurred_Total");

                entity.Property(e => e.KontrolNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Kontrol_Number");

                entity.Property(e => e.LossDate)
                    .HasColumnType("datetime")
                    .HasColumnName("Loss_date");

                entity.Property(e => e.OwnerFirstName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_First_Name");

                entity.Property(e => e.OwnerId)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Id");

                entity.Property(e => e.OwnerLastName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Last_Name");

                entity.Property(e => e.OwnerName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Name");

                entity.Property(e => e.PaidRecovery)
                    .HasColumnType("money")
                    .HasColumnName("Paid_Recovery");

                entity.Property(e => e.PaidTotal)
                    .HasColumnType("money")
                    .HasColumnName("Paid_Total");

                entity.Property(e => e.PolicyYear)
                    .HasMaxLength(255)
                    .HasColumnName("Policy Year");
            });

            modelBuilder.Entity<Lossruns2020>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("lossruns_2020", "dbo");

                entity.Property(e => e.AccidentDescription)
                    .HasMaxLength(255)
                    .HasColumnName("Accident_Description");

                entity.Property(e => e.AccountGroupCode)
                    .HasMaxLength(255)
                    .HasColumnName("Account_Group_Code");

                entity.Property(e => e.ClaimNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Number");

                entity.Property(e => e.ClaimStatus)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Status");

                entity.Property(e => e.Coverage).HasMaxLength(255);

                entity.Property(e => e.EntityId)
                    .HasMaxLength(255)
                    .HasColumnName("Entity_Id");

                entity.Property(e => e.IncurredIndemnity)
                    .HasColumnType("money")
                    .HasColumnName("Incurred_Indemnity");

                entity.Property(e => e.IncurredTotal)
                    .HasColumnType("money")
                    .HasColumnName("Incurred_Total");

                entity.Property(e => e.KontrolNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Kontrol_Number");

                entity.Property(e => e.LossDate)
                    .HasColumnType("datetime")
                    .HasColumnName("Loss_date");

                entity.Property(e => e.OwnerFirstName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_First_Name");

                entity.Property(e => e.OwnerId)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Id");

                entity.Property(e => e.OwnerLastName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Last_Name");

                entity.Property(e => e.OwnerName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Name");

                entity.Property(e => e.PaidRecovery)
                    .HasColumnType("money")
                    .HasColumnName("Paid_Recovery");

                entity.Property(e => e.PaidTotal)
                    .HasColumnType("money")
                    .HasColumnName("Paid_Total");

                entity.Property(e => e.PolicyYear)
                    .HasMaxLength(255)
                    .HasColumnName("Policy Year");
            });

            modelBuilder.Entity<Lossruns202012>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("lossruns_2020_12", "dbo");

                entity.Property(e => e.AccidentDescription)
                    .HasMaxLength(2000)
                    .HasColumnName("Accident_Description");

                entity.Property(e => e.AccountGroupCode)
                    .HasMaxLength(255)
                    .HasColumnName("Account_Group_Code");

                entity.Property(e => e.ClaimClosedDate)
                    .HasColumnType("date")
                    .HasColumnName("Claim Closed Date");

                entity.Property(e => e.ClaimNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Number");

                entity.Property(e => e.ClaimOpenDate)
                    .HasColumnType("date")
                    .HasColumnName("Claim Open Date");

                entity.Property(e => e.ClaimStatus)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Status");

                entity.Property(e => e.Coverage).HasMaxLength(255);

                entity.Property(e => e.EntityId)
                    .HasMaxLength(255)
                    .HasColumnName("Entity_Id");

                entity.Property(e => e.IncurredIndemnity)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Incurred_Indemnity");

                entity.Property(e => e.IncurredTotal)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Incurred_Total");

                entity.Property(e => e.KontrolNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Kontrol_Number");

                entity.Property(e => e.LossDate)
                    .HasColumnType("date")
                    .HasColumnName("Loss_date");

                entity.Property(e => e.OwnerFirstName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_First_Name");

                entity.Property(e => e.OwnerId)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Id");

                entity.Property(e => e.OwnerLastName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Last_Name");

                entity.Property(e => e.OwnerName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Name");

                entity.Property(e => e.PaidRecovery)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Paid_Recovery");

                entity.Property(e => e.PaidTotal)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("Paid_Total");

                entity.Property(e => e.PolicyYear)
                    .HasMaxLength(255)
                    .HasColumnName("Policy Year");
            });

            modelBuilder.Entity<Lossruns2021>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("lossruns_2021", "dbo");

                entity.Property(e => e.AccidentDescription).HasColumnName("Accident_Description");

                entity.Property(e => e.AccountGroupCode)
                    .HasMaxLength(255)
                    .HasColumnName("Account_Group_Code");

                entity.Property(e => e.Broker)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Carrier).HasMaxLength(255);

                entity.Property(e => e.ClaimNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Number");

                entity.Property(e => e.ClaimStatus)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Status");

                entity.Property(e => e.EntityId)
                    .HasMaxLength(255)
                    .HasColumnName("Entity_Id");

                entity.Property(e => e.IncurredIndemnity).HasColumnName("Incurred_Indemnity");

                entity.Property(e => e.IncurredTotal).HasColumnName("Incurred_Total");

                entity.Property(e => e.KontrolNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Kontrol_Number");

                entity.Property(e => e.LossDate).HasColumnName("Loss_date");

                entity.Property(e => e.OwnerFirstName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_First_Name");

                entity.Property(e => e.OwnerId)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Id");

                entity.Property(e => e.OwnerLastName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Last_Name");

                entity.Property(e => e.OwnerName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Name");

                entity.Property(e => e.PaidRecovery).HasColumnName("Paid_Recovery");

                entity.Property(e => e.PaidTotal).HasColumnName("Paid_Total");

                entity.Property(e => e.PolicyEndDate).HasColumnName("Policy_End_Date");

                entity.Property(e => e.PolicyNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Policy_Number");

                entity.Property(e => e.PolicyStartDate).HasColumnName("Policy_Start_Date");

                entity.Property(e => e.PolicyYear).HasColumnName("Policy_Year");
            });

            modelBuilder.Entity<LossrunsCombined>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("lossruns_combined", "dbo");

                entity.Property(e => e.AccidentDescription).HasColumnName("Accident_Description");

                entity.Property(e => e.AccountGroupCode)
                    .HasMaxLength(255)
                    .HasColumnName("Account_Group_Code");

                entity.Property(e => e.ClaimNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Number");

                entity.Property(e => e.ClaimStatus)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Status");

                entity.Property(e => e.Coverage).HasMaxLength(255);

                entity.Property(e => e.EntityId)
                    .HasMaxLength(255)
                    .HasColumnName("Entity_Id");

                entity.Property(e => e.IncurredIndemnity).HasColumnName("Incurred_Indemnity");

                entity.Property(e => e.IncurredTotal).HasColumnName("Incurred_Total");

                entity.Property(e => e.KontrolNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Kontrol_Number");

                entity.Property(e => e.LossDate).HasColumnName("Loss_date");

                entity.Property(e => e.OwnerFirstName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_First_Name");

                entity.Property(e => e.OwnerId)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Id");

                entity.Property(e => e.OwnerLastName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Last_Name");

                entity.Property(e => e.OwnerName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Name");

                entity.Property(e => e.PaidRecovery).HasColumnName("Paid_Recovery");

                entity.Property(e => e.PaidTotal).HasColumnName("Paid_Total");

                entity.Property(e => e.PolicyYear).HasColumnName("Policy Year");

                entity.Property(e => e.Valuation)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LossrunsCombinedScrubbed>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("lossruns_combined_scrubbed", "dbo");

                entity.Property(e => e.AccidentDescription).HasColumnName("Accident_Description");

                entity.Property(e => e.AccountGroupCode)
                    .HasMaxLength(255)
                    .HasColumnName("Account_Group_Code");

                entity.Property(e => e.ClaimNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Number");

                entity.Property(e => e.ClaimNumberClean)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimStatus)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Status");

                entity.Property(e => e.Coverage).HasMaxLength(255);

                entity.Property(e => e.EntityId)
                    .HasMaxLength(255)
                    .HasColumnName("Entity_Id");

                entity.Property(e => e.IncurredIndemnity).HasColumnName("Incurred_Indemnity");

                entity.Property(e => e.IncurredTotal).HasColumnName("Incurred_Total");

                entity.Property(e => e.KontrolNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Kontrol_Number");

                entity.Property(e => e.LossDate).HasColumnName("Loss_date");

                entity.Property(e => e.OccurrenceKey)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.OwnerFirstName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_First_Name");

                entity.Property(e => e.OwnerId)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Id");

                entity.Property(e => e.OwnerLastName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Last_Name");

                entity.Property(e => e.OwnerName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Name");

                entity.Property(e => e.PaidRecovery).HasColumnName("Paid_Recovery");

                entity.Property(e => e.PaidTotal).HasColumnName("Paid_Total");

                entity.Property(e => e.PolicyYear).HasColumnName("Policy Year");

                entity.Property(e => e.Valuation)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LossrunsCombinedScrubbedClaimNumber>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("lossruns_combined_scrubbed_ClaimNumber", "dbo");

                entity.Property(e => e.AccidentDescription).HasColumnName("Accident_Description");

                entity.Property(e => e.AccountGroupCode)
                    .HasMaxLength(255)
                    .HasColumnName("Account_Group_Code");

                entity.Property(e => e.ClaimNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Number");

                entity.Property(e => e.ClaimNumberClean)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimStatus)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Status");

                entity.Property(e => e.Coverage).HasMaxLength(255);

                entity.Property(e => e.EntityId)
                    .HasMaxLength(255)
                    .HasColumnName("Entity_Id");

                entity.Property(e => e.IncurredIndemnity).HasColumnName("Incurred_Indemnity");

                entity.Property(e => e.IncurredTotal).HasColumnName("Incurred_Total");

                entity.Property(e => e.KontrolNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Kontrol_Number");

                entity.Property(e => e.LossDate).HasColumnName("Loss_date");

                entity.Property(e => e.OwnerFirstName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_First_Name");

                entity.Property(e => e.OwnerId)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Id");

                entity.Property(e => e.OwnerLastName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Last_Name");

                entity.Property(e => e.OwnerName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Name");

                entity.Property(e => e.PaidRecovery).HasColumnName("Paid_Recovery");

                entity.Property(e => e.PaidTotal).HasColumnName("Paid_Total");

                entity.Property(e => e.PolicyYear).HasColumnName("Policy Year");

                entity.Property(e => e.Valuation)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LossrunsCombinedScrubbedDescription>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("lossruns_combined_scrubbed_Description", "dbo");

                entity.Property(e => e.AccidentDescription).HasColumnName("Accident_Description");

                entity.Property(e => e.AccountGroupCode)
                    .HasMaxLength(255)
                    .HasColumnName("Account_Group_Code");

                entity.Property(e => e.ClaimNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Number");

                entity.Property(e => e.ClaimStatus)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Status");

                entity.Property(e => e.Coverage).HasMaxLength(255);

                entity.Property(e => e.EntityId)
                    .HasMaxLength(255)
                    .HasColumnName("Entity_Id");

                entity.Property(e => e.IncurredIndemnity).HasColumnName("Incurred_Indemnity");

                entity.Property(e => e.IncurredTotal).HasColumnName("Incurred_Total");

                entity.Property(e => e.KontrolNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Kontrol_Number");

                entity.Property(e => e.LossDate).HasColumnName("Loss_date");

                entity.Property(e => e.OwnerFirstName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_First_Name");

                entity.Property(e => e.OwnerId)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Id");

                entity.Property(e => e.OwnerLastName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Last_Name");

                entity.Property(e => e.OwnerName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Name");

                entity.Property(e => e.PaidRecovery).HasColumnName("Paid_Recovery");

                entity.Property(e => e.PaidTotal).HasColumnName("Paid_Total");

                entity.Property(e => e.PolicyYear).HasColumnName("Policy Year");

                entity.Property(e => e.Valuation)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LossrunsCombinedScrubbedOccurrence>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("lossruns_combined_scrubbed_Occurrence", "dbo");

                entity.Property(e => e.AccidentDescription).HasColumnName("Accident_Description");

                entity.Property(e => e.AccountGroupCode)
                    .HasMaxLength(255)
                    .HasColumnName("Account_Group_Code");

                entity.Property(e => e.ClaimNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Number");

                entity.Property(e => e.ClaimNumberClean)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimStatus)
                    .HasMaxLength(255)
                    .HasColumnName("Claim_Status");

                entity.Property(e => e.Coverage).HasMaxLength(255);

                entity.Property(e => e.EntityId)
                    .HasMaxLength(255)
                    .HasColumnName("Entity_Id");

                entity.Property(e => e.IncurredIndemnity).HasColumnName("Incurred_Indemnity");

                entity.Property(e => e.IncurredTotal).HasColumnName("Incurred_Total");

                entity.Property(e => e.KontrolNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Kontrol_Number");

                entity.Property(e => e.LossDate).HasColumnName("Loss_date");

                entity.Property(e => e.OccurrenceKey)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.OwnerFirstName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_First_Name");

                entity.Property(e => e.OwnerId)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Id");

                entity.Property(e => e.OwnerLastName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Last_Name");

                entity.Property(e => e.OwnerName)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Name");

                entity.Property(e => e.PaidRecovery).HasColumnName("Paid_Recovery");

                entity.Property(e => e.PaidTotal).HasColumnName("Paid_Total");

                entity.Property(e => e.PolicyYear).HasColumnName("Policy Year");

                entity.Property(e => e.Valuation)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LossrunsCombinedScrubbedSummary>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("lossruns_combined_scrubbed_summary", "dbo");

                entity.Property(e => e.Coverage).HasMaxLength(255);

                entity.Property(e => e.IncurredTotal).HasColumnType("money");

                entity.Property(e => e.KontrolNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Kontrol_Number");

                entity.Property(e => e.OwnerId)
                    .HasMaxLength(255)
                    .HasColumnName("Owner_Id");

                entity.Property(e => e.PolicyYear)
                    .HasMaxLength(255)
                    .HasColumnName("Policy Year");

                entity.Property(e => e.Valuation)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LossrunsCurrent>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("lossruns_current", "dbo");

                entity.Property(e => e.CatFlag)
                    .HasMaxLength(255)
                    .HasColumnName("cat_flag");

                entity.Property(e => e.ClaimNumber)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("Claim_Number");

                entity.Property(e => e.Coverage)
                    .HasMaxLength(255)
                    .HasColumnName("coverage");

                entity.Property(e => e.CpTrendCumulative)
                    .HasColumnType("decimal(8, 6)")
                    .HasColumnName("cp_trend_cumulative");

                entity.Property(e => e.GlTrendCumulative)
                    .HasColumnType("decimal(8, 6)")
                    .HasColumnName("gl_trend_cumulative");

                entity.Property(e => e.IncurredCase).HasColumnName("incurred_case");

                entity.Property(e => e.IncurredTotal).HasColumnName("incurred_total");

                entity.Property(e => e.LossDate).HasColumnName("Loss_date");

                entity.Property(e => e.PaidRecovery).HasColumnName("paid_recovery");

                entity.Property(e => e.PaidTotal).HasColumnName("paid_total");

                entity.Property(e => e.PolicyYear).HasColumnName("Policy Year");

                entity.Property(e => e.Storenumber).HasColumnName("storenumber");

                entity.Property(e => e.Valuation)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e._100kCappedIncLoss).HasColumnName("100k_capped_inc_loss");

                entity.Property(e => e._250kCappedIncLoss).HasColumnName("250k_capped_inc_loss");
            });

            modelBuilder.Entity<McDonaldsLocationType>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("McDonaldsLocationType", "Proposal");

                entity.Property(e => e.LocationStateCd)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.LocationTypeNm)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.QuoteNbr)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ModelCpCoefficient>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("model_cp_coefficients", "dbo");

                entity.HasIndex(e => e.Id, "ix_dbo_model_cp_coefficients_id");

                entity.Property(e => e.Coefficients).HasColumnName("coefficients");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Names)
                    .IsUnicode(false)
                    .HasColumnName("names");

                entity.Property(e => e.PValue).HasColumnName("p_value");

                entity.Property(e => e.StandardizedCoefficients).HasColumnName("standardized_coefficients");

                entity.Property(e => e.StdError).HasColumnName("std_error");

                entity.Property(e => e.ZValue).HasColumnName("z_value");
            });

            modelBuilder.Entity<ModelCpCoefficientsTest>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("model_cp_coefficients_test", "dbo");

                entity.HasIndex(e => e.Id, "ix_dbo_model_cp_coefficients_test_id");

                entity.Property(e => e.Coefficients).HasColumnName("coefficients");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Names)
                    .IsUnicode(false)
                    .HasColumnName("names");

                entity.Property(e => e.PValue).HasColumnName("p_value");

                entity.Property(e => e.StandardizedCoefficients).HasColumnName("standardized_coefficients");

                entity.Property(e => e.StdError).HasColumnName("std_error");

                entity.Property(e => e.ZValue).HasColumnName("z_value");
            });

            modelBuilder.Entity<ModelDataset>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("model_dataset", "dbo");

                entity.Property(e => e.AalEq)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("aal_eq");

                entity.Property(e => e.AalHurricane)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("aal_hurricane");

                entity.Property(e => e.Addbuildings)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("ADDBUILDINGS");

                entity.Property(e => e.Address1)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("address1");

                entity.Property(e => e.Alcoholuse)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ALCOHOLUSE");

                entity.Property(e => e.Annualsales)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("ANNUALSALES");

                entity.Property(e => e.Ansulsystem)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ANSULSYSTEM");

                entity.Property(e => e.AtheniumHail)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_hail");

                entity.Property(e => e.AtheniumHurricane)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_hurricane");

                entity.Property(e => e.AtheniumIce)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_ice");

                entity.Property(e => e.AtheniumRain)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_rain");

                entity.Property(e => e.AtheniumSlWind)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_slWind");

                entity.Property(e => e.AtheniumTornado)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_tornado");

                entity.Property(e => e.AtheniumWs)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_ws");

                entity.Property(e => e.Autosprinkler)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("AUTOSPRINKLER");

                entity.Property(e => e.Basement)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("BASEMENT");

                entity.Property(e => e.Burgalarmcent)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("BURGALARMCENT");

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("city");

                entity.Property(e => e.Cleanhoodduck)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("CLEANHOODDUCK");

                entity.Property(e => e.CommuteTime)
                    .HasColumnType("decimal(9, 6)")
                    .HasColumnName("commute_time");

                entity.Property(e => e.Companyname)
                    .HasMaxLength(255)
                    .HasColumnName("COMPANYNAME");

                entity.Property(e => e.Companynamestore)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("COMPANYNAMESTORE");

                entity.Property(e => e.Companytype)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("COMPANYTYPE");

                entity.Property(e => e.Constructiontype)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("CONSTRUCTIONTYPE");

                entity.Property(e => e.Delannualsales)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("DELANNUALSALES");

                entity.Property(e => e.Delesgguestcount)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("DELESGGUESTCOUNT");

                entity.Property(e => e.Drivethru)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("DRIVETHRU");

                entity.Property(e => e.Drivethruguestcount)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("DRIVETHRUGUESTCOUNT");

                entity.Property(e => e.Dtannualsales)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("DTANNUALSALES");

                entity.Property(e => e.Dtsales19).HasColumnName("DTSALES19");

                entity.Property(e => e.Estguestcount)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("ESTGUESTCOUNT");

                entity.Property(e => e.Firealarmcent)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("FIREALARMCENT");

                entity.Property(e => e.FloodZone)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.Fryerreplace)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("FRYERREPLACE");

                entity.Property(e => e.Haveplayplace)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("HAVEPLAYPLACE");

                entity.Property(e => e.If24op)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("IF24OP");

                entity.Property(e => e.Ilf250kTo1m)
                    .HasColumnType("decimal(30, 10)")
                    .HasColumnName("ILF250kTo1m");

                entity.Property(e => e.IsostateGroup)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("ISOStateGroup");

                entity.Property(e => e.JudicialProfile)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasColumnName("judicial_profile");

                entity.Property(e => e.Lastrebuild).HasColumnName("LASTREBUILD");

                entity.Property(e => e.Latitude).HasColumnType("decimal(38, 12)");

                entity.Property(e => e.LocationType)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("location_type");

                entity.Property(e => e.Longitude).HasColumnType("decimal(38, 12)");

                entity.Property(e => e.ManualLiabilityPremium).HasColumnType("decimal(38, 6)");

                entity.Property(e => e.ManualPropertyPremium).HasColumnType("decimal(38, 6)");

                entity.Property(e => e.ManualPropertyPremiumBgii)
                    .HasColumnType("decimal(38, 6)")
                    .HasColumnName("ManualPropertyPremiumBGII");

                entity.Property(e => e.ManualPropertyPremiumExclBgii)
                    .HasColumnType("decimal(38, 6)")
                    .HasColumnName("ManualPropertyPremiumExclBGII");

                entity.Property(e => e.ManualPropertyPremiumModel).HasColumnType("decimal(38, 6)");

                entity.Property(e => e.OperatorEid)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("operator eid");

                entity.Property(e => e.OperatorName)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("operator_name");

                entity.Property(e => e.OperatorStoreCount).HasColumnName("operator_store_count");

                entity.Property(e => e.Ownershipstart)
                    .HasColumnType("date")
                    .HasColumnName("OWNERSHIPSTART");

                entity.Property(e => e.Parking)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("PARKING");

                entity.Property(e => e.Playland)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("PLAYLAND");

                entity.Property(e => e.Playplace)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("PLAYPLACE");

                entity.Property(e => e.PropLimit)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("prop_limit");

                entity.Property(e => e.Region)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("region");

                entity.Property(e => e.RucaPrimary)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("RUCA_primary");

                entity.Property(e => e.RucaSecondary)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("RUCA_secondary");

                entity.Property(e => e.Seating)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("SEATING");

                entity.Property(e => e.Seccamera)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("SECCAMERA");

                entity.Property(e => e.Secguard)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("SECGUARD");

                entity.Property(e => e.SiteTypeDesc)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("site_type_desc");

                entity.Property(e => e.Sqfoot)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("SQFOOT");

                entity.Property(e => e.State)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("state")
                    .IsFixedLength(true);

                entity.Property(e => e.StoreBeginningDate).HasColumnName("storeBeginningDate");

                entity.Property(e => e.StoreEndingDate).HasColumnName("storeEndingDate");

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.StoreSource)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("store_source");

                entity.Property(e => e.Storebuilt).HasColumnName("STOREBUILT");

                entity.Property(e => e.Storetype)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("STORETYPE");

                entity.Property(e => e.Visualscore)
                    .HasColumnType("decimal(18, 0)")
                    .HasColumnName("visualscore");

                entity.Property(e => e.Zip).HasColumnName("zip");

                entity.Property(e => e._24hrop)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("24HROP");
            });

            modelBuilder.Entity<ModelGlCoefficient>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("model_gl_coefficients", "dbo");

                entity.HasIndex(e => e.Id, "ix_dbo_model_gl_coefficients_id");

                entity.Property(e => e.Coefficients).HasColumnName("coefficients");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Names)
                    .IsUnicode(false)
                    .HasColumnName("names");

                entity.Property(e => e.PValue).HasColumnName("p_value");

                entity.Property(e => e.StandardizedCoefficients).HasColumnName("standardized_coefficients");

                entity.Property(e => e.StdError).HasColumnName("std_error");

                entity.Property(e => e.ZValue).HasColumnName("z_value");
            });

            modelBuilder.Entity<ModelGlCoefficientsTest>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("model_gl_coefficients_test", "dbo");

                entity.HasIndex(e => e.Id, "ix_dbo_model_gl_coefficients_test_id");

                entity.Property(e => e.Coefficients).HasColumnName("coefficients");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Names)
                    .IsUnicode(false)
                    .HasColumnName("names");

                entity.Property(e => e.PValue).HasColumnName("p_value");

                entity.Property(e => e.StandardizedCoefficients).HasColumnName("standardized_coefficients");

                entity.Property(e => e.StdError).HasColumnName("std_error");

                entity.Property(e => e.ZValue).HasColumnName("z_value");
            });

            modelBuilder.Entity<ModelResult>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("model_results", "dbo");

                entity.HasIndex(e => e.Id, "ix_dbo_model_results_id");

                entity.Property(e => e.AalEq).HasColumnName("aal_eq");

                entity.Property(e => e.AalHurricane).HasColumnName("aal_hurricane");

                entity.Property(e => e.Address1).IsUnicode(false);

                entity.Property(e => e.AtheniumHail).HasColumnName("athenium_hail");

                entity.Property(e => e.AtheniumHurricane).HasColumnName("athenium_hurricane");

                entity.Property(e => e.AtheniumIce).HasColumnName("athenium_ice");

                entity.Property(e => e.AtheniumQuantile).HasColumnName("athenium_quantile");

                entity.Property(e => e.AtheniumRain).HasColumnName("athenium_rain");

                entity.Property(e => e.AtheniumScore).HasColumnName("athenium_score");

                entity.Property(e => e.AtheniumSlWind).HasColumnName("athenium_slWind");

                entity.Property(e => e.AtheniumTornado).HasColumnName("athenium_tornado");

                entity.Property(e => e.AtheniumWs).HasColumnName("athenium_ws");

                entity.Property(e => e.Aug2020DtsalesPct).HasColumnName("Aug2020DTSalesPct");

                entity.Property(e => e.Aug2020FranchiseeNm).IsUnicode(false);

                entity.Property(e => e.Aug2020Gc).HasColumnName("Aug2020GC");

                entity.Property(e => e.City).IsUnicode(false);

                entity.Property(e => e.CommuteTime).HasColumnName("commute_time");

                entity.Property(e => e.CompanyName)
                    .IsUnicode(false)
                    .HasColumnName("company_name");

                entity.Property(e => e.CpQuantile).HasColumnName("cp_quantile");

                entity.Property(e => e.DriveThrough)
                    .IsUnicode(false)
                    .HasColumnName("drive_through");

                entity.Property(e => e.DtGroup)
                    .IsUnicode(false)
                    .HasColumnName("dt_group");

                entity.Property(e => e.DtYear).HasColumnName("dtYear");

                entity.Property(e => e.Dthours).HasColumnName("DTHours");

                entity.Property(e => e.FloodZone).IsUnicode(false);

                entity.Property(e => e.GlClaimCount).HasColumnName("gl_claim_count");

                entity.Property(e => e.GlIncurredTotal).HasColumnName("gl_incurred_total");

                entity.Property(e => e.GlModel1).HasColumnName("gl_model_1");

                entity.Property(e => e.GlModel1Err).HasColumnName("gl_model_1_err");

                entity.Property(e => e.GlModel1Pp).HasColumnName("gl_model_1_pp");

                entity.Property(e => e.GlModelPp).HasColumnName("gl_model_pp");

                entity.Property(e => e.GlModelPrem).HasColumnName("gl_model_prem");

                entity.Property(e => e.GlQuantile).HasColumnName("gl_quantile");

                entity.Property(e => e.GlRate).HasColumnName("gl_rate");

                entity.Property(e => e.GlTrendedCappedDeveloped).HasColumnName("gl_trended_capped_developed");

                entity.Property(e => e.GlTrendedCappedTotal).HasColumnName("gl_trended_capped_total");

                entity.Property(e => e.GlTrendedDevelopedUl).HasColumnName("gl_trended_developed_ul");

                entity.Property(e => e.GlcappedToManual).HasColumnName("GLCappedToManual");

                entity.Property(e => e.GlincToManual).HasColumnName("GLIncToManual");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IndoorPlayground)
                    .IsUnicode(false)
                    .HasColumnName("indoor_playground");

                entity.Property(e => e.JudicialProfile)
                    .IsUnicode(false)
                    .HasColumnName("judicial_profile");

                entity.Property(e => e.Jul2019DtsalesPct).HasColumnName("Jul2019DTSalesPct");

                entity.Property(e => e.LocationType)
                    .IsUnicode(false)
                    .HasColumnName("location_type");

                entity.Property(e => e.ManualPp).HasColumnName("manual_pp");

                entity.Property(e => e.ManualPrem).HasColumnName("manual_prem");

                entity.Property(e => e.MobileDeals)
                    .IsUnicode(false)
                    .HasColumnName("mobile_deals");

                entity.Property(e => e.MobileOrders)
                    .IsUnicode(false)
                    .HasColumnName("mobile_orders");

                entity.Property(e => e.ModelPp).HasColumnName("model_pp");

                entity.Property(e => e.ModelPrem).HasColumnName("model_prem");

                entity.Property(e => e.ModelToManualFactor).HasColumnName("model_to_manual_factor");

                entity.Property(e => e.OutdoorPlayPlaceAvailable)
                    .IsUnicode(false)
                    .HasColumnName("outdoor_play_place_available");

                entity.Property(e => e.PropCappedToManualExclBgii).HasColumnName("PropCappedToManualExclBGII");

                entity.Property(e => e.PropClaimCount).HasColumnName("prop_claim_count");

                entity.Property(e => e.PropIncPlusCatToManual).HasColumnName("propIncPlusCatToManual");

                entity.Property(e => e.PropIncToManualExclBgii).HasColumnName("PropIncToManualExclBGII");

                entity.Property(e => e.PropIncurredTotal).HasColumnName("prop_incurred_total");

                entity.Property(e => e.PropLimit).HasColumnName("prop_limit");

                entity.Property(e => e.PropModel1).HasColumnName("prop_model_1");

                entity.Property(e => e.PropModel1Err).HasColumnName("prop_model_1_err");

                entity.Property(e => e.PropModel1Pp).HasColumnName("prop_model_1_pp");

                entity.Property(e => e.PropModelPp).HasColumnName("prop_model_pp");

                entity.Property(e => e.PropModelPrem).HasColumnName("prop_model_prem");

                entity.Property(e => e.PropRate).HasColumnName("prop_rate");

                entity.Property(e => e.PropTrendedCappedDeveloped).HasColumnName("prop_trended_capped_developed");

                entity.Property(e => e.PropTrendedCappedTotal).HasColumnName("prop_trended_capped_total");

                entity.Property(e => e.PropTrendedDevelopedUl).HasColumnName("prop_trended_developed_ul");

                entity.Property(e => e.PurePremiumPropertyBgii).HasColumnName("PurePremiumPropertyBGII");

                entity.Property(e => e.PurePremiumPropertyExclBgii).HasColumnName("PurePremiumPropertyExclBGII");

                entity.Property(e => e.Region)
                    .IsUnicode(false)
                    .HasColumnName("region");

                entity.Property(e => e.RucaPrimary)
                    .IsUnicode(false)
                    .HasColumnName("RUCA_primary");

                entity.Property(e => e.RucaSecondary)
                    .IsUnicode(false)
                    .HasColumnName("RUCA_secondary");

                entity.Property(e => e.SalesBucket).IsUnicode(false);

                entity.Property(e => e.State).IsUnicode(false);

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.Wifi)
                    .IsUnicode(false)
                    .HasColumnName("wifi");

                entity.Property(e => e.Zip).HasColumnName("ZIP");
            });

            modelBuilder.Entity<ModelResultsTest>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("model_results_test", "dbo");

                entity.HasIndex(e => e.Id, "ix_dbo_model_results_test_id");

                entity.Property(e => e.AalEq).HasColumnName("aal_eq");

                entity.Property(e => e.AalHurricane).HasColumnName("aal_hurricane");

                entity.Property(e => e.Addbuildings)
                    .IsUnicode(false)
                    .HasColumnName("ADDBUILDINGS");

                entity.Property(e => e.Address1)
                    .IsUnicode(false)
                    .HasColumnName("address1");

                entity.Property(e => e.Alcoholuse)
                    .IsUnicode(false)
                    .HasColumnName("ALCOHOLUSE");

                entity.Property(e => e.Annualsales).HasColumnName("ANNUALSALES");

                entity.Property(e => e.Ansulsystem)
                    .IsUnicode(false)
                    .HasColumnName("ANSULSYSTEM");

                entity.Property(e => e.AtheniumHail).HasColumnName("athenium_hail");

                entity.Property(e => e.AtheniumHurricane).HasColumnName("athenium_hurricane");

                entity.Property(e => e.AtheniumIce).HasColumnName("athenium_ice");

                entity.Property(e => e.AtheniumQuantile).HasColumnName("athenium_quantile");

                entity.Property(e => e.AtheniumRain).HasColumnName("athenium_rain");

                entity.Property(e => e.AtheniumScore).HasColumnName("athenium_score");

                entity.Property(e => e.AtheniumSlWind).HasColumnName("athenium_slWind");

                entity.Property(e => e.AtheniumTornado).HasColumnName("athenium_tornado");

                entity.Property(e => e.AtheniumWs).HasColumnName("athenium_ws");

                entity.Property(e => e.Autosprinkler)
                    .IsUnicode(false)
                    .HasColumnName("AUTOSPRINKLER");

                entity.Property(e => e.Basement)
                    .IsUnicode(false)
                    .HasColumnName("BASEMENT");

                entity.Property(e => e.Burgalarmcent)
                    .IsUnicode(false)
                    .HasColumnName("BURGALARMCENT");

                entity.Property(e => e.City)
                    .IsUnicode(false)
                    .HasColumnName("city");

                entity.Property(e => e.Cleanhoodduck)
                    .IsUnicode(false)
                    .HasColumnName("CLEANHOODDUCK");

                entity.Property(e => e.CommuteTime).HasColumnName("commute_time");

                entity.Property(e => e.Companyname)
                    .IsUnicode(false)
                    .HasColumnName("COMPANYNAME");

                entity.Property(e => e.Companynamestore)
                    .IsUnicode(false)
                    .HasColumnName("COMPANYNAMESTORE");

                entity.Property(e => e.Companytype)
                    .IsUnicode(false)
                    .HasColumnName("COMPANYTYPE");

                entity.Property(e => e.Constructiontype)
                    .IsUnicode(false)
                    .HasColumnName("CONSTRUCTIONTYPE");

                entity.Property(e => e.CpCappedToManual).HasColumnName("cp_capped_to_manual");

                entity.Property(e => e.CpModelPpWtd).HasColumnName("CP_model_pp_wtd");

                entity.Property(e => e.CpModelToManualFactor).HasColumnName("cp_model_to_manual_factor");

                entity.Property(e => e.CpQuantile).HasColumnName("cp_quantile");

                entity.Property(e => e.Delannualsales).HasColumnName("DELANNUALSALES");

                entity.Property(e => e.Delesgguestcount).HasColumnName("DELESGGUESTCOUNT");

                entity.Property(e => e.Drivethru)
                    .IsUnicode(false)
                    .HasColumnName("DRIVETHRU");

                entity.Property(e => e.Drivethruguestcount).HasColumnName("DRIVETHRUGUESTCOUNT");

                entity.Property(e => e.DtGroup)
                    .IsUnicode(false)
                    .HasColumnName("dt_group");

                entity.Property(e => e.Dtannualsales).HasColumnName("DTANNUALSALES");

                entity.Property(e => e.Dtsalescube).HasColumnName("DTSALESCUBE");

                entity.Property(e => e.Dtsalespct).HasColumnName("DTSALESPCT");

                entity.Property(e => e.Dtsalestrans).HasColumnName("DTSALESTRANS");

                entity.Property(e => e.Estguestcount).HasColumnName("ESTGUESTCOUNT");

                entity.Property(e => e.Firealarmcent)
                    .IsUnicode(false)
                    .HasColumnName("FIREALARMCENT");

                entity.Property(e => e.FloodZone).IsUnicode(false);

                entity.Property(e => e.Fryerreplace)
                    .IsUnicode(false)
                    .HasColumnName("FRYERREPLACE");

                entity.Property(e => e.GlCappedToManual).HasColumnName("gl_capped_to_manual");

                entity.Property(e => e.GlClaimCount).HasColumnName("gl_claim_count");

                entity.Property(e => e.GlDevTrendCapped).HasColumnName("gl_dev_trend_capped");

                entity.Property(e => e.GlIncurredTotal).HasColumnName("gl_incurred_total");

                entity.Property(e => e.GlModel1).HasColumnName("gl_model_1");

                entity.Property(e => e.GlModel1Err).HasColumnName("gl_model_1_err");

                entity.Property(e => e.GlModel1Prem).HasColumnName("gl_model_1_prem");

                entity.Property(e => e.GlModelPpWtd).HasColumnName("GL_model_pp_wtd");

                entity.Property(e => e.GlModelPrem).HasColumnName("gl_model_prem");

                entity.Property(e => e.GlModelPremIlf).HasColumnName("gl_model_prem_ilf");

                entity.Property(e => e.GlModelToManualFactor).HasColumnName("gl_model_to_manual_factor");

                entity.Property(e => e.GlQuantile).HasColumnName("gl_quantile");

                entity.Property(e => e.GlRate).HasColumnName("gl_rate");

                entity.Property(e => e.Haveplayplace)
                    .IsUnicode(false)
                    .HasColumnName("HAVEPLAYPLACE");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.If24op)
                    .IsUnicode(false)
                    .HasColumnName("IF24OP");

                entity.Property(e => e.Ilf250kTo1m).HasColumnName("ILF250kTo1m");

                entity.Property(e => e.IsostateGroup)
                    .IsUnicode(false)
                    .HasColumnName("ISOStateGroup");

                entity.Property(e => e.JudicialProfile)
                    .IsUnicode(false)
                    .HasColumnName("judicial_profile");

                entity.Property(e => e.Lastrebuild).HasColumnName("LASTREBUILD");

                entity.Property(e => e.LocationType)
                    .IsUnicode(false)
                    .HasColumnName("location_type");

                entity.Property(e => e.ManualPrem).HasColumnName("manual_prem");

                entity.Property(e => e.ManualPropertyPremiumBgii).HasColumnName("ManualPropertyPremiumBGII");

                entity.Property(e => e.ManualPropertyPremiumExclBgii).HasColumnName("ManualPropertyPremiumExclBGII");

                entity.Property(e => e.ModelPrem).HasColumnName("model_prem");

                entity.Property(e => e.ModelToManualFactor).HasColumnName("model_to_manual_factor");

                entity.Property(e => e.OperatorEid)
                    .IsUnicode(false)
                    .HasColumnName("operator eid");

                entity.Property(e => e.OperatorName)
                    .IsUnicode(false)
                    .HasColumnName("operator_name");

                entity.Property(e => e.OperatorStoreCount).HasColumnName("operator_store_count");

                entity.Property(e => e.Ownershipstart).HasColumnName("OWNERSHIPSTART");

                entity.Property(e => e.Parking).HasColumnName("PARKING");

                entity.Property(e => e.Playland)
                    .IsUnicode(false)
                    .HasColumnName("PLAYLAND");

                entity.Property(e => e.Playplace)
                    .IsUnicode(false)
                    .HasColumnName("PLAYPLACE");

                entity.Property(e => e.PropClaimCount).HasColumnName("prop_claim_count");

                entity.Property(e => e.PropDevTrendCapped).HasColumnName("prop_dev_trend_capped");

                entity.Property(e => e.PropIncurredTotal).HasColumnName("prop_incurred_total");

                entity.Property(e => e.PropLimit).HasColumnName("prop_limit");

                entity.Property(e => e.PropModel1).HasColumnName("prop_model_1");

                entity.Property(e => e.PropModel1Err).HasColumnName("prop_model_1_err");

                entity.Property(e => e.PropModel1Prem).HasColumnName("prop_model_1_prem");

                entity.Property(e => e.PropModelPrem).HasColumnName("prop_model_prem");

                entity.Property(e => e.PropRate).HasColumnName("prop_rate");

                entity.Property(e => e.Region)
                    .IsUnicode(false)
                    .HasColumnName("region");

                entity.Property(e => e.Residual).HasColumnName("residual");

                entity.Property(e => e.RoofGroup)
                    .IsUnicode(false)
                    .HasColumnName("roof_group");

                entity.Property(e => e.RucaPrimary)
                    .IsUnicode(false)
                    .HasColumnName("RUCA_primary");

                entity.Property(e => e.RucaSecondary)
                    .IsUnicode(false)
                    .HasColumnName("RUCA_secondary");

                entity.Property(e => e.Seating).HasColumnName("SEATING");

                entity.Property(e => e.Seccamera)
                    .IsUnicode(false)
                    .HasColumnName("SECCAMERA");

                entity.Property(e => e.Secguard)
                    .IsUnicode(false)
                    .HasColumnName("SECGUARD");

                entity.Property(e => e.SiteTypeDesc)
                    .IsUnicode(false)
                    .HasColumnName("site_type_desc");

                entity.Property(e => e.Sqfoot).HasColumnName("SQFOOT");

                entity.Property(e => e.State)
                    .IsUnicode(false)
                    .HasColumnName("state");

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.StoreSource)
                    .IsUnicode(false)
                    .HasColumnName("store_source");

                entity.Property(e => e.Storebuilt).HasColumnName("STOREBUILT");

                entity.Property(e => e.Storetype)
                    .IsUnicode(false)
                    .HasColumnName("STORETYPE");

                entity.Property(e => e.Visualscore).HasColumnName("visualscore");

                entity.Property(e => e.WorkingWeight).HasColumnName("working_weight");

                entity.Property(e => e.WwResidual).HasColumnName("ww_residual");

                entity.Property(e => e.Yr).HasColumnName("yr");

                entity.Property(e => e.YrWeight).HasColumnName("yr_weight");

                entity.Property(e => e.Zip).HasColumnName("zip");

                entity.Property(e => e._24hrop)
                    .IsUnicode(false)
                    .HasColumnName("24HROP");
            });

            modelBuilder.Entity<ModelStoreyear>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("model_storeyear", "dbo");

                entity.Property(e => e.AalEq)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("aal_eq");

                entity.Property(e => e.AalHurricane)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("aal_hurricane");

                entity.Property(e => e.Addbuildings)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("ADDBUILDINGS");

                entity.Property(e => e.Address1)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("address1");

                entity.Property(e => e.Alcoholuse)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ALCOHOLUSE");

                entity.Property(e => e.Annualsales)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("ANNUALSALES");

                entity.Property(e => e.Ansulsystem)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ANSULSYSTEM");

                entity.Property(e => e.AtheniumHail)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_hail");

                entity.Property(e => e.AtheniumHurricane)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_hurricane");

                entity.Property(e => e.AtheniumIce)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_ice");

                entity.Property(e => e.AtheniumRain)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_rain");

                entity.Property(e => e.AtheniumSlWind)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_slWind");

                entity.Property(e => e.AtheniumTornado)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_tornado");

                entity.Property(e => e.AtheniumWs)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_ws");

                entity.Property(e => e.Autosprinkler)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("AUTOSPRINKLER");

                entity.Property(e => e.Basement)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("BASEMENT");

                entity.Property(e => e.Burgalarmcent)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("BURGALARMCENT");

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("city");

                entity.Property(e => e.Cleanhoodduck)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("CLEANHOODDUCK");

                entity.Property(e => e.CommuteTime)
                    .HasColumnType("decimal(9, 6)")
                    .HasColumnName("commute_time");

                entity.Property(e => e.Companyname)
                    .HasMaxLength(255)
                    .HasColumnName("COMPANYNAME");

                entity.Property(e => e.Companynamestore)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("COMPANYNAMESTORE");

                entity.Property(e => e.Companytype)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("COMPANYTYPE");

                entity.Property(e => e.Constructiontype)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("CONSTRUCTIONTYPE");

                entity.Property(e => e.Delannualsales)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("DELANNUALSALES");

                entity.Property(e => e.Delesgguestcount)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("DELESGGUESTCOUNT");

                entity.Property(e => e.Drivethru)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("DRIVETHRU");

                entity.Property(e => e.Drivethruguestcount)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("DRIVETHRUGUESTCOUNT");

                entity.Property(e => e.Dtannualsales)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("DTANNUALSALES");

                entity.Property(e => e.Dtsalespct).HasColumnName("DTSALESPCT");

                entity.Property(e => e.Estguestcount)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("ESTGUESTCOUNT");

                entity.Property(e => e.Firealarmcent)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("FIREALARMCENT");

                entity.Property(e => e.FloodZone)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.Fryerreplace)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("FRYERREPLACE");

                entity.Property(e => e.GlClaimCount).HasColumnName("gl_claim_count");

                entity.Property(e => e.GlDevTrendCapped).HasColumnName("gl_dev_trend_capped");

                entity.Property(e => e.GlIncurredTotal).HasColumnName("gl_incurred_total");

                entity.Property(e => e.Haveplayplace)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("HAVEPLAYPLACE");

                entity.Property(e => e.If24op)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("IF24OP");

                entity.Property(e => e.Ilf250kTo1m)
                    .HasColumnType("decimal(30, 10)")
                    .HasColumnName("ILF250kTo1m");

                entity.Property(e => e.IsostateGroup)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("ISOStateGroup");

                entity.Property(e => e.JudicialProfile)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasColumnName("judicial_profile");

                entity.Property(e => e.Lastrebuild).HasColumnName("LASTREBUILD");

                entity.Property(e => e.Latitude).HasColumnType("decimal(38, 12)");

                entity.Property(e => e.LocationType)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("location_type");

                entity.Property(e => e.Longitude).HasColumnType("decimal(38, 12)");

                entity.Property(e => e.ManualLiabilityPremium).HasColumnType("decimal(38, 6)");

                entity.Property(e => e.ManualPropertyPremium).HasColumnType("decimal(38, 6)");

                entity.Property(e => e.ManualPropertyPremiumBgii)
                    .HasColumnType("decimal(38, 6)")
                    .HasColumnName("ManualPropertyPremiumBGII");

                entity.Property(e => e.ManualPropertyPremiumExclBgii)
                    .HasColumnType("decimal(38, 6)")
                    .HasColumnName("ManualPropertyPremiumExclBGII");

                entity.Property(e => e.ManualPropertyPremiumModel).HasColumnType("decimal(38, 6)");

                entity.Property(e => e.OperatorEid)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("operator eid");

                entity.Property(e => e.OperatorName)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("operator_name");

                entity.Property(e => e.OperatorStoreCount).HasColumnName("operator_store_count");

                entity.Property(e => e.Ownershipstart)
                    .HasColumnType("date")
                    .HasColumnName("OWNERSHIPSTART");

                entity.Property(e => e.Parking)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("PARKING");

                entity.Property(e => e.Playland)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("PLAYLAND");

                entity.Property(e => e.Playplace)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("PLAYPLACE");

                entity.Property(e => e.PropClaimCount).HasColumnName("prop_claim_count");

                entity.Property(e => e.PropDevTrendCapped).HasColumnName("prop_dev_trend_capped");

                entity.Property(e => e.PropIncurredTotal).HasColumnName("prop_incurred_total");

                entity.Property(e => e.PropLimit)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("prop_limit");

                entity.Property(e => e.Region)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("region");

                entity.Property(e => e.RucaPrimary)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("RUCA_primary");

                entity.Property(e => e.RucaSecondary)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("RUCA_secondary");

                entity.Property(e => e.Seating)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("SEATING");

                entity.Property(e => e.Seccamera)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("SECCAMERA");

                entity.Property(e => e.Secguard)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("SECGUARD");

                entity.Property(e => e.SiteTypeDesc)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("site_type_desc");

                entity.Property(e => e.Sqfoot)
                    .HasColumnType("decimal(30, 6)")
                    .HasColumnName("SQFOOT");

                entity.Property(e => e.State)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("state")
                    .IsFixedLength(true);

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.StoreSource)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("store_source");

                entity.Property(e => e.Storebuilt).HasColumnName("STOREBUILT");

                entity.Property(e => e.Storetype)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("STORETYPE");

                entity.Property(e => e.Visualscore)
                    .HasColumnType("decimal(18, 0)")
                    .HasColumnName("visualscore");

                entity.Property(e => e.Yr).HasColumnName("yr");

                entity.Property(e => e.YrWeight)
                    .HasColumnType("decimal(8, 4)")
                    .HasColumnName("yr_weight");

                entity.Property(e => e.Zip).HasColumnName("zip");

                entity.Property(e => e._24hrop)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("24HROP");
            });

            modelBuilder.Entity<OfficeAddressMapping>(entity =>
            {
                entity.ToTable("OfficeAddressMapping", "dbo");
            });

            modelBuilder.Entity<OldXmlLossRun>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("old_xml_loss_runs", "dbo");

                entity.Property(e => e.AccdntDate).HasColumnType("date");

                entity.Property(e => e.ClaimDescription)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.Claimnbr)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("claimnbr");

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("company_name");

                entity.Property(e => e.Cvg)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("cvg");

                entity.Property(e => e.ExperienceRatingInclusion)
                    .IsRequired()
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.Fn)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("fn");

                entity.Property(e => e.IncurredTotal).HasColumnType("money");

                entity.Property(e => e.PaidTotal).HasColumnType("money");

                entity.Property(e => e.Storenbr)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("storenbr");
            });

            modelBuilder.Entity<OldXmlStoreinfo>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("old_xml_storeinfo", "dbo");

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("company_name");

                entity.Property(e => e.Fn)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("fn");

                entity.Property(e => e.Ownershipstart)
                    .HasColumnType("date")
                    .HasColumnName("OWNERSHIPSTART");

                entity.Property(e => e.Statename)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("statename");

                entity.Property(e => e.StorebuiltYear).HasColumnName("storebuilt_year");

                entity.Property(e => e.Storenbr).HasColumnName("storenbr");
            });

            modelBuilder.Entity<OlobMap>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("OLOB_map", "dbo");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.MappedLob)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("MappedLOB");

                entity.Property(e => e.OriginalLineOfBusiness)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("Original Line of Business");
            });

            modelBuilder.Entity<PayloadXml>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("PayloadXML", "dbo");

                entity.Property(e => e.Fn)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("fn");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.Payload)
                    .HasColumnType("xml")
                    .HasColumnName("payload");
            });

            modelBuilder.Entity<PivotXmlgeneral>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("PivotXMLGeneral", "dbo");

                entity.Property(e => e.Altemail)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("ALTEMAIL");

                entity.Property(e => e.Cellphone)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("CELLPHONE");

                entity.Property(e => e.Claimcontact)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("CLAIMCONTACT");

                entity.Property(e => e.Claimcontactname)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("CLAIMCONTACTNAME");

                entity.Property(e => e.Claimcontactphone)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("CLAIMCONTACTPHONE");

                entity.Property(e => e.Claimssince2011)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("CLAIMSSINCE2011");

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("company_name");

                entity.Property(e => e.Companyfein)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("COMPANYFEIN");

                entity.Property(e => e.Companyname1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("COMPANYNAME");

                entity.Property(e => e.Cyberclaim)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("CYBERCLAIM");

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("EMAIL");

                entity.Property(e => e.Employliability)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("EMPLOYLIABILITY");

                entity.Property(e => e.Faxnumber)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("FAXNUMBER");

                entity.Property(e => e.Fn)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("fn");

                entity.Property(e => e.Genstoreownership)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("GENSTOREOWNERSHIP");

                entity.Property(e => e.Insurancecontact)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("INSURANCECONTACT");

                entity.Property(e => e.Mailingaddress1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("MAILINGADDRESS1");

                entity.Property(e => e.Mailingaddress2)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("MAILINGADDRESS2");

                entity.Property(e => e.Mailingcity)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("MAILINGCITY");

                entity.Property(e => e.Mailingcounty)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("MAILINGCOUNTY");

                entity.Property(e => e.Mailingstcd)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("MAILINGSTCD");

                entity.Property(e => e.Mailingzip)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("MAILINGZIP");

                entity.Property(e => e.Misc)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("MISC");

                entity.Property(e => e.Numberofstores)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("NUMBEROFSTORES");

                entity.Property(e => e.Officephone)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("OFFICEPHONE");

                entity.Property(e => e.Ownernamefirst)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("OWNERNAMEFIRST");

                entity.Property(e => e.Ownernamelast)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("OWNERNAMELAST");

                entity.Property(e => e.Ownernamemiddle)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("OWNERNAMEMIDDLE");

                entity.Property(e => e.Riseclaim)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("RISECLAIM");

                entity.Property(e => e.Securitybreach)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("SECURITYBREACH");

                entity.Property(e => e.State)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("STATE");

                entity.Property(e => e._401k)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e._401kValue)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PivotXmllossRun>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("PivotXMLLossRun", "dbo");

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("company_name");

                entity.Property(e => e.Fn)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("fn");

                entity.Property(e => e.LossRunAccdntDate)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LossRunAccdntDesc)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LossRunClaimNbr)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LossRunClaimNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LossRunCovgDesc)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LossRunIncurredTotal)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LossRunKontrolNbr)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LossRunNatlStoreNbr)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LossRunPaidTotal)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LossRunPolicyEffEndDate)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LossRunPolicyEffStartDate)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PivotXmlstore>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("PivotXMLStore", "dbo");

                entity.Property(e => e.Addbuildings)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("ADDBUILDINGS");

                entity.Property(e => e.Addbuildingsdesc)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("ADDBUILDINGSDESC");

                entity.Property(e => e.Addstoretype)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("ADDSTORETYPE");

                entity.Property(e => e.Alcoholuse)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("ALCOHOLUSE");

                entity.Property(e => e.Annualsales)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("ANNUALSALES");

                entity.Property(e => e.Ansulsystem)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("ANSULSYSTEM");

                entity.Property(e => e.Armoredcare)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("ARMOREDCARE");

                entity.Property(e => e.Autosprinkler)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("AUTOSPRINKLER");

                entity.Property(e => e.Basement)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("BASEMENT");

                entity.Property(e => e.Burgalarmcent)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("BURGALARMCENT");

                entity.Property(e => e.Cleanhoodduck)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("CLEANHOODDUCK");

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("company_name");

                entity.Property(e => e.Companynamestore)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("COMPANYNAMESTORE");

                entity.Property(e => e.Companytype)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("COMPANYTYPE");

                entity.Property(e => e.Constructiontype)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("CONSTRUCTIONTYPE");

                entity.Property(e => e.Delannualsales)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("DELANNUALSALES");

                entity.Property(e => e.Delesgguestcount)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("DELESGGUESTCOUNT");

                entity.Property(e => e.Deliver)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("DELIVER");

                entity.Property(e => e.Depositstobank)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("DEPOSITSTOBANK");

                entity.Property(e => e.Drivethru)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("DRIVETHRU");

                entity.Property(e => e.Drivethruguestcount)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("DRIVETHRUGUESTCOUNT");

                entity.Property(e => e.Estguestcount)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("ESTGUESTCOUNT");

                entity.Property(e => e.Fein)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("FEIN");

                entity.Property(e => e.Firealarmcent)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("FIREALARMCENT");

                entity.Property(e => e.Fn)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("fn");

                entity.Property(e => e.Fryerreplace)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("FRYERREPLACE");

                entity.Property(e => e.Haveplayplace)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("HAVEPLAYPLACE");

                entity.Property(e => e.If24op)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("IF24OP");

                entity.Property(e => e.Insured)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("INSURED");

                entity.Property(e => e.Lastrebuild)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("LASTREBUILD");

                entity.Property(e => e.Natlstorenbr)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("NATLSTORENBR");

                entity.Property(e => e.Newoperatornamefirst)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("NEWOPERATORNAMEFIRST");

                entity.Property(e => e.Newoperatornamelast)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("NEWOPERATORNAMELAST");

                entity.Property(e => e.Nonmcdops)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("NONMCDOPS");

                entity.Property(e => e.Operatorid)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("OPERATORID");

                entity.Property(e => e.Operatornamefirst)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("OPERATORNAMEFIRST");

                entity.Property(e => e.Operatornamelast)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("OPERATORNAMELAST");

                entity.Property(e => e.Ownershipstart)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("OWNERSHIPSTART");

                entity.Property(e => e.Parking)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("PARKING");

                entity.Property(e => e.Playland)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("PLAYLAND");

                entity.Property(e => e.Playplace)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("PLAYPLACE");

                entity.Property(e => e.Seating)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("SEATING");

                entity.Property(e => e.Seccamera)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("SECCAMERA");

                entity.Property(e => e.Seccameraindoor)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("SECCAMERAINDOOR");

                entity.Property(e => e.Seccameraoutdoor)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("SECCAMERAOUTDOOR");

                entity.Property(e => e.Secguard)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("SECGUARD");

                entity.Property(e => e.Secguardarmed)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("SECGUARDARMED");

                entity.Property(e => e.Secguardcontracted)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("SECGUARDCONTRACTED");

                entity.Property(e => e.Secguardunarmed)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("SECGUARDUNARMED");

                entity.Property(e => e.Sqfoot)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("SQFOOT");

                entity.Property(e => e.StoreId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Storeaddress1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("STOREADDRESS1");

                entity.Property(e => e.Storeaddress2)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("STOREADDRESS2");

                entity.Property(e => e.Storebuilt)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("STOREBUILT");

                entity.Property(e => e.Storecity)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("STORECITY");

                entity.Property(e => e.Storecounty)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("STORECOUNTY");

                entity.Property(e => e.Storestcd)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("STORESTCD");

                entity.Property(e => e.Storetype)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("STORETYPE");

                entity.Property(e => e.Storezip)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("STOREZIP");

                entity.Property(e => e.Verfopername)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("VERFOPERNAME");

                entity.Property(e => e._24hrop)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("_24HROP");
            });

            modelBuilder.Entity<PricingRule>(entity =>
            {
                entity.ToTable("PricingRule", "dbo");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Expression)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RoofScore>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("roof_score", "dbo");

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.Visualscore)
                    .HasColumnType("decimal(18, 0)")
                    .HasColumnName("visualscore");
            });

            modelBuilder.Entity<SalesBucketingByRegion>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("SalesBucketingByRegion", "dbo");

                entity.Property(e => e.Low).HasColumnName("low");

                entity.Property(e => e.Med).HasColumnName("med");

                entity.Property(e => e.Region)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("region");
            });

            modelBuilder.Entity<SiteSales2019V2>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("SiteSales_2019_v2", "dbo");

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("Company_Name");

                entity.Property(e => e.DtDeliverySalesAnnualized).HasColumnName("DT_Delivery_Sales___Annualized");

                entity.Property(e => e.DtDeliverySalesAnnualized1).HasColumnName("DT_Delivery_Sales_______Annualized");

                entity.Property(e => e.DtGuestCountsAnnualized).HasColumnName("DT_Guest_Counts___Annualized");

                entity.Property(e => e.DtGuestCountsAnnualized1).HasColumnName("DT_Guest_Counts_______Annualized");

                entity.Property(e => e.Feid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("FEID");

                entity.Property(e => e.Jul19TtmDtGcs).HasColumnName("Jul_19_TTM_DT_GCs__");

                entity.Property(e => e.Jul19TtmDtSls).HasColumnName("Jul_19_TTM_DT_Sls__");

                entity.Property(e => e.Jul19TtmGcs).HasColumnName("Jul_19_TTM_GCs");

                entity.Property(e => e.Jul19TtmSls)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("Jul_19_TTM_Sls");

                entity.Property(e => e.NatlStrNu).HasColumnName("Natl_Str_Nu");

                entity.Property(e => e.OperId).HasColumnName("Oper_ID");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.StSite).HasColumnName("St_Site");

                entity.Property(e => e._5DigitNsn).HasColumnName("5_digit_NSN");
            });

            modelBuilder.Entity<Sitesales2019>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("sitesales_2019", "dbo");

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("Company_Name");

                entity.Property(e => e.DtDeliverySalesAnnualized).HasColumnName("DT_Delivery_Sales___Annualized");

                entity.Property(e => e.DtDeliverySalesAnnualized1)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("DT_Delivery_Sales_______Annualized");

                entity.Property(e => e.DtGuestCountsAnnualized).HasColumnName("DT_Guest_Counts___Annualized");

                entity.Property(e => e.DtGuestCountsAnnualized1)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("DT_Guest_Counts_______Annualized");

                entity.Property(e => e.Feid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("FEID");

                entity.Property(e => e.Jul19TtmDtGcs)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("Jul_19_TTM_DT_GCs__");

                entity.Property(e => e.Jul19TtmDtSls)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("Jul_19_TTM_DT_Sls__");

                entity.Property(e => e.Jul19TtmGcs)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("Jul_19_TTM_GCs");

                entity.Property(e => e.Jul19TtmSls)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("Jul_19_TTM_Sls");

                entity.Property(e => e.NatlStrNu).HasColumnName("Natl_Str_Nu");

                entity.Property(e => e.OperId).HasColumnName("Oper_ID");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.StSite).HasColumnName("St_Site");

                entity.Property(e => e._5DigitNsn).HasColumnName("5_digit_NSN");
            });

            modelBuilder.Entity<Sitesales2020>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("sitesales_2020", "dbo");

                entity.Property(e => e.Aug20TtmDtGcs)
                    .HasColumnType("decimal(38, 4)")
                    .HasColumnName("Aug 20 TTM DT GCs %");

                entity.Property(e => e.Aug20TtmDtSls)
                    .HasColumnType("decimal(38, 4)")
                    .HasColumnName("Aug 20 TTM DT Sls %");

                entity.Property(e => e.Aug20TtmGcs)
                    .HasColumnType("money")
                    .HasColumnName("Aug 20 TTM GCs");

                entity.Property(e => e.Aug20TtmSls)
                    .HasColumnType("money")
                    .HasColumnName("Aug 20 TTM Sls");

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(255)
                    .HasColumnName("Company Name");

                entity.Property(e => e.DtDeliverySalesAnnualized)
                    .HasColumnType("money")
                    .HasColumnName("DT Delivery Sales - Annualized");

                entity.Property(e => e.DtDeliverySalesAnnualized1)
                    .HasColumnType("decimal(38, 4)")
                    .HasColumnName("DT Delivery Sales - % - Annualized");

                entity.Property(e => e.DtGuestCountsAnnualized)
                    .HasColumnType("money")
                    .HasColumnName("DT Guest Counts - Annualized");

                entity.Property(e => e.DtGuestCountsAnnualized1)
                    .HasColumnType("decimal(38, 4)")
                    .HasColumnName("DT Guest Counts - % - Annualized");

                entity.Property(e => e.Feid).HasColumnName("FEID");

                entity.Property(e => e.NatlStrNu).HasColumnName("Natl Str Nu");

                entity.Property(e => e.OperId).HasColumnName("Oper ID");

                entity.Property(e => e.Operator).HasMaxLength(255);

                entity.Property(e => e.StSite).HasColumnName("St/Site");

                entity.Property(e => e._5DigitNsn).HasColumnName("5 digit NSN");
            });

            modelBuilder.Entity<StateRegion>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("state_regions", "dbo");

                entity.Property(e => e.Ilf250kTo1m)
                    .HasColumnType("decimal(30, 10)")
                    .HasColumnName("ILF250kTo1m");

                entity.Property(e => e.IsostateGroup)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("ISOStateGroup");

                entity.Property(e => e.Region)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("region");

                entity.Property(e => e.StandardFederalRegion)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("standard_federal_region");

                entity.Property(e => e.State)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("state");

                entity.Property(e => e.StateCode)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("state_code");
            });

            modelBuilder.Entity<StoreAddressMapping>(entity =>
            {
                entity.ToTable("StoreAddressMapping", "dbo");
            });

            modelBuilder.Entity<StoreCrimeScore>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("store_crime_score", "dbo");

                entity.Property(e => e.StateCrimeCategory)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("state_crime_category");

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");
            });

            modelBuilder.Entity<StoreYearExperience>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("store_year_experience", "dbo");

                entity.Property(e => e.Address1).HasMaxLength(255);

                entity.Property(e => e.Aug2020DeliverySalesPct).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.Aug2020DtsalesPct)
                    .HasColumnType("decimal(38, 4)")
                    .HasColumnName("Aug2020DTSalesPct");

                entity.Property(e => e.Aug2020FranchiseeNm).HasMaxLength(255);

                entity.Property(e => e.Aug2020Gc)
                    .HasColumnType("money")
                    .HasColumnName("Aug2020GC");

                entity.Property(e => e.Aug2020Sales).HasColumnType("money");

                entity.Property(e => e.City).HasMaxLength(255);

                entity.Property(e => e.CreateYourTasteAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("create_your_taste_available");

                entity.Property(e => e.DriveThrough)
                    .HasMaxLength(255)
                    .HasColumnName("drive_through");

                entity.Property(e => e.DriveThruHours)
                    .HasMaxLength(255)
                    .HasColumnName("drive_thru_hours");

                entity.Property(e => e.Dt)
                    .HasColumnType("date")
                    .HasColumnName("dt");

                entity.Property(e => e.FridayDthours)
                    .HasColumnType("numeric(14, 1)")
                    .HasColumnName("FridayDTHours");

                entity.Property(e => e.FridayStoreHours).HasColumnType("numeric(14, 1)");

                entity.Property(e => e.GiftCards)
                    .HasMaxLength(255)
                    .HasColumnName("gift_cards");

                entity.Property(e => e.GlClaimCount).HasColumnName("gl_claim_count");

                entity.Property(e => e.GlIncurredCappedTotal)
                    .HasColumnType("decimal(38, 10)")
                    .HasColumnName("gl_incurred_capped_total");

                entity.Property(e => e.GlIncurredTotal)
                    .HasColumnType("money")
                    .HasColumnName("gl_incurred_total");

                entity.Property(e => e.IndoorDiningAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_dining_available");

                entity.Property(e => e.IndoorPlayground)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_playground");

                entity.Property(e => e.IndoorPlayplace)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_playplace");

                entity.Property(e => e.Latitude).HasColumnType("decimal(38, 12)");

                entity.Property(e => e.Longitude).HasColumnType("decimal(38, 12)");

                entity.Property(e => e.Mcdelivery)
                    .HasMaxLength(255)
                    .HasColumnName("mcdelivery");

                entity.Property(e => e.MobileDeals)
                    .HasMaxLength(255)
                    .HasColumnName("mobile_deals");

                entity.Property(e => e.MobileOrders)
                    .HasMaxLength(255)
                    .HasColumnName("mobile_orders");

                entity.Property(e => e.MondayDthours)
                    .HasColumnType("numeric(14, 1)")
                    .HasColumnName("MondayDTHours");

                entity.Property(e => e.MondayStoreHours).HasColumnType("numeric(14, 1)");

                entity.Property(e => e.OutdoorPlayPlaceAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("outdoor_play_place_available");

                entity.Property(e => e.PropClaimCount).HasColumnName("prop_claim_count");

                entity.Property(e => e.PropIncurredCappedTotal)
                    .HasColumnType("decimal(38, 10)")
                    .HasColumnName("prop_incurred_capped_total");

                entity.Property(e => e.PropIncurredTotal)
                    .HasColumnType("money")
                    .HasColumnName("prop_incurred_total");

                entity.Property(e => e.SaturdayDthours)
                    .HasColumnType("numeric(14, 1)")
                    .HasColumnName("SaturdayDTHours");

                entity.Property(e => e.SaturdayStoreHours).HasColumnType("numeric(14, 1)");

                entity.Property(e => e.SiteTypeDesc)
                    .HasMaxLength(255)
                    .HasColumnName("site type desc");

                entity.Property(e => e.State).HasMaxLength(255);

                entity.Property(e => e.StoreHours)
                    .HasMaxLength(255)
                    .HasColumnName("store_hours");

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.StoreNumberDt).HasColumnName("store_number_dt");

                entity.Property(e => e.Storebeginningdate)
                    .HasMaxLength(9)
                    .IsUnicode(false)
                    .HasColumnName("storebeginningdate");

                entity.Property(e => e.Storeendingdate)
                    .HasMaxLength(9)
                    .IsUnicode(false)
                    .HasColumnName("storeendingdate");

                entity.Property(e => e.SundayDthours)
                    .HasColumnType("numeric(14, 1)")
                    .HasColumnName("SundayDTHours");

                entity.Property(e => e.SundayStoreHours).HasColumnType("numeric(14, 1)");

                entity.Property(e => e.ThursdayDthours)
                    .HasColumnType("numeric(14, 1)")
                    .HasColumnName("ThursdayDTHours");

                entity.Property(e => e.ThursdayStoreHours).HasColumnType("numeric(14, 1)");

                entity.Property(e => e.TuesdayDthours)
                    .HasColumnType("numeric(14, 1)")
                    .HasColumnName("TuesdayDTHours");

                entity.Property(e => e.TuesdayStoreHours).HasColumnType("numeric(14, 1)");

                entity.Property(e => e.WalmartLocation)
                    .HasMaxLength(255)
                    .HasColumnName("walmart_location");

                entity.Property(e => e.WednesdayDthours)
                    .HasColumnType("numeric(14, 1)")
                    .HasColumnName("WednesdayDTHours");

                entity.Property(e => e.WednesdayStoreHours).HasColumnType("numeric(14, 1)");

                entity.Property(e => e.Wifi)
                    .HasMaxLength(255)
                    .HasColumnName("wifi");

                entity.Property(e => e.Zip).HasColumnName("ZIP");
            });

            modelBuilder.Entity<TieringRule>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tiering_rules", "dbo");

                entity.Property(e => e.CpTierFactor)
                    .HasColumnType("decimal(3, 2)")
                    .HasColumnName("cp_tier_factor");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("date")
                    .HasColumnName("created_date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EffDate)
                    .HasColumnType("date")
                    .HasColumnName("eff_date");

                entity.Property(e => e.GlTierFactor)
                    .HasColumnType("decimal(3, 2)")
                    .HasColumnName("gl_tier_factor");

                entity.Property(e => e.TierNo).HasColumnName("tier_no");
            });

            modelBuilder.Entity<VotingDatum>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("voting_data", "dbo");

                entity.Property(e => e.Candidate)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("candidate");

                entity.Property(e => e.Candidatevotes).HasColumnName("candidatevotes");

                entity.Property(e => e.CountyFips)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("county_fips");

                entity.Property(e => e.CountyName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("county_name");

                entity.Property(e => e.Mode)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("mode");

                entity.Property(e => e.Office)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("office");

                entity.Property(e => e.Party)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("party");

                entity.Property(e => e.State)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("state");

                entity.Property(e => e.StatePo)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("state_po");

                entity.Property(e => e.Totalvotes).HasColumnName("totalvotes");

                entity.Property(e => e.Version)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("version");

                entity.Property(e => e.Year)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("year");
            });

            modelBuilder.Entity<Vw50QuoteSummary>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw50QuoteSummary", "dbo");

                entity.Property(e => e.BasicGlpremium)
                    .HasColumnType("numeric(38, 11)")
                    .HasColumnName("BasicGLPremium");

                entity.Property(e => e.BasicPkgPremium).HasColumnType("decimal(38, 20)");

                entity.Property(e => e.BasicPremPremium).HasColumnType("numeric(38, 11)");

                entity.Property(e => e.BasicProdPremium).HasColumnType("numeric(38, 11)");

                entity.Property(e => e.BatchNum).HasColumnName("BATCH_NUM");

                entity.Property(e => e.ClientId)
                    .HasColumnType("decimal(15, 0)")
                    .HasColumnName("CLIENT_ID");

                entity.Property(e => e.CpincurredLossRuns)
                    .HasColumnType("money")
                    .HasColumnName("CPIncurredLossRuns");

                entity.Property(e => e.GlexperienceRatingEligible)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("GLExperienceRatingEligible");

                entity.Property(e => e.GlincurredLossRuns)
                    .HasColumnType("money")
                    .HasColumnName("GLIncurredLossRuns");

                entity.Property(e => e.InsuredName)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("INSURED_NAME");

                entity.Property(e => e.PolicyNbrSeq)
                    .HasColumnType("decimal(15, 0)")
                    .HasColumnName("POLICY_NBR_SEQ");

                entity.Property(e => e.PolicyNo)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("POLICY_NO");

                entity.Property(e => e.PolicySymbolC)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("POLICY_SYMBOL_C");

                entity.Property(e => e.TotalCppremium)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("TotalCPPremium");

                entity.Property(e => e.TotalGlpremium)
                    .HasColumnType("decimal(38, 2)")
                    .HasColumnName("TotalGLPremium");

                entity.Property(e => e.TotalPkgPremium).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.Ver)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VwConsolidatedLocationExposure>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwConsolidatedLocationExposures", "dbo");

                entity.Property(e => e.Address1).HasMaxLength(255);

                entity.Property(e => e.Aug2020DeliverySalesPct).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.Aug2020DtsalesPct)
                    .HasColumnType("decimal(38, 4)")
                    .HasColumnName("Aug2020DTSalesPct");

                entity.Property(e => e.Aug2020FranchiseeNm).HasMaxLength(255);

                entity.Property(e => e.Aug2020Gc)
                    .HasColumnType("money")
                    .HasColumnName("Aug2020GC");

                entity.Property(e => e.Aug2020Sales).HasColumnType("money");

                entity.Property(e => e.City).HasMaxLength(255);

                entity.Property(e => e.CreateYourTasteAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("create_your_taste_available");

                entity.Property(e => e.DriveThrough)
                    .HasMaxLength(255)
                    .HasColumnName("drive_through");

                entity.Property(e => e.DriveThruHours)
                    .HasMaxLength(255)
                    .HasColumnName("drive_thru_hours");

                entity.Property(e => e.GiftCards)
                    .HasMaxLength(255)
                    .HasColumnName("gift_cards");

                entity.Property(e => e.IndoorDiningAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_dining_available");

                entity.Property(e => e.IndoorPlayground)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_playground");

                entity.Property(e => e.IndoorPlayplace)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_playplace");

                entity.Property(e => e.Latitude).HasColumnType("decimal(38, 12)");

                entity.Property(e => e.Longitude).HasColumnType("decimal(38, 12)");

                entity.Property(e => e.Mcdelivery)
                    .HasMaxLength(255)
                    .HasColumnName("mcdelivery");

                entity.Property(e => e.MobileDeals)
                    .HasMaxLength(255)
                    .HasColumnName("mobile_deals");

                entity.Property(e => e.MobileOrders)
                    .HasMaxLength(255)
                    .HasColumnName("mobile_orders");

                entity.Property(e => e.OutdoorPlayPlaceAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("outdoor_play_place_available");

                entity.Property(e => e.SiteTypeDesc)
                    .HasMaxLength(255)
                    .HasColumnName("site type desc");

                entity.Property(e => e.State).HasMaxLength(255);

                entity.Property(e => e.StoreHours)
                    .HasMaxLength(255)
                    .HasColumnName("store_hours");

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.WalmartLocation)
                    .HasMaxLength(255)
                    .HasColumnName("walmart_location");

                entity.Property(e => e.Wifi)
                    .HasMaxLength(255)
                    .HasColumnName("wifi");

                entity.Property(e => e.Zip).HasColumnName("ZIP");
            });

            modelBuilder.Entity<VwGenesys50Quote>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwGenesys50Quotes", "dbo");

                entity.Property(e => e.BatchNum).HasColumnName("BATCH_NUM");

                entity.Property(e => e.Cpdisc)
                    .HasColumnType("decimal(11, 2)")
                    .HasColumnName("CPDisc");

                entity.Property(e => e.Cptier)
                    .HasColumnType("decimal(11, 2)")
                    .HasColumnName("CPTier");

                entity.Property(e => e.Gldisc)
                    .HasColumnType("decimal(11, 2)")
                    .HasColumnName("GLDisc");

                entity.Property(e => e.Gltier)
                    .HasColumnType("decimal(11, 2)")
                    .HasColumnName("GLTier");

                entity.Property(e => e.LiabilityPremium).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.ManualLiabilityPremium).HasColumnType("decimal(38, 6)");

                entity.Property(e => e.ManualPropertyPremium).HasColumnType("decimal(38, 6)");

                entity.Property(e => e.ManualPropertyPremiumExclBgii)
                    .HasColumnType("decimal(38, 6)")
                    .HasColumnName("ManualPropertyPremiumExclBGII");

                entity.Property(e => e.PolicyNo)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("POLICY_NO");

                entity.Property(e => e.PropertyPremium).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.PropertyPremiumExclBgii)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("PropertyPremiumExclBGII");

                entity.Property(e => e.UnitAddress)
                    .HasMaxLength(163)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_ADDRESS");

                entity.Property(e => e.UnitCity)
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_CITY");

                entity.Property(e => e.UnitDesc)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_DESC");

                entity.Property(e => e.UnitState)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_STATE");

                entity.Property(e => e.UnitType)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.UnitZipcode)
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_ZIPCODE");
            });

            modelBuilder.Entity<VwGenesysQuote>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwGenesysQuotes", "dbo");

                entity.Property(e => e.BatchNum).HasColumnName("BATCH_NUM");

                entity.Property(e => e.Cpdisc)
                    .HasColumnType("decimal(11, 2)")
                    .HasColumnName("CPDisc");

                entity.Property(e => e.Cptier)
                    .HasColumnType("decimal(11, 2)")
                    .HasColumnName("CPTier");

                entity.Property(e => e.EarthquakePremium).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.FloodPremium).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.Gldisc)
                    .HasColumnType("decimal(11, 2)")
                    .HasColumnName("GLDisc");

                entity.Property(e => e.Gltier)
                    .HasColumnType("decimal(11, 2)")
                    .HasColumnName("GLTier");

                entity.Property(e => e.LiabilityPremium).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.ManualLiabilityPremium).HasColumnType("decimal(38, 6)");

                entity.Property(e => e.ManualPropertyPremium).HasColumnType("decimal(38, 6)");

                entity.Property(e => e.ManualPropertyPremiumExclBgii)
                    .HasColumnType("decimal(38, 6)")
                    .HasColumnName("ManualPropertyPremiumExclBGII");

                entity.Property(e => e.PolicyNo)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("POLICY_NO");

                entity.Property(e => e.PropertyBpplimit)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("PropertyBPPLimit");

                entity.Property(e => e.PropertyBuildingLimit).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.PropertyPremium).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.PropertyPremiumExclBgii)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("PropertyPremiumExclBGII");

                entity.Property(e => e.PropertyTelimit)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("PropertyTELimit");

                entity.Property(e => e.UnitAddress)
                    .HasMaxLength(163)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_ADDRESS");

                entity.Property(e => e.UnitCity)
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_CITY");

                entity.Property(e => e.UnitDesc)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_DESC");

                entity.Property(e => e.UnitState)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_STATE");

                entity.Property(e => e.UnitType)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.UnitZipcode)
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_ZIPCODE");
            });

            modelBuilder.Entity<VwGenesysQuotesAlBeforererate>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vwGenesysQuotes_AL_beforererate", "dbo");

                entity.Property(e => e.BatchNum).HasColumnName("BATCH_NUM");

                entity.Property(e => e.Cpdisc)
                    .HasColumnType("decimal(11, 2)")
                    .HasColumnName("CPDisc");

                entity.Property(e => e.Cptier)
                    .HasColumnType("decimal(11, 2)")
                    .HasColumnName("CPTier");

                entity.Property(e => e.Gldisc)
                    .HasColumnType("decimal(11, 2)")
                    .HasColumnName("GLDisc");

                entity.Property(e => e.Gltier)
                    .HasColumnType("decimal(11, 2)")
                    .HasColumnName("GLTier");

                entity.Property(e => e.LiabilityPremium).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.ManualLiabilityPremium).HasColumnType("decimal(38, 6)");

                entity.Property(e => e.ManualPropertyPremium).HasColumnType("decimal(38, 6)");

                entity.Property(e => e.ManualPropertyPremiumExclBgii)
                    .HasColumnType("decimal(38, 6)")
                    .HasColumnName("ManualPropertyPremiumExclBGII");

                entity.Property(e => e.PolicyNo)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("POLICY_NO");

                entity.Property(e => e.PropertyBpplimit)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("PropertyBPPLimit");

                entity.Property(e => e.PropertyBuildingLimit).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.PropertyPremium).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.PropertyPremiumExclBgii)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("PropertyPremiumExclBGII");

                entity.Property(e => e.PropertyTelimit)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("PropertyTELimit");

                entity.Property(e => e.UnitAddress)
                    .HasMaxLength(163)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_ADDRESS");

                entity.Property(e => e.UnitCity)
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_CITY");

                entity.Property(e => e.UnitDesc)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_DESC");

                entity.Property(e => e.UnitState)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_STATE");

                entity.Property(e => e.UnitType)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.UnitZipcode)
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_ZIPCODE");
            });

            modelBuilder.Entity<VwGenesysQuotesVaBeforererate>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vwGenesysQuotes_VA_beforererate", "dbo");

                entity.Property(e => e.BatchNum).HasColumnName("BATCH_NUM");

                entity.Property(e => e.Cpdisc)
                    .HasColumnType("decimal(11, 2)")
                    .HasColumnName("CPDisc");

                entity.Property(e => e.Cptier)
                    .HasColumnType("decimal(11, 2)")
                    .HasColumnName("CPTier");

                entity.Property(e => e.Gldisc)
                    .HasColumnType("decimal(11, 2)")
                    .HasColumnName("GLDisc");

                entity.Property(e => e.Gltier)
                    .HasColumnType("decimal(11, 2)")
                    .HasColumnName("GLTier");

                entity.Property(e => e.LiabilityPremium).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.ManualLiabilityPremium).HasColumnType("decimal(38, 6)");

                entity.Property(e => e.ManualPropertyPremium).HasColumnType("decimal(38, 6)");

                entity.Property(e => e.ManualPropertyPremiumExclBgii)
                    .HasColumnType("decimal(38, 6)")
                    .HasColumnName("ManualPropertyPremiumExclBGII");

                entity.Property(e => e.PolicyNo)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("POLICY_NO");

                entity.Property(e => e.PropertyBpplimit)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("PropertyBPPLimit");

                entity.Property(e => e.PropertyBuildingLimit).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.PropertyPremium).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.PropertyPremiumExclBgii)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("PropertyPremiumExclBGII");

                entity.Property(e => e.PropertyTelimit)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("PropertyTELimit");

                entity.Property(e => e.UnitAddress)
                    .HasMaxLength(163)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_ADDRESS");

                entity.Property(e => e.UnitCity)
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_CITY");

                entity.Property(e => e.UnitDesc)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_DESC");

                entity.Property(e => e.UnitState)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_STATE");

                entity.Property(e => e.UnitType)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.UnitZipcode)
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .HasColumnName("UNIT_ZIPCODE");
            });

            modelBuilder.Entity<VwLocationSale>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwLocationSales", "dbo");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .HasColumnName("address");

                entity.Property(e => e.BldgTiv).HasColumnName("BLDG_TIV");

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .HasColumnName("city");

                entity.Property(e => e.Country)
                    .HasMaxLength(255)
                    .HasColumnName("country");

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(255)
                    .HasColumnName("country_code");

                entity.Property(e => e.County)
                    .HasMaxLength(255)
                    .HasColumnName("county");

                entity.Property(e => e.CreateYourTasteAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("create_your_taste_available");

                entity.Property(e => e.DriveThrough)
                    .HasMaxLength(255)
                    .HasColumnName("drive_through");

                entity.Property(e => e.DriveThruHours)
                    .HasMaxLength(255)
                    .HasColumnName("drive_thru_hours");

                entity.Property(e => e.GeoAccuracy)
                    .HasMaxLength(255)
                    .HasColumnName("geo_accuracy");

                entity.Property(e => e.GiftCards)
                    .HasMaxLength(255)
                    .HasColumnName("gift_cards");

                entity.Property(e => e.IndoorDiningAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_dining_available");

                entity.Property(e => e.IndoorPlayground)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_playground");

                entity.Property(e => e.IndoorPlayplace)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_playplace");

                entity.Property(e => e.Latitude)
                    .HasColumnType("decimal(38, 12)")
                    .HasColumnName("latitude");

                entity.Property(e => e.Longitude)
                    .HasColumnType("decimal(38, 12)")
                    .HasColumnName("longitude");

                entity.Property(e => e.Mcdelivery)
                    .HasMaxLength(255)
                    .HasColumnName("mcdelivery");

                entity.Property(e => e.MobileDeals)
                    .HasMaxLength(255)
                    .HasColumnName("mobile_deals");

                entity.Property(e => e.MobileOrders)
                    .HasMaxLength(255)
                    .HasColumnName("mobile_orders");

                entity.Property(e => e.OutdoorPlayPlaceAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("outdoor_play_place_available");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(255)
                    .HasColumnName("phone_number");

                entity.Property(e => e.Services)
                    .HasMaxLength(255)
                    .HasColumnName("services");

                entity.Property(e => e.SiteTypeDesc)
                    .HasMaxLength(255)
                    .HasColumnName("site type desc");

                entity.Property(e => e.State)
                    .HasMaxLength(255)
                    .HasColumnName("state");

                entity.Property(e => e.StoreHours)
                    .HasMaxLength(255)
                    .HasColumnName("store_hours");

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.TotalSales).HasColumnType("money");

                entity.Property(e => e.WalmartLocation)
                    .HasMaxLength(255)
                    .HasColumnName("walmart_location");

                entity.Property(e => e.Wifi)
                    .HasMaxLength(255)
                    .HasColumnName("wifi");

                entity.Property(e => e.ZipCode).HasColumnName("zip_code");
            });

            modelBuilder.Entity<VwMissingLossesCurrentValuation>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwMissingLossesCurrentValuation", "dbo");

                entity.Property(e => e.Count2018).HasColumnName("count_2018");

                entity.Property(e => e.Count2020).HasColumnName("count_2020");

                entity.Property(e => e.Incurred2018)
                    .HasColumnType("money")
                    .HasColumnName("incurred_2018");

                entity.Property(e => e.Incurred2020)
                    .HasColumnType("money")
                    .HasColumnName("incurred_2020");

                entity.Property(e => e.KontrolNumber)
                    .HasMaxLength(255)
                    .HasColumnName("Kontrol_Number");

                entity.Property(e => e.LossDate)
                    .HasColumnType("datetime")
                    .HasColumnName("Loss_date");
            });

            modelBuilder.Entity<VwStoreBeginEnd>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwStoreBeginEnd", "dbo");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .HasColumnName("address");

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .HasColumnName("city");

                entity.Property(e => e.State)
                    .HasMaxLength(255)
                    .HasColumnName("state");

                entity.Property(e => e.StoreBeginningDate)
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.StoreEndingDate)
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.ZipCode).HasColumnName("zip_code");
            });

            modelBuilder.Entity<VwStoreHistory>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwStoreHistory", "dbo");

                entity.Property(e => e.ActiveDt)
                    .IsRequired()
                    .HasMaxLength(9)
                    .IsUnicode(false)
                    .HasColumnName("active_dt");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .HasColumnName("address");

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .HasColumnName("city");

                entity.Property(e => e.Country)
                    .HasMaxLength(255)
                    .HasColumnName("country");

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(255)
                    .HasColumnName("country_code");

                entity.Property(e => e.County)
                    .HasMaxLength(255)
                    .HasColumnName("county");

                entity.Property(e => e.Latitude)
                    .HasColumnType("decimal(38, 12)")
                    .HasColumnName("latitude");

                entity.Property(e => e.Longitude)
                    .HasColumnType("decimal(38, 12)")
                    .HasColumnName("longitude");

                entity.Property(e => e.State)
                    .HasMaxLength(255)
                    .HasColumnName("state");

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.ZipCode).HasColumnName("zip_code");
            });

            modelBuilder.Entity<VwStoreYear>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwStoreYear", "dbo");

                entity.Property(e => e.AalEq)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("aal_eq");

                entity.Property(e => e.AalHurricane)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("aal_hurricane");

                entity.Property(e => e.Address1).HasMaxLength(255);

                entity.Property(e => e.AtheniumHail)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_hail");

                entity.Property(e => e.AtheniumHurricane)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_hurricane");

                entity.Property(e => e.AtheniumIce)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_ice");

                entity.Property(e => e.AtheniumRain)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_rain");

                entity.Property(e => e.AtheniumSlWind)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_slWind");

                entity.Property(e => e.AtheniumTornado)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_tornado");

                entity.Property(e => e.AtheniumWs)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_ws");

                entity.Property(e => e.Aug2020DeliverySalesPct).HasColumnType("decimal(38, 4)");

                entity.Property(e => e.Aug2020DtsalesPct)
                    .HasColumnType("decimal(38, 4)")
                    .HasColumnName("Aug2020DTSalesPct");

                entity.Property(e => e.Aug2020FranchiseeNm).HasMaxLength(255);

                entity.Property(e => e.Aug2020Gc)
                    .HasColumnType("money")
                    .HasColumnName("Aug2020GC");

                entity.Property(e => e.Aug2020Sales).HasColumnType("money");

                entity.Property(e => e.City).HasMaxLength(255);

                entity.Property(e => e.CommuteTime)
                    .HasColumnType("decimal(9, 6)")
                    .HasColumnName("commute_time");

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("company_name");

                entity.Property(e => e.DriveThrough)
                    .HasMaxLength(255)
                    .HasColumnName("drive_through");

                entity.Property(e => e.DtYear).HasColumnName("dtYear");

                entity.Property(e => e.Dthours)
                    .HasColumnType("numeric(38, 1)")
                    .HasColumnName("DTHours");

                entity.Property(e => e.FloodZone)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.GlClaimCount).HasColumnName("gl_claim_count");

                entity.Property(e => e.GlIncurredTotal)
                    .HasColumnType("money")
                    .HasColumnName("gl_incurred_total");

                entity.Property(e => e.GlTrendedCappedDeveloped)
                    .HasColumnType("decimal(38, 7)")
                    .HasColumnName("gl_trended_capped_developed");

                entity.Property(e => e.GlTrendedCappedTotal)
                    .HasColumnType("decimal(38, 10)")
                    .HasColumnName("gl_trended_capped_total");

                entity.Property(e => e.GlTrendedDevelopedUl)
                    .HasColumnType("decimal(37, 16)")
                    .HasColumnName("gl_trended_developed_ul");

                entity.Property(e => e.GlcappedToManual)
                    .HasColumnType("numeric(38, 6)")
                    .HasColumnName("GLCappedToManual");

                entity.Property(e => e.GlincToManual)
                    .HasColumnType("numeric(38, 11)")
                    .HasColumnName("GLIncToManual");

                entity.Property(e => e.Ilf250kTo1m)
                    .HasColumnType("decimal(30, 10)")
                    .HasColumnName("ILF250kTo1m");

                entity.Property(e => e.IndoorPlayground)
                    .HasMaxLength(255)
                    .HasColumnName("indoor_playground");

                entity.Property(e => e.IsostateGroup)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("ISOStateGroup");

                entity.Property(e => e.JudicialProfile)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasColumnName("judicial_profile");

                entity.Property(e => e.Jul2019DtsalesPct).HasColumnName("Jul2019DTSalesPct");

                entity.Property(e => e.Latitude).HasColumnType("decimal(38, 12)");

                entity.Property(e => e.LocationType)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("location_type");

                entity.Property(e => e.Longitude).HasColumnType("decimal(38, 12)");

                entity.Property(e => e.MobileDeals)
                    .HasMaxLength(255)
                    .HasColumnName("mobile_deals");

                entity.Property(e => e.MobileOrders)
                    .HasMaxLength(255)
                    .HasColumnName("mobile_orders");

                entity.Property(e => e.OutdoorPlayPlaceAvailable)
                    .HasMaxLength(255)
                    .HasColumnName("outdoor_play_place_available");

                entity.Property(e => e.PropCappedToManualExclBgii)
                    .HasColumnType("numeric(38, 6)")
                    .HasColumnName("PropCappedToManualExclBGII");

                entity.Property(e => e.PropClaimCount).HasColumnName("prop_claim_count");

                entity.Property(e => e.PropIncPlusCatToManual)
                    .HasColumnType("numeric(38, 16)")
                    .HasColumnName("propIncPlusCatToManual");

                entity.Property(e => e.PropIncToManualExclBgii)
                    .HasColumnType("numeric(38, 11)")
                    .HasColumnName("PropIncToManualExclBGII");

                entity.Property(e => e.PropIncurredTotal)
                    .HasColumnType("money")
                    .HasColumnName("prop_incurred_total");

                entity.Property(e => e.PropLimit)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("prop_limit");

                entity.Property(e => e.PropTrendedCappedDeveloped)
                    .HasColumnType("decimal(38, 7)")
                    .HasColumnName("prop_trended_capped_developed");

                entity.Property(e => e.PropTrendedCappedTotal)
                    .HasColumnType("decimal(38, 10)")
                    .HasColumnName("prop_trended_capped_total");

                entity.Property(e => e.PropTrendedDevelopedUl)
                    .HasColumnType("decimal(37, 16)")
                    .HasColumnName("prop_trended_developed_ul");

                entity.Property(e => e.PurePremiumLiability).HasColumnType("numeric(38, 6)");

                entity.Property(e => e.PurePremiumProperty).HasColumnType("numeric(38, 6)");

                entity.Property(e => e.PurePremiumPropertyBgii)
                    .HasColumnType("numeric(38, 6)")
                    .HasColumnName("PurePremiumPropertyBGII");

                entity.Property(e => e.PurePremiumPropertyExclBgii)
                    .HasColumnType("numeric(38, 6)")
                    .HasColumnName("PurePremiumPropertyExclBGII");

                entity.Property(e => e.PurePremiumPropertyModel).HasColumnType("numeric(38, 6)");

                entity.Property(e => e.Region)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("region");

                entity.Property(e => e.RucaPrimary)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("RUCA_primary");

                entity.Property(e => e.RucaSecondary)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("RUCA_secondary");

                entity.Property(e => e.SalesBucket)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.State).HasMaxLength(255);

                entity.Property(e => e.StoreHours).HasColumnType("numeric(38, 1)");

                entity.Property(e => e.StoreNumber).HasColumnName("store_number");

                entity.Property(e => e.Wifi)
                    .HasMaxLength(255)
                    .HasColumnName("wifi");

                entity.Property(e => e.Zip).HasColumnName("ZIP");
            });

            modelBuilder.Entity<VwVerifyScrub>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwVerifyScrub", "dbo");

                entity.Property(e => e.ScrubOffbal)
                    .HasColumnType("money")
                    .HasColumnName("scrub_offbal");

                entity.Property(e => e.Valuation)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WarehouseAddressMapping>(entity =>
            {
                entity.ToTable("WarehouseAddressMapping", "dbo");
            });

            modelBuilder.Entity<XmlAccount>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("xml_accounts", "dbo");

                entity.HasIndex(e => e.Id, "ix_dbo_xml_accounts_id");

                entity.Property(e => e.CompanyName)
                    .IsUnicode(false)
                    .HasColumnName("company_name");

                entity.Property(e => e.DomicileState)
                    .IsUnicode(false)
                    .HasColumnName("domicile_state");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.OperatorName)
                    .IsUnicode(false)
                    .HasColumnName("operator_name");

                entity.Property(e => e.OwnerCount).HasColumnName("owner_count");

                entity.Property(e => e.OwnershipStart)
                    .IsUnicode(false)
                    .HasColumnName("ownership_start");

                entity.Property(e => e.StoreCount)
                    .IsUnicode(false)
                    .HasColumnName("store_count");
            });

            modelBuilder.Entity<XmlGeneralPrepivot>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("xml_general_prepivot", "dbo");

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("company_name");

                entity.Property(e => e.Fn)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("fn");

                entity.Property(e => e.QuestionAnswer)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.QuestionCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.QuestionId)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XmlLossrunPrepivot>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("xml_lossrun_prepivot", "dbo");

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("company_name");

                entity.Property(e => e.Fn)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("fn");

                entity.Property(e => e.LossRunClaimNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.QuestionAnswer)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.QuestionCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XmlStore>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("xml_stores", "dbo");

                entity.HasIndex(e => e.Id, "ix_dbo_xml_stores_id");

                entity.Property(e => e.CompanyName)
                    .IsUnicode(false)
                    .HasColumnName("company_name");

                entity.Property(e => e.DelPct).IsUnicode(false);

                entity.Property(e => e.Dtpct)
                    .IsUnicode(false)
                    .HasColumnName("DTPct");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ownershipstart)
                    .IsUnicode(false)
                    .HasColumnName("OWNERSHIPSTART");

                entity.Property(e => e.Sales)
                    .IsUnicode(false)
                    .HasColumnName("sales");

                entity.Property(e => e.SiteTypeDesc)
                    .IsUnicode(false)
                    .HasColumnName("site type desc");

                entity.Property(e => e.Statename)
                    .IsUnicode(false)
                    .HasColumnName("statename");

                entity.Property(e => e.StoreNumber)
                    .IsUnicode(false)
                    .HasColumnName("store_number");

                entity.Property(e => e.Zip)
                    .IsUnicode(false)
                    .HasColumnName("zip");
            });

            modelBuilder.Entity<XmlStorePrepivot>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("xml_store_prepivot", "dbo");

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("company_name");

                entity.Property(e => e.Fn)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("fn");

                entity.Property(e => e.QuestionAnswer)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.QuestionCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.QuestionId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StoreId)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ZipDatum>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("zip_data", "dbo");

                entity.Property(e => e.AtheniumHail)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_hail");

                entity.Property(e => e.AtheniumHurricane)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_hurricane");

                entity.Property(e => e.AtheniumIce)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_ice");

                entity.Property(e => e.AtheniumRain)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_rain");

                entity.Property(e => e.AtheniumSlWind)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_slWind");

                entity.Property(e => e.AtheniumTornado)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_tornado");

                entity.Property(e => e.AtheniumWs)
                    .HasColumnType("decimal(3, 1)")
                    .HasColumnName("athenium_ws");

                entity.Property(e => e.CommuteError)
                    .HasColumnType("decimal(9, 6)")
                    .HasColumnName("commute_error");

                entity.Property(e => e.CommuteTime)
                    .HasColumnType("decimal(9, 6)")
                    .HasColumnName("commute_time");

                entity.Property(e => e.County)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("county");

                entity.Property(e => e.JudicialProfile)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasColumnName("judicial_profile");

                entity.Property(e => e.Latitude)
                    .HasColumnType("decimal(8, 6)")
                    .HasColumnName("latitude");

                entity.Property(e => e.Longitude)
                    .HasColumnType("decimal(9, 6)")
                    .HasColumnName("longitude");

                entity.Property(e => e.PlaceName)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("place_name");

                entity.Property(e => e.Ruca1).HasColumnName("RUCA1");

                entity.Property(e => e.RucaPrimary)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("RUCA_primary");

                entity.Property(e => e.RucaSecondary)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("RUCA_secondary");

                entity.Property(e => e.StateCode)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("state_code")
                    .IsFixedLength(true);

                entity.Property(e => e.Zip)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("zip")
                    .IsFixedLength(true);

                entity.Property(e => e.ZipType)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("zip_type");
            });

            modelBuilder.Entity<_2021General>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("2021_general", "dbo");

                entity.Property(e => e.Altemail)
                    .HasMaxLength(255)
                    .HasColumnName("ALTEMAIL");

                entity.Property(e => e.Cellphone)
                    .HasMaxLength(255)
                    .HasColumnName("CELLPHONE");

                entity.Property(e => e.Claimcontact)
                    .HasMaxLength(255)
                    .HasColumnName("CLAIMCONTACT");

                entity.Property(e => e.Claimcontactname)
                    .HasMaxLength(255)
                    .HasColumnName("CLAIMCONTACTNAME");

                entity.Property(e => e.Claimcontactphone)
                    .HasMaxLength(255)
                    .HasColumnName("CLAIMCONTACTPHONE");

                entity.Property(e => e.Companyfein)
                    .HasMaxLength(255)
                    .HasColumnName("COMPANYFEIN");

                entity.Property(e => e.Companyname)
                    .HasMaxLength(255)
                    .HasColumnName("COMPANYNAME");

                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .HasColumnName("EMAIL");

                entity.Property(e => e.Faxnumber)
                    .HasMaxLength(255)
                    .HasColumnName("FAXNUMBER");

                entity.Property(e => e.Genstoreownership)
                    .HasMaxLength(255)
                    .HasColumnName("GENSTOREOWNERSHIP");

                entity.Property(e => e.Insurancecontact)
                    .HasMaxLength(255)
                    .HasColumnName("INSURANCECONTACT");

                entity.Property(e => e.Mailingaddress1)
                    .HasMaxLength(255)
                    .HasColumnName("MAILINGADDRESS1");

                entity.Property(e => e.Mailingaddress2)
                    .HasMaxLength(255)
                    .HasColumnName("MAILINGADDRESS2");

                entity.Property(e => e.Mailingcity)
                    .HasMaxLength(255)
                    .HasColumnName("MAILINGCITY");

                entity.Property(e => e.Mailingcounty)
                    .HasMaxLength(255)
                    .HasColumnName("MAILINGCOUNTY");

                entity.Property(e => e.Mailingstcd)
                    .HasMaxLength(255)
                    .HasColumnName("MAILINGSTCD");

                entity.Property(e => e.Mailingzip)
                    .HasMaxLength(255)
                    .HasColumnName("MAILINGZIP");

                entity.Property(e => e.Numberofstores)
                    .HasMaxLength(255)
                    .HasColumnName("NUMBEROFSTORES");

                entity.Property(e => e.Officephone)
                    .HasMaxLength(255)
                    .HasColumnName("OFFICEPHONE");

                entity.Property(e => e.OperatorEid)
                    .HasMaxLength(255)
                    .HasColumnName("operator eid");

                entity.Property(e => e.Ownernamefirst)
                    .HasMaxLength(255)
                    .HasColumnName("OWNERNAMEFIRST");

                entity.Property(e => e.Ownernamelast)
                    .HasMaxLength(255)
                    .HasColumnName("OWNERNAMELAST");

                entity.Property(e => e.Ownernamemiddle)
                    .HasMaxLength(255)
                    .HasColumnName("OWNERNAMEMIDDLE");

                entity.Property(e => e.State)
                    .HasMaxLength(255)
                    .HasColumnName("STATE");
            });

            modelBuilder.Entity<_2021Office>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("2021_office", "dbo");

                entity.Property(e => e.Buildingvalue)
                    .HasMaxLength(255)
                    .HasColumnName("BUILDINGVALUE");

                entity.Property(e => e.Construtiontype)
                    .HasMaxLength(255)
                    .HasColumnName("CONSTRUTIONTYPE");

                entity.Property(e => e.Contents)
                    .HasMaxLength(255)
                    .HasColumnName("CONTENTS");

                entity.Property(e => e.Contentvalue)
                    .HasMaxLength(255)
                    .HasColumnName("CONTENTVALUE");

                entity.Property(e => e.Desctenants)
                    .HasMaxLength(255)
                    .HasColumnName("DESCTENANTS");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Ifown)
                    .HasMaxLength(255)
                    .HasColumnName("IFOWN");

                entity.Property(e => e.Officeaddress1)
                    .HasMaxLength(255)
                    .HasColumnName("OFFICEADDRESS1");

                entity.Property(e => e.Officeaddress2)
                    .HasMaxLength(255)
                    .HasColumnName("OFFICEADDRESS2");

                entity.Property(e => e.Officecity)
                    .HasMaxLength(255)
                    .HasColumnName("OFFICECITY");

                entity.Property(e => e.Officehas)
                    .HasMaxLength(255)
                    .HasColumnName("OFFICEHAS");

                entity.Property(e => e.Officename)
                    .HasMaxLength(255)
                    .HasColumnName("OFFICENAME");

                entity.Property(e => e.Officestcd)
                    .HasMaxLength(255)
                    .HasColumnName("OFFICESTCD");

                entity.Property(e => e.Officezip)
                    .HasMaxLength(255)
                    .HasColumnName("OFFICEZIP");

                entity.Property(e => e.OperatorEid)
                    .HasMaxLength(255)
                    .HasColumnName("operator eid");

                entity.Property(e => e.Ownrentbuilding)
                    .HasMaxLength(255)
                    .HasColumnName("OWNRENTBUILDING");

                entity.Property(e => e.Sqft)
                    .HasMaxLength(255)
                    .HasColumnName("SQFT");

                entity.Property(e => e.Yearbuilt)
                    .HasMaxLength(255)
                    .HasColumnName("YEARBUILT");
            });

            modelBuilder.Entity<_2021Store>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("2021_store", "dbo");

                entity.Property(e => e.Addbuildings)
                    .HasMaxLength(255)
                    .HasColumnName("ADDBUILDINGS");

                entity.Property(e => e.Addbuildingsdesc)
                    .HasMaxLength(255)
                    .HasColumnName("ADDBUILDINGSDESC");

                entity.Property(e => e.Addstoretype)
                    .HasMaxLength(255)
                    .HasColumnName("ADDSTORETYPE");

                entity.Property(e => e.Alcoholuse)
                    .HasMaxLength(255)
                    .HasColumnName("ALCOHOLUSE");

                entity.Property(e => e.Annualsales)
                    .HasMaxLength(255)
                    .HasColumnName("ANNUALSALES");

                entity.Property(e => e.Ansulsystem)
                    .HasMaxLength(255)
                    .HasColumnName("ANSULSYSTEM");

                entity.Property(e => e.Armoredcare)
                    .HasMaxLength(255)
                    .HasColumnName("ARMOREDCARE");

                entity.Property(e => e.Autosprinkler)
                    .HasMaxLength(255)
                    .HasColumnName("AUTOSPRINKLER");

                entity.Property(e => e.Basement)
                    .HasMaxLength(255)
                    .HasColumnName("BASEMENT");

                entity.Property(e => e.Burgalarmcent)
                    .HasMaxLength(255)
                    .HasColumnName("BURGALARMCENT");

                entity.Property(e => e.Cleanhoodduck)
                    .HasMaxLength(255)
                    .HasColumnName("CLEANHOODDUCK");

                entity.Property(e => e.Companynamestore)
                    .HasMaxLength(255)
                    .HasColumnName("COMPANYNAMESTORE");

                entity.Property(e => e.Companytype)
                    .HasMaxLength(255)
                    .HasColumnName("COMPANYTYPE");

                entity.Property(e => e.Constructiontype)
                    .HasMaxLength(255)
                    .HasColumnName("CONSTRUCTIONTYPE");

                entity.Property(e => e.Delannualsales)
                    .HasMaxLength(255)
                    .HasColumnName("DELANNUALSALES");

                entity.Property(e => e.Delavgrad)
                    .HasMaxLength(255)
                    .HasColumnName("DELAVGRAD");

                entity.Property(e => e.Deldrilic)
                    .HasMaxLength(255)
                    .HasColumnName("DELDRILIC");

                entity.Property(e => e.Deldrirec)
                    .HasMaxLength(255)
                    .HasColumnName("DELDRIREC");

                entity.Property(e => e.Deldrivehrec)
                    .HasMaxLength(255)
                    .HasColumnName("DELDRIVEHREC");

                entity.Property(e => e.Delemp)
                    .HasMaxLength(255)
                    .HasColumnName("DELEMP");

                entity.Property(e => e.Delesgguestcount)
                    .HasMaxLength(255)
                    .HasColumnName("DELESGGUESTCOUNT");

                entity.Property(e => e.Delinscar)
                    .HasMaxLength(255)
                    .HasColumnName("DELINSCAR");

                entity.Property(e => e.Deliver)
                    .HasMaxLength(255)
                    .HasColumnName("DELIVER");

                entity.Property(e => e.Delmore5)
                    .HasMaxLength(255)
                    .HasColumnName("DELMORE5");

                entity.Property(e => e.Delseccar)
                    .HasMaxLength(255)
                    .HasColumnName("DELSECCAR");

                entity.Property(e => e.Delwho)
                    .HasMaxLength(255)
                    .HasColumnName("DELWHO");

                entity.Property(e => e.Delwriproc)
                    .HasMaxLength(255)
                    .HasColumnName("DELWRIPROC");

                entity.Property(e => e.Depositstobank)
                    .HasMaxLength(255)
                    .HasColumnName("DEPOSITSTOBANK");

                entity.Property(e => e.Drivethru)
                    .HasMaxLength(255)
                    .HasColumnName("DRIVETHRU");

                entity.Property(e => e.Drivethruguestcount)
                    .HasMaxLength(255)
                    .HasColumnName("DRIVETHRUGUESTCOUNT");

                entity.Property(e => e.Estguestcount)
                    .HasMaxLength(255)
                    .HasColumnName("ESTGUESTCOUNT");

                entity.Property(e => e.Fein)
                    .HasMaxLength(255)
                    .HasColumnName("FEIN");

                entity.Property(e => e.Firealarmcent)
                    .HasMaxLength(255)
                    .HasColumnName("FIREALARMCENT");

                entity.Property(e => e.Fryerreplace)
                    .HasMaxLength(255)
                    .HasColumnName("FRYERREPLACE");

                entity.Property(e => e.Haveplayplace)
                    .HasMaxLength(255)
                    .HasColumnName("HAVEPLAYPLACE");

                entity.Property(e => e.If24op)
                    .HasMaxLength(255)
                    .HasColumnName("IF24OP");

                entity.Property(e => e.Ifnonmcdops)
                    .HasMaxLength(255)
                    .HasColumnName("IFNONMCDOPS");

                entity.Property(e => e.Insured)
                    .HasMaxLength(255)
                    .HasColumnName("INSURED");

                entity.Property(e => e.Lastrebuild)
                    .HasMaxLength(255)
                    .HasColumnName("LASTREBUILD");

                entity.Property(e => e.Liqctl)
                    .HasMaxLength(255)
                    .HasColumnName("LIQCTL");

                entity.Property(e => e.Liqctlexp)
                    .HasMaxLength(255)
                    .HasColumnName("LIQCTLEXP");

                entity.Property(e => e.Liqdescsold)
                    .HasMaxLength(255)
                    .HasColumnName("LIQDESCSOLD");

                entity.Property(e => e.Liqdrivethru)
                    .HasMaxLength(255)
                    .HasColumnName("LIQDRIVETHRU");

                entity.Property(e => e.Liqempdrink)
                    .HasMaxLength(255)
                    .HasColumnName("LIQEMPDRINK");

                entity.Property(e => e.Liqempexp)
                    .HasMaxLength(255)
                    .HasColumnName("LIQEMPEXP");

                entity.Property(e => e.Liqemptrain)
                    .HasMaxLength(255)
                    .HasColumnName("LIQEMPTRAIN");

                entity.Property(e => e.Liqhostevt)
                    .HasMaxLength(255)
                    .HasColumnName("LIQHOSTEVT");

                entity.Property(e => e.Liqhostexp)
                    .HasMaxLength(255)
                    .HasColumnName("LIQHOSTEXP");

                entity.Property(e => e.Liqhoursold)
                    .HasMaxLength(255)
                    .HasColumnName("LIQHOURSOLD");

                entity.Property(e => e.Liqintcus)
                    .HasMaxLength(255)
                    .HasColumnName("LIQINTCUS");

                entity.Property(e => e.Liqintexp)
                    .HasMaxLength(255)
                    .HasColumnName("LIQINTEXP");

                entity.Property(e => e.Liqintsign)
                    .HasMaxLength(255)
                    .HasColumnName("LIQINTSIGN");

                entity.Property(e => e.Liqlicexp)
                    .HasMaxLength(255)
                    .HasColumnName("LIQLICEXP");

                entity.Property(e => e.Liqlicsup)
                    .HasMaxLength(255)
                    .HasColumnName("LIQLICSUP");

                entity.Property(e => e.Liqphtexp)
                    .HasMaxLength(255)
                    .HasColumnName("LIQPHTEXP");

                entity.Property(e => e.Liqphtid)
                    .HasMaxLength(255)
                    .HasColumnName("LIQPHTID");

                entity.Property(e => e.Liqsignexp)
                    .HasMaxLength(255)
                    .HasColumnName("LIQSIGNEXP");

                entity.Property(e => e.Liqtotsale)
                    .HasMaxLength(255)
                    .HasColumnName("LIQTOTSALE");

                entity.Property(e => e.Liqtrainexp)
                    .HasMaxLength(255)
                    .HasColumnName("LIQTRAINEXP");

                entity.Property(e => e.Lobbytreatyes)
                    .HasMaxLength(255)
                    .HasColumnName("LOBBYTREATYES");

                entity.Property(e => e.Natlstorenbr)
                    .HasMaxLength(255)
                    .HasColumnName("NATLSTORENBR");

                entity.Property(e => e.Newoperatornamefirst)
                    .HasMaxLength(255)
                    .HasColumnName("NEWOPERATORNAMEFIRST");

                entity.Property(e => e.Newoperatornamelast)
                    .HasMaxLength(255)
                    .HasColumnName("NEWOPERATORNAMELAST");

                entity.Property(e => e.Nonmcdops)
                    .HasMaxLength(255)
                    .HasColumnName("NONMCDOPS");

                entity.Property(e => e.OperatorEid)
                    .HasMaxLength(255)
                    .HasColumnName("operator eid");

                entity.Property(e => e.Operatorid)
                    .HasMaxLength(255)
                    .HasColumnName("OPERATORID");

                entity.Property(e => e.Operatornamefirst)
                    .HasMaxLength(255)
                    .HasColumnName("OPERATORNAMEFIRST");

                entity.Property(e => e.Operatornamelast)
                    .HasMaxLength(255)
                    .HasColumnName("OPERATORNAMELAST");

                entity.Property(e => e.Ownershipstart)
                    .HasMaxLength(255)
                    .HasColumnName("OWNERSHIPSTART");

                entity.Property(e => e.Parking)
                    .HasMaxLength(255)
                    .HasColumnName("PARKING");

                entity.Property(e => e.Playland)
                    .HasMaxLength(255)
                    .HasColumnName("PLAYLAND");

                entity.Property(e => e.Playplace)
                    .HasMaxLength(255)
                    .HasColumnName("PLAYPLACE");

                entity.Property(e => e.Seating)
                    .HasMaxLength(255)
                    .HasColumnName("SEATING");

                entity.Property(e => e.Seccamera)
                    .HasMaxLength(255)
                    .HasColumnName("SECCAMERA");

                entity.Property(e => e.Seccameraindoor)
                    .HasMaxLength(255)
                    .HasColumnName("SECCAMERAINDOOR");

                entity.Property(e => e.Seccameraoutdoor)
                    .HasMaxLength(255)
                    .HasColumnName("SECCAMERAOUTDOOR");

                entity.Property(e => e.Secguard)
                    .HasMaxLength(255)
                    .HasColumnName("SECGUARD");

                entity.Property(e => e.Secguardarmed)
                    .HasMaxLength(255)
                    .HasColumnName("SECGUARDARMED");

                entity.Property(e => e.Secguardcontracted)
                    .HasMaxLength(255)
                    .HasColumnName("SECGUARDCONTRACTED");

                entity.Property(e => e.Secguardunarmed)
                    .HasMaxLength(255)
                    .HasColumnName("SECGUARDUNARMED");

                entity.Property(e => e.Sqfoot)
                    .HasMaxLength(255)
                    .HasColumnName("SQFOOT");

                entity.Property(e => e.Storeaddress1)
                    .HasMaxLength(255)
                    .HasColumnName("STOREADDRESS1");

                entity.Property(e => e.Storeaddress2)
                    .HasMaxLength(255)
                    .HasColumnName("STOREADDRESS2");

                entity.Property(e => e.Storebuilt)
                    .HasMaxLength(255)
                    .HasColumnName("STOREBUILT");

                entity.Property(e => e.Storecity)
                    .HasMaxLength(255)
                    .HasColumnName("STORECITY");

                entity.Property(e => e.Storecounty)
                    .HasMaxLength(255)
                    .HasColumnName("STORECOUNTY");

                entity.Property(e => e.Storestcd)
                    .HasMaxLength(255)
                    .HasColumnName("STORESTCD");

                entity.Property(e => e.Storetype)
                    .HasMaxLength(255)
                    .HasColumnName("STORETYPE");

                entity.Property(e => e.Storezip)
                    .HasMaxLength(255)
                    .HasColumnName("STOREZIP");

                entity.Property(e => e.Verfopername)
                    .HasMaxLength(255)
                    .HasColumnName("VERFOPERNAME");

                entity.Property(e => e._24hrop)
                    .HasMaxLength(255)
                    .HasColumnName("24HROP");
            });

            modelBuilder.Entity<_2021Warehouse>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("2021_warehouse", "dbo");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.OperatorEid)
                    .HasMaxLength(255)
                    .HasColumnName("operator eid");

                entity.Property(e => e.Warehouseaddress1)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEADDRESS1");

                entity.Property(e => e.Warehouseaddress2)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEADDRESS2");

                entity.Property(e => e.Warehousebuildingvalue)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEBUILDINGVALUE");

                entity.Property(e => e.Warehousecity)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSECITY");

                entity.Property(e => e.Warehouseconstrutiontype)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSECONSTRUTIONTYPE");

                entity.Property(e => e.Warehousecontents)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSECONTENTS");

                entity.Property(e => e.Warehousecontentvalue)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSECONTENTVALUE");

                entity.Property(e => e.Warehouseifown)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEIFOWN");

                entity.Property(e => e.Warehousemortgages)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEMORTGAGES");

                entity.Property(e => e.Warehousename)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSENAME");

                entity.Property(e => e.Warehouseofficehas)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEOFFICEHAS");

                entity.Property(e => e.Warehouseownrentbuilding)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEOWNRENTBUILDING");

                entity.Property(e => e.Warehousesqft)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSESQFT");

                entity.Property(e => e.Warehousestcd)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSESTCD");

                entity.Property(e => e.Warehouseyearbuilt)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEYEARBUILT");

                entity.Property(e => e.Warehousezip)
                    .HasMaxLength(255)
                    .HasColumnName("WAREHOUSEZIP");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
