﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class ModelCpCoefficient
    {
        public long? Id { get; set; }
        public string Names { get; set; }
        public double? Coefficients { get; set; }
        public double? StdError { get; set; }
        public double? ZValue { get; set; }
        public double? PValue { get; set; }
        public double? StandardizedCoefficients { get; set; }
    }
}
