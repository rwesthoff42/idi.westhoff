﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class ModelResult
    {
        public long? Id { get; set; }
        public long? DtYear { get; set; }
        public long? StoreNumber { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Region { get; set; }
        public long? Zip { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string Aug2020FranchiseeNm { get; set; }
        public long? Aug2020FranchiseeId { get; set; }
        public string CompanyName { get; set; }
        public long? PropLimit { get; set; }
        public long? Aug2020Sales { get; set; }
        public string SalesBucket { get; set; }
        public long? Aug2020Gc { get; set; }
        public double? Aug2020DtsalesPct { get; set; }
        public double? Jul2019DtsalesPct { get; set; }
        public double? Aug2020DeliverySalesPct { get; set; }
        public string DriveThrough { get; set; }
        public string Wifi { get; set; }
        public string MobileDeals { get; set; }
        public string MobileOrders { get; set; }
        public string IndoorPlayground { get; set; }
        public string OutdoorPlayPlaceAvailable { get; set; }
        public double? PropIncurredTotal { get; set; }
        public double? PropTrendedDevelopedUl { get; set; }
        public double? PropTrendedCappedTotal { get; set; }
        public double? PropTrendedCappedDeveloped { get; set; }
        public long? PropClaimCount { get; set; }
        public double? GlIncurredTotal { get; set; }
        public double? GlTrendedDevelopedUl { get; set; }
        public double? GlTrendedCappedTotal { get; set; }
        public double? GlTrendedCappedDeveloped { get; set; }
        public long? GlClaimCount { get; set; }
        public string LocationType { get; set; }
        public string RucaPrimary { get; set; }
        public string RucaSecondary { get; set; }
        public string JudicialProfile { get; set; }
        public double? CommuteTime { get; set; }
        public double? AtheniumHail { get; set; }
        public double? AtheniumSlWind { get; set; }
        public double? AtheniumTornado { get; set; }
        public double? AtheniumWs { get; set; }
        public double? AtheniumIce { get; set; }
        public double? AtheniumRain { get; set; }
        public double? AtheniumHurricane { get; set; }
        public string FloodZone { get; set; }
        public double? Dthours { get; set; }
        public double? StoreHours { get; set; }
        public double? AalHurricane { get; set; }
        public double? AalEq { get; set; }
        public double? PurePremiumLiability { get; set; }
        public double? PurePremiumProperty { get; set; }
        public double? PurePremiumPropertyExclBgii { get; set; }
        public double? PurePremiumPropertyBgii { get; set; }
        public double? PurePremiumPropertyModel { get; set; }
        public double? PropIncToManualExclBgii { get; set; }
        public double? PropCappedToManualExclBgii { get; set; }
        public double? PropIncPlusCatToManual { get; set; }
        public double? GlincToManual { get; set; }
        public double? GlcappedToManual { get; set; }
        public double? AtheniumScore { get; set; }
        public double? GlRate { get; set; }
        public double? PropRate { get; set; }
        public string DtGroup { get; set; }
        public double? AtheniumQuantile { get; set; }
        public long? GlQuantile { get; set; }
        public long? CpQuantile { get; set; }
        public double? PropModel1 { get; set; }
        public double? PropModel1Err { get; set; }
        public double? GlModel1 { get; set; }
        public double? GlModel1Err { get; set; }
        public double? PropModel1Pp { get; set; }
        public double? GlModel1Pp { get; set; }
        public double? PropModelPp { get; set; }
        public double? GlModelPp { get; set; }
        public double? ModelPp { get; set; }
        public double? PropModelPrem { get; set; }
        public double? GlModelPrem { get; set; }
        public double? ModelPrem { get; set; }
        public double? ManualPp { get; set; }
        public double? ModelToManualFactor { get; set; }
        public double? ManualPrem { get; set; }
    }
}
