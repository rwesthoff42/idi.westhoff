﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class OfficeAddressMapping
    {
        public int Id { get; set; }
        public int OfficeId { get; set; }
        public long AddressId { get; set; }
    }
}
