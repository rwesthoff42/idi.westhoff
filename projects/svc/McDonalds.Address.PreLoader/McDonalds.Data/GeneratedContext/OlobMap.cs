﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class OlobMap
    {
        public int Id { get; set; }
        public string OriginalLineOfBusiness { get; set; }
        public string MappedLob { get; set; }
    }
}
