﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class Sitesales2019
    {
        public int NatlStrNu { get; set; }
        public int _5DigitNsn { get; set; }
        public int StSite { get; set; }
        public int OperId { get; set; }
        public string Operator { get; set; }
        public string Feid { get; set; }
        public string CompanyName { get; set; }
        public string Jul19TtmSls { get; set; }
        public string Jul19TtmGcs { get; set; }
        public string Jul19TtmDtSls { get; set; }
        public string Jul19TtmDtGcs { get; set; }
        public double DtDeliverySalesAnnualized { get; set; }
        public string DtDeliverySalesAnnualized1 { get; set; }
        public int DtGuestCountsAnnualized { get; set; }
        public string DtGuestCountsAnnualized1 { get; set; }
    }
}
