﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class StateRegion
    {
        public string Region { get; set; }
        public string State { get; set; }
        public string StateCode { get; set; }
        public string StandardFederalRegion { get; set; }
        public decimal? Ilf250kTo1m { get; set; }
        public string IsostateGroup { get; set; }
    }
}
