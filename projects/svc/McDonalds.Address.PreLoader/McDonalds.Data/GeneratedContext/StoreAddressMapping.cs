﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class StoreAddressMapping
    {
        public int Id { get; set; }
        public int StoreNumber { get; set; }
        public long? AddressId { get; set; }
    }
}
