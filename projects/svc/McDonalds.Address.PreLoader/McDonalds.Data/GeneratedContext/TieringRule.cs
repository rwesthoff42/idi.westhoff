﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class TieringRule
    {
        public int? TierNo { get; set; }
        public decimal? GlTierFactor { get; set; }
        public decimal? CpTierFactor { get; set; }
        public DateTime? EffDate { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
