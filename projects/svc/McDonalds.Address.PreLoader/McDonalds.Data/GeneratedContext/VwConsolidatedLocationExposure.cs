﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class VwConsolidatedLocationExposure
    {
        public int? StoreNumber { get; set; }
        public int HasLocationType { get; set; }
        public int HasAggData2020 { get; set; }
        public int HasAggData2019 { get; set; }
        public int HasAggData2018 { get; set; }
        public int Has2020Sales { get; set; }
        public string SiteTypeDesc { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int? Zip { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Aug2020Sales { get; set; }
        public decimal? Aug2020Gc { get; set; }
        public decimal? Aug2020DtsalesPct { get; set; }
        public decimal? Aug2020DeliverySalesPct { get; set; }
        public string Aug2020FranchiseeNm { get; set; }
        public long? Aug2020FranchiseeId { get; set; }
        public string StoreHours { get; set; }
        public string DriveThruHours { get; set; }
        public string DriveThrough { get; set; }
        public string Wifi { get; set; }
        public string MobileDeals { get; set; }
        public string MobileOrders { get; set; }
        public string IndoorPlayplace { get; set; }
        public string IndoorPlayground { get; set; }
        public string OutdoorPlayPlaceAvailable { get; set; }
        public string IndoorDiningAvailable { get; set; }
        public string GiftCards { get; set; }
        public string WalmartLocation { get; set; }
        public string CreateYourTasteAvailable { get; set; }
        public string Mcdelivery { get; set; }
    }
}
