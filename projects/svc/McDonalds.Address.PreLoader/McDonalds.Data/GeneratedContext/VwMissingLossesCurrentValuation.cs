﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class VwMissingLossesCurrentValuation
    {
        public string KontrolNumber { get; set; }
        public DateTime? LossDate { get; set; }
        public decimal? Incurred2018 { get; set; }
        public int? Count2018 { get; set; }
        public decimal? Incurred2020 { get; set; }
        public int? Count2020 { get; set; }
    }
}
