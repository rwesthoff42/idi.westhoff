﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class VwStoreBeginEnd
    {
        public int? StoreNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int? ZipCode { get; set; }
        public string StoreBeginningDate { get; set; }
        public string StoreEndingDate { get; set; }
    }
}
