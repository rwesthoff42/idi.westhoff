﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class VwStoreYear
    {
        public int? DtYear { get; set; }
        public int? StoreNumber { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Region { get; set; }
        public string IsostateGroup { get; set; }
        public decimal? Ilf250kTo1m { get; set; }
        public int? Zip { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public string Aug2020FranchiseeNm { get; set; }
        public long? Aug2020FranchiseeId { get; set; }
        public string CompanyName { get; set; }
        public decimal? PropLimit { get; set; }
        public decimal? Aug2020Sales { get; set; }
        public string SalesBucket { get; set; }
        public decimal? Aug2020Gc { get; set; }
        public decimal? Aug2020DtsalesPct { get; set; }
        public double? Jul2019DtsalesPct { get; set; }
        public decimal? Aug2020DeliverySalesPct { get; set; }
        public string DriveThrough { get; set; }
        public string Wifi { get; set; }
        public string MobileDeals { get; set; }
        public string MobileOrders { get; set; }
        public string IndoorPlayground { get; set; }
        public string OutdoorPlayPlaceAvailable { get; set; }
        public decimal? PropIncurredTotal { get; set; }
        public decimal? PropTrendedDevelopedUl { get; set; }
        public decimal? PropTrendedCappedTotal { get; set; }
        public decimal? PropTrendedCappedDeveloped { get; set; }
        public int? PropClaimCount { get; set; }
        public decimal? GlIncurredTotal { get; set; }
        public decimal? GlTrendedDevelopedUl { get; set; }
        public decimal? GlTrendedCappedTotal { get; set; }
        public decimal? GlTrendedCappedDeveloped { get; set; }
        public int? GlClaimCount { get; set; }
        public string LocationType { get; set; }
        public string RucaPrimary { get; set; }
        public string RucaSecondary { get; set; }
        public string JudicialProfile { get; set; }
        public decimal? CommuteTime { get; set; }
        public decimal? AtheniumHail { get; set; }
        public decimal? AtheniumSlWind { get; set; }
        public decimal? AtheniumTornado { get; set; }
        public decimal? AtheniumWs { get; set; }
        public decimal? AtheniumIce { get; set; }
        public decimal? AtheniumRain { get; set; }
        public decimal? AtheniumHurricane { get; set; }
        public string FloodZone { get; set; }
        public decimal? Dthours { get; set; }
        public decimal? StoreHours { get; set; }
        public decimal? AalHurricane { get; set; }
        public decimal? AalEq { get; set; }
        public decimal? PurePremiumLiability { get; set; }
        public decimal? PurePremiumProperty { get; set; }
        public decimal? PurePremiumPropertyExclBgii { get; set; }
        public decimal? PurePremiumPropertyBgii { get; set; }
        public decimal? PurePremiumPropertyModel { get; set; }
        public decimal? PropIncToManualExclBgii { get; set; }
        public decimal? PropCappedToManualExclBgii { get; set; }
        public decimal? PropIncPlusCatToManual { get; set; }
        public decimal? GlincToManual { get; set; }
        public decimal? GlcappedToManual { get; set; }
    }
}
