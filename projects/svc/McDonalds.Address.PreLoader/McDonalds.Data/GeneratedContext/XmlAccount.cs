﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class XmlAccount
    {
        public long? Id { get; set; }
        public string CompanyName { get; set; }
        public string DomicileState { get; set; }
        public string OperatorName { get; set; }
        public string StoreCount { get; set; }
        public string OwnershipStart { get; set; }
        public double? OwnerCount { get; set; }
    }
}
