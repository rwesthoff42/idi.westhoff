﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class XmlStorePrepivot
    {
        public string Fn { get; set; }
        public string CompanyName { get; set; }
        public string StoreId { get; set; }
        public string QuestionId { get; set; }
        public string QuestionCode { get; set; }
        public string QuestionAnswer { get; set; }
    }
}
