﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class ZipDatum
    {
        public string Zip { get; set; }
        public string StateCode { get; set; }
        public string ZipType { get; set; }
        public int? Ruca1 { get; set; }
        public string RucaPrimary { get; set; }
        public string RucaSecondary { get; set; }
        public string PlaceName { get; set; }
        public string County { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public string JudicialProfile { get; set; }
        public decimal? CommuteTime { get; set; }
        public decimal? CommuteError { get; set; }
        public decimal? AtheniumHail { get; set; }
        public decimal? AtheniumSlWind { get; set; }
        public decimal? AtheniumTornado { get; set; }
        public decimal? AtheniumWs { get; set; }
        public decimal? AtheniumIce { get; set; }
        public decimal? AtheniumRain { get; set; }
        public decimal? AtheniumHurricane { get; set; }
    }
}
