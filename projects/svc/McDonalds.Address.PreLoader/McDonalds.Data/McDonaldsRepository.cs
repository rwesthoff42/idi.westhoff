﻿using Idi.Mcdonalds.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace McDonalds.Data
{//
    public class McDonaldsRepository
    {
        private readonly McdonaldsAccuarialContext _context;

        public McDonaldsRepository(McdonaldsAccuarialContext context)
        {
            _context = context;
        }

        public async Task AddGoogleData(GoogleAddressDatum data)
        {
            await _context.AddAsync(data);
            await _context.SaveChangesAsync();
        }

        public async Task<List<GoogleAddressDatum>> GetAllGoogleAddressesAsync()
        {
            return await _context.GoogleAddressData.Where(a => !a.AddressId.HasValue).ToListAsync();
        }

        public async Task<Dictionary<int?, (decimal? lat, decimal? lng)>> GetStoreLatLongByStoreNumber2020(IReadOnlyCollection<int?> storeNumbers)
        {
            var stores = await _context.Aggdata20201201s.Where(a => storeNumbers.Contains(a.StoreNumber))
                .ToDictionaryAsync(
                a => a.StoreNumber,
                a => (lat: a.Latitude, lng: a.Longitude));

            return stores;
        }

        public async Task<Dictionary<int?, (decimal? lat, decimal? lng)>> GetStoreLatLongByStoreNumber2019(IEnumerable<int?> storeNumbers)
        {
            var stores = await _context.Aggdata20191201s.Where(a => storeNumbers.Contains(a.StoreNumber))
                .ToDictionaryAsync(
                a => a.StoreNumber,
                a => (lat: a.Latitude, lng: a.Longitude));

            return stores;
        }

        public async Task<List<_2021Store>> GetUnmappedStoresAsync()
        {
            var stores = await _context._2021Stores
                .Where(s => s.Natlstorenbr != null)
                .ToListAsync();

            var mappedStores = await _context.StoreAddressMappings.Select(m => m.StoreNumber).ToListAsync();

            var unMappedStores = stores.Where(s => !mappedStores.Contains(Int32.Parse(s.Natlstorenbr))).ToList();

            return unMappedStores.
                OrderByDescending(s => s.Ownershipstart)
                .GroupBy(s => s.Natlstorenbr)
                .Select(g => g.First()).ToList();
        }

        public async Task<List<_2021Office>> GetUnmappedOfficesAsync()
        {
            var unMappedOffices = await (from o in _context._2021Offices
                                         join m in _context.OfficeAddressMappings on o.Id equals m.OfficeId into leftM
                                         from m2 in leftM.DefaultIfEmpty()
                                         where m2 == null
                                         && o.Contentvalue != null
                                         && o.Buildingvalue != null
                                         select o).ToListAsync();

            return unMappedOffices.Where(o => int.Parse(o.Contentvalue) > 0 && int.Parse(o.Buildingvalue) > 0).ToList();
        }

        public async Task<List<_2021Warehouse>> GetUnmappedWarehousesAsync()
        {
            var unMappedOffices = await (from o in _context._2021Warehouses
                                         join m in _context.WarehouseAddressMappings on o.Id equals m.WarehouseId into leftM
                                         from m2 in leftM.DefaultIfEmpty()
                                         where m2 == null
                                         && o.Warehousecontentvalue != null
                                         && o.Warehousebuildingvalue != null
                                         select o).ToListAsync();

            return unMappedOffices.Where(o => int.Parse(o.Warehousecontentvalue) > 0 && int.Parse(o.Warehousebuildingvalue) > 0).ToList();
        }

        public async Task<List<FloodMapping>> GetAllFloodMappingsAsync(IEnumerable<string> stores)
        {
            return await _context.FloodMappings.Where(f => f.StoreNum != null && stores.Contains(f.StoreNum)).ToListAsync();
        }

        public async Task<bool> AddStoreAddressMappingAsync(string storeNumber, long addressId)
        {
            var mapping = new StoreAddressMapping { AddressId = addressId, StoreNumber = Int32.Parse(storeNumber) };
            await _context.AddAsync(mapping);
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> AddOfficeAddressMappingAsync(int officeId, long addressId)
        {
            var mapping = new OfficeAddressMapping { AddressId = addressId, OfficeId = officeId };
            await _context.AddAsync(mapping);
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> AddWarehouseAddressMappingAsync(int warehouseId, long addressId)
        {
            var mapping = new WarehouseAddressMapping { AddressId = addressId, WarehouseId = warehouseId };
            await _context.AddAsync(mapping);
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<List<(string AddressId, string Hash, string Alias)>> GetWarehouseMappings()
        {
            var query = from m in _context.WarehouseAddressMappings
                        join w in _context._2021Warehouses on m.WarehouseId equals w.Id
                        select new
                        {
                            m.AddressId,
                            w.OperatorEid,
                            w.Warehouseaddress1,
                            w.Id,
                            w.Warehousename

                        };
            var hashes = new List<(string AddressId, string Hash, string Alias)>();

            foreach (var mapping in await query.ToListAsync())
            {
                var parts = mapping.Warehouseaddress1.Split(" ");
                if (parts.Length >= 2)
                {
                    var hash = MD5Hash($"{mapping.OperatorEid}warehouse{parts[0]}{parts[1]}".ToLower().Replace(" ", ""));
                    hashes.Add((mapping.AddressId.ToString(), hash, $"Warehouse - {mapping.OperatorEid} - {mapping.Warehousename.Replace("'", "''")}"));
                }
            }

            return hashes;
        }

        public async Task<List<(string AddressId, string Hash, string Alias)>> GetOfficeMappings()
        {
            var query = from m in _context.OfficeAddressMappings
                        join o in _context._2021Offices on m.OfficeId equals o.Id
                        select new
                        {
                            m.AddressId,
                            o.OperatorEid,
                            o.Officeaddress1,
                            o.Id,
                            o.Officename

                        };
            var hashes = new List<(string AddressId, string Hash, string Alias)>();

            foreach (var mapping in await query.ToListAsync())
            {
                var parts = mapping.Officeaddress1.Split(" ");
                if (parts.Length >= 2)
                {
                    var hash = MD5Hash($"{mapping.OperatorEid}office{parts[0]}{parts[1]}".ToLower().Replace(" ", ""));
                    hashes.Add((mapping.AddressId.ToString(), hash, $"Office - {mapping.OperatorEid} - {mapping.Officename?.Replace("'", "''")}"));
                }
            }

            return hashes;
        }

        private static string MD5Hash(string input)
        {
            using var md5 = MD5.Create();
            var hashBytes = md5.ComputeHash(Encoding.ASCII.GetBytes(input));

            // Convert the byte array to hexadecimal string
            var sb = new StringBuilder();
            foreach (var t in hashBytes)
            {
                sb.Append(t.ToString("X2"));
            }
            return sb.ToString();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
