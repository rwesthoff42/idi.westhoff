function Get-Configuration {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string[]]$Path='.',
        [Parameter()][Switch] $SetEnv=$False
    )

    Push-Location (Resolve-Path $Path)
    try {
        $root = Get-RepositoryRoot
        $artifactRoot = Join-Path $root "artifacts" | Resolve-VirtualPath

        $config = New-Object PSObject -Property @{ 
            ArtifactRoot = $artifactRoot
            BuildOutputPath =  Join-Path $artifactRoot "build"
            ValidateOutputPath =  Join-Path $artifactRoot "validate"
            PackageOutputPath = Join-Path $artifactRoot "publish"
        }

        if (![string]::IsNullOrWhiteSpace($Env:ARTIFACT_ROOT_PATH)) {
            $config.ArtifactRoot = $Env:ARTIFACT_ROOT_PATH
        } elseif ($SetEnv) {
            $Env:ARTIFACT_ROOT_PATH = $config.ArtifactRoot
        }
        if (![string]::IsNullOrWhiteSpace($Env:BUILD_OUTPUT_PATH)) {
            $config.BuildOutputPath = $Env:BUILD_OUTPUT_PATH
        } elseif ($SetEnv) {
            $Env:BUILD_OUTPUT_PATH = $config.BuildOutputPath
        }
        if (![string]::IsNullOrWhiteSpace($Env:VALIDATE_OUTPUT_PATH)) {
            $config.ValidateOutputPath = $Env:VALIDATE_OUTPUT_PATH
        } elseif ($SetEnv) {
            $Env:VALIDATE_OUTPUT_PATH = $config.ValidateOutputPath
        }
        if (![string]::IsNullOrWhiteSpace($Env:PACKAGE_OUTPUT_PATH)) {
            $config.PackageOutputPath = $Env:PACKAGE_OUTPUT_PATH
        } elseif ($SetEnv) {
            $Env:PACKAGE_OUTPUT_PATH = $config.PackageOutputPath
        }

        Write-Verbose "Using configuration $config"
        
        return $config
    } finally {
        Pop-Location
    }
}
