function Get-ConventionalCommits {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)] [string]$CommitRange, 
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string]$Path='.'
    )
    $typeMap = @{
        "feat" = 2
    
        "fix" = 3
        "style" = 3
        "refactor" = 3
        "perf" = 3
        "build" = 3
        "ci" = 3
        "config" = 3
    
        "docs" = 4
        "test" = 4
        "chore" = 4
    }

    $projectRoot = Resolve-Path $Path | 
                        Select-String -Pattern "\bprojects[/\\][^/\\]+[/\\][^/\\]+([/\\]|$)" | 
                        Select-Object -ExpandProperty Matches -First 1 | 
                        Select-Object -ExpandProperty Value

    $commits = Invoke-Expression -Command "git log --format=`">>>%B<<<`" $CommitRange -- $projectRoot" |
        Out-String -OutVariable gitlog |
        Select-String -AllMatches -Pattern "(?smi)>>>(?<type>\w+)(\((?<scope>\w+)\))?(?<BreakingChange>!)?:\s(?<message>.*?)<<<" |
        Select-Object -ExpandProperty Matches | 
        ForEach-Object {New-Object PSObject -Property @{ 
            Type = $_.Groups["type"].Value
            Scope = $_.Groups["scope"].Value
            Message = $_.Groups["message"].Value.Trim()
            BreakingChange = (($_.Groups["BreakingChange"].Value -eq "!") -or ($_.Groups["message"].Value -match "BREAKING CHANGE"))
            BumpType = if ($null -ne $typeMap[$_.Groups["type"].Value]) { $typeMap[$_.Groups["type"].Value] } else { 4 }
            SkipCi = $_.Groups["message"].Value -match "\[skip ci\]"
        }}
    
    return $commits
}