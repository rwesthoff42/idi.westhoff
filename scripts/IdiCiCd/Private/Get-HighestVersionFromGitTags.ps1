<#
    .SYNOPSIS
        Returns detailed information about the latest version tag with the specified prefix.

    .DESCRIPTION
        Examines all tags that match the pattern "<prefix>v<version>" and returns detailed information 
        about the highest version tag available.
#>
function Get-HighestVersionFromGitTags {
    param ([Parameter(Mandatory)] [string]$tagPrefix)
    
    $tagPattern = "(?<hash>[\w\d]+)\s(?<tag>$($tagPrefix -replace "\-","\-")v(?<version>(?<major>\d+)(\.(?<minor>\d+))?(\.(?<patch>\d+))?(\-(?<pre>[0-9A-Za-z\-\.]+))?(\+(?<build>[0-9A-Za-z\-\.]+))?))$"

    Write-Verbose "Searching for version tags with pattern: $tagPattern"
    
    git fetch origin 'refs/tags/*:refs/tags/*'
    $version = git tag --list --format '%(objectname) %(refname:short)' | 
                Select-String -Pattern $tagPattern |
                Select-Object -ExpandProperty Matches | 
                ForEach-Object {New-Object PSObject -Property @{ 
                    Major = [int]$_.Groups["major"].Value
                    Minor = [int]$_.Groups["minor"].Value
                    Patch = [int]$_.Groups["patch"].Value
                    Pre = $_.Groups["pre"].Value
                    Build = [int]$_.Groups["build"].Value
                    Version = $_.Groups["version"].Value
                    Hash = $_.Groups["hash"].Value
                    Tag = $_.Groups["tag"].Value
                }} |
                Sort-Object Major,Minor,Patch,Pre,Build |
                Select-Object -Last 1
                
    return $version
}
