<#
    .SYNOPSIS
        Returns the full path to the root of GIT repository for the current or given path.
#>
function Get-RepositoryRoot {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string[]]$Path='.'
    )
    Push-Location (Resolve-Path $Path)
    try {
        return git rev-parse --show-toplevel
    } finally {
        Pop-Location
    }
}