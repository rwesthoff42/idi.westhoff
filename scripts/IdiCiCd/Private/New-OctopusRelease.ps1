function New-OctopusRelease {
    [CmdletBinding()]
    param (
        [string]$OctopusUrl,
        [string]$ApiKey,
        [string]$ProjectName,
        [string]$ChannelName,
        [string]$Version
    )

    $expandedVersion = Expand-VersionNumber $Version

    $header = @{ "X-Octopus-ApiKey" = $ApiKey }
    $project = (Invoke-RestMethod -Method Get -Uri "$OctopusUrl/api/projects/all" -Headers $header) | Where-Object { $_.Name -eq $ProjectName }
    $channel = (Invoke-RestMethod -Method Get -Uri "$OctopusUrl/api/projects/$($project.Id)/channels" -Headers $header).Items | Where-Object { $_.Name -eq $ChannelName }

    $releaseBody = @{
        ChannelId        = $channel.Id
        ProjectId        = $project.Id
        Version          = $expandedVersion.Version
        SelectedPackages = @()
    }

    $template = Invoke-RestMethod -Uri "$OctopusUrl/api/deploymentprocesses/deploymentprocess-$($project.id)/template?channel=$($channel.Id)" -Headers $header
    $template.Packages | ForEach-Object {
        $packageVersion = $expandedVersion.NugetVersion
        $cypressSearch = "cypress"
        if ($_.StepName.ToLower().Contains($cypressSearch) -or $_.ActionName.ToLower().Contains($cypressSearch)) {        
            $feedId = $_.FeedId
            $packageId = $_.PackageId
            $latestVersion = (Invoke-RestMethod -Uri "$OctopusUrl/api/feeds/$feedId/packages/versions?packageId=$packageId&take=1" -Headers $header).Items[0].Version
            $packageVersion = $latestVersion
        }
       
            $releaseBody.SelectedPackages += @{
                ActionName           = $_.ActionName
                PackageReferenceName = $_.PackageReferenceName
                Version              = $packageVersion
            
        }
    }
    Write-Host "Selected Packages"
    Write-Host ($releaseBody.SelectedPackages | ConvertTo-Json -depth 10)
    return Invoke-RestMethod -Uri "$OctopusUrl/api/releases" -Method POST -Headers $header -Body ($releaseBody | ConvertTo-Json -depth 10)
}
