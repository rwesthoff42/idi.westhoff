<#
    .SYNOPSIS
        Reads the version number from project files and returns it
#>
function Get-TeamCityBuildNumber {
    param (
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string[]]$Path='.',
        [Parameter(Mandatory)] [string]$Branch, 
        [Parameter(Mandatory)] [string]$BuildCounter
    )

    $projectRoot = Get-ProjectFolder $Path

    $version = Get-ProjectVersion $projectRoot
    if ($null -eq $version) {
        $baseVersion = "1.0.0"
    } else {
        $baseVersion = @($version.Major,$version.Minor,$version.Patch) -join "."
    }

    $preRelease = Get-PrereleaseFlag $Branch

    if (![string]::IsNullOrEmpty($BuildCounter)) {
        $BuildCounter = "+$BuildCounter"
    }

    return Expand-VersionNumber "$baseVersion$preRelease$BuildCounter"
}
