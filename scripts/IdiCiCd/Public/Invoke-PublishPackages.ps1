function Invoke-PublishPackages {
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string[]]$Path='.',
        [Parameter(Mandatory=$False)] [string]$OctopusProjectName,
        [Parameter(Mandatory=$False)] [string]$OctopusUrl,
        [Parameter(Mandatory=$False)] [string]$OctopusApiKey,
        [Parameter(Mandatory=$False)] [string]$OctopusChannel
    )

    $config = Get-Configuration $Path -SetEnv
    
    Push-Location (Resolve-Path $Path)
    try {
        if (Test-Path "prepublish.ps1" -PathType Leaf) {
            if ($PSCmdlet.ShouldProcess("Invoking pre-publish script prepublish.ps1 with args $args", "TARGET", "OPERATION")) {
                .".\prepublish.ps1" @args
            }
        }

        if (Test-Path "publish.ps1" -PathType Leaf) {
            if ($PSCmdlet.ShouldProcess("Invoking publish script publish.ps1 with args $args", "TARGET", "OPERATION")) {
                .".\publish.ps1" @args
            }
        } else {
            $octoDeploy = ![string]::IsNullOrWhiteSpace($OctopusProjectName)
            $hasPublishedToOctopus = $False

            Get-ChildItem $config.PackageOutputPath -ErrorAction Ignore | Where-Object { $_.BaseName -notmatch "\-local[\.\+]" } | ForEach-Object {
                $fileInfo = $_
                $isNpmPkg = $False

                if ($_ -is [System.IO.DirectoryInfo]) {
                    $isNpmPkg = Join-Path $_.FullName "package.json" | Test-Path
                    $zipPath = Join-Path $config.ArtifactRoot "$($_.Name).zip"
                    if (-not(Test-Path $zipPath)) {
                        Compress-Archive -Path (Join-Path $_.FullName "*") -DestinationPath $zipPath -Force
                    }
                    $fileInfo = Get-Item $zipPath
                } elseif ($fileInfo.Extension -match "\.(tgz)$") {
                    $isNpmPkg = $True
                }

                if ($isNpmPkg) { # npm package
                    Write-Host "Not yet publishing npm packages $($fileInfo.FullName)..."

                } elseif ($octoDeploy -and ($fileInfo.Extension -match "\.(nupkg|zip)$")) { # nuget package or published app assets
                    if ($PSCmdlet.ShouldProcess("Push file $($fileInfo.FullName) to octopus server $OctopusUrl", "TARGET", "OPERATION")) {
                        Publish-ToOctopus -FilePath $fileInfo.FullName -OctopusUrl $OctopusUrl -ApiKey $OctopusApiKey
                    }
                    $hasPublishedToOctopus = $True
                } else {
                    Write-Warning "Unsupported package type or no octopus target provided. Unable to publish $($fileInfo.FullName)"
                }
            }

            if (Test-Path "postpublish.ps1" -PathType Leaf) {
                if ($PSCmdlet.ShouldProcess("Invoking post-publish script postpublish.ps1 with args $args", "TARGET", "OPERATION")) {
                    .".\postpublish.ps1" @args
                }
            }

            if ($octoDeploy) {
                if (!$hasPublishedToOctopus) {
                    Write-Host "No updated packages published to octopus. Skip creating new Octopus release."
                } elseif ($PSCmdlet.ShouldProcess("Create new release for $OctopusProjectName in octopus server $OctopusUrl", "TARGET", "OPERATION")) {
                    $release = New-OctopusRelease -ProjectName $OctopusProjectName -OctopusUrl $OctopusUrl -ApiKey $OctopusApiKey -ChannelName $OctopusChannel -Version (Get-ProjectVersion).Version
                    Write-Host "Created new octopus release $($release.Id)@$($release.Version): $OctopusUrl$($release.Links.Web)"
                }
            }
        }
    } finally {
        Pop-Location
    }
}
