<#
    .SYNOPSIS
        Validates a project by running tests
#>
function Invoke-Validate {
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string[]]$Path='.',
        [Switch]$SkipPackageRestore=$False,
        [string]$BuildConfiguration="Release"
    )

    $config = Get-Configuration $Path -SetEnv

    Push-Location (Resolve-Path $Path)
    try {
        if (Test-Path ".scripts\prevalidate.ps1" -PathType Leaf) {
            if ($PSCmdlet.ShouldProcess("Invoking pre-validation script prevalidate.ps1", "TARGET", "OPERATION")) {                
                .".scripts\prevalidate.ps1"
            }
        }

        if (Test-Path ".scripts\validate.ps1" -PathType Leaf) {
            if ($PSCmdlet.ShouldProcess("Invoking validation script validate.ps1", "TARGET", "OPERATION")) {                
                .".scripts\validate.ps1"
            }
        } elseif (Test-Path "package.json" -PathType Leaf) {
            if ($PSCmdlet.ShouldProcess("Invoking NPM test", "TARGET", "OPERATION")) {
                if (!$SkipPackageRestore) {
                    npm ci
                }
                npm run validate
            }
        } elseif (Test-Path "*.sln") {
            $solutionFiles = Get-Item "*.sln"
            
            foreach ($solutionFile in $solutionFiles) {
                if ($PSCmdlet.ShouldProcess("Invoking .NET test on $solutionFile", "TARGET", "OPERATION")) {
                    $testOutputFilename = (Join-Path $config.ValidateOutputPath $solutionFile.BaseName) + ".trx"
                    
                    if (!$SkipPackageRestore) {
                        dotnet restore $solutionFile --configfile (Resolve-Path (Join-Path (Get-RepositoryRoot) "nuget.config"))
                    }
                    dotnet test $solutionFile.FullName --no-restore --configuration $BuildConfiguration --logger "trx;LogFileName=$testOutputFilename" --collect "Code Coverage"
                    Get-Item "**\TestResults\**\*.coverage" | Move-Item -Destination $config.ValidateOutputPath -Force
                }
            }
        } elseif (Test-Path "main.tf") {
            Install-Terraform

            New-Item $config.ValidateOutputPath -ItemType Directory -ErrorAction Ignore

            $projectName = (Get-Item -Path ".\").BaseName
            $outPath = (Join-Path $config.ValidateOutputPath "$projectName.pmd")

            terraform init -backend=false
            terraform validate -json | Out-String | Convert-TfJsonToPmdXml | Set-Content -Path $outPath
        } else {
            Write-Host "Unsupported project type."
        }

        if (Test-Path ".scripts\postvalidate.ps1" -PathType Leaf) {
            if ($PSCmdlet.ShouldProcess("Invoking post-validation script postvalidate.ps1", "TARGET", "OPERATION")) {                
                .".scripts\postvalidate.ps1"
            }
        }
    } finally {
        Pop-Location
    }
}
