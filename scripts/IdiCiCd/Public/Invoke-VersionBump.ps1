<#
    .SYNOPSIS
        Determines appropriate version bump, sets file versions, commits updates and applies version tag.
#>
function Invoke-VersionBump {
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string[]]$Path='.', 
        [Parameter(Mandatory)] [string]$GitUsername,   
        [Switch]$Force=$False
    )

    $projectRoot = Get-ProjectFolder $Path
    $projectId = Get-ProjectId $Path
    if ([string]::IsNullOrEmpty($projectId)) {
        Write-Host "Unable to determine project id. Path must be a project folder."
        return 
    }

    $projectTagPrefix = "$projectId-"

    Write-Verbose "Looking for latest version tag with prefix '$projectTagPrefix'..."
    $latestVersion = Get-HighestVersionFromGitTags $projectTagPrefix

    if ($null -eq $latestVersion) {
        Write-Host "No existing version tag found with prefix '$projectTagPrefix'. Defaulting to version 1.0.0"
        $newVersion = "1.0.0"
    } else {
        Write-Host "Found last version tag: $latestVersion"
        Write-Host "Analyzing commits since $($latestVersion.Hash) for '$projectRoot'..."

        $commits = Get-ConventionalCommits -Path "$projectRoot" -CommitRange "$($latestVersion.Hash)..HEAD"

        $releaseBumpType = 4
        $commits |
            ForEach-Object {
                if ($_.BreakingChange) {
                    $releaseBumpType = 1
                } elseif ($_.SkipCi) {
                    # Skip this one for detecting version bump
                } elseif ($_.BumpType -lt $releaseBumpType) {
                    $releaseBumpType = $_.BumpType
                }
            }
    
        $bumpTypes = @{ 1 = "Major"; 2 = "Minor"; 3 = "Patch"; 4 = "Build"; }
        $bump = New-Object PSObject -Property @{
            Ordinal = $releaseBumpType
            Type = $bumpTypes[$releaseBumpType]
        }

        Write-Host "Conventional commits indicate $($bump.Type) version bump is required."

        if ($bump.Ordinal -le 1) { 
            $versionParts = ($latestVersion.Major+1),0,0
        } elseif ($bump.Ordinal -le 2) {
            $versionParts = $latestVersion.Major,($latestVersion.Minor+1),0
        } elseif (($bump.Ordinal -le 3) -or $Force) {
            $versionParts = $latestVersion.Major,$latestVersion.Minor,($latestVersion.Patch+1)
        } else {
            #$versionParts = $latestVersion.Major,$latestVersion.Minor,$latestVersion.Patch
            Write-Host "Tagging is disabled for Build level version bump. Skipping."
            return
        }

        $newVersion = $versionParts -join "."
    }
    $newVersionTag = "${projectTagPrefix}v${newVersion}"

    Set-ProjectVersion -Path $Path -Version "$newVersion" -WhatIf:$WhatIfPreference
    Add-ChangelogEntry -Path $Path -Version "$newVersion" -Commits $commits -WhatIf:$WhatIfPreference

    if ($PSCmdlet.ShouldProcess("Committing version update and tagging release $newVersionTag", "GIT", "commit and tag")) {
        Write-Host "Committing version update and tagging release $newVersionTag"
        git config user.name $GitUsername
        git config user.email "$($GitUsername)@wrbts.ads.wrberkley.com"
        Write-Host "$(git config user.name) - $(git config user.email)"
        git pull
        git tag $newVersionTag
        git push --tags
        git add -A
        git commit -am "chore(release): $projectRoot $newVersion [skip ci]"
        git push
    }
}
