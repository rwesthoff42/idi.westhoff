<#
    .SYNOPSIS
        Tells TeamCity to publish the given file as a build artifact
#>
function Publish-ArtifactsToTeamCity {

    $config = Get-Configuration
    $published = @()

    @($config.BuildOutputPath,$config.ValidateOutputPath,$config.PackageOutputPath) | ForEach-Object {
        Get-ChildItem $_ -ErrorAction Ignore | ForEach-Object {

            if ($_ -is [System.IO.DirectoryInfo]) {
                $zipPath = Join-Path $config.ArtifactRoot "$($_.Name).zip"
                Compress-Archive -Path (Join-Path $_.FullName "*") -DestinationPath $zipPath -Force
                Write-Host "##teamcity[publishArtifacts '$zipPath']"
                $published += $zipPath
            } elseif ($_.Extension -eq ".coverage") {
                Write-Host "##teamcity[importData type='dotNetCoverage' path='$($_.FullName)' parseOutOfDate='true']"
            } elseif ($_.Extension -eq ".trx") {
                Write-Host "##teamcity[importData type='vstest' path='$($_.FullName)' parseOutOfDate='true']"
            } elseif ($_.Extension -match "^\.(checkstyle|junit|surefire|nunit|checkstyle|jslint|pmd)$") {
                Write-Host "##teamcity[importData type='$($Matches[1])' path='$($_.FullName)' parseOutOfDate='true']"
            } elseif ($_ -is [System.IO.FileInfo]) {
                if ($published -notcontains $_.FullName) {
                    Write-Host "##teamcity[publishArtifacts '$($_.FullName)']"
                    $published += $_.FullName
                }
            }

        }
    }
    
}
