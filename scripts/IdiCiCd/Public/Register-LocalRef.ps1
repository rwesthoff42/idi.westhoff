<#
    .SYNOPSIS
        Installs a local reference for the target project.
#>
function Register-LocalRef {
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter()] [string] $TargetProject="Idi\.[^""]+",
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string[]]$Path='.'
    )

    $config = Get-Configuration $Path
    $repoRoot = Get-RepositoryRoot -Path $Path
    $libraryRoot = Join-Path $repoRoot "projects\lib"
    $packageFolder = $config.PackageOutputPath

    Push-Location (Resolve-Path $Path)
    try {
        $projectFiles = Get-Item "**\*.csproj"
        foreach ($projectFile in $projectFiles) {
            Write-Verbose "Processing $projectFile for references..."

            $originalProjectFileContents = Get-Content $projectFile

            $idiProjectRefs =  $originalProjectFileContents |
                    Select-String "<PackageReference Include=""(?<project>$TargetProject)"".*?Version=""(?<version>[^""]+)""" |
                    Select-Object -ExpandProperty Matches | 
                    ForEach-Object {New-Object PSObject -Property @{ 
                        Name = $_.Groups["project"].Value
                        Version = (Expand-VersionNumber $_.Groups["version"].Value)
                    }}
        
            if ($idiProjectRefs.Count -eq 0) {
                Write-Verbose "No matching project references found to update."
            }

            $refBackupFilePath = Join-Path "tmp" "$($projectFile.BaseName).json" | Resolve-VirtualPath
            $refBackup = @{}
            if (Test-Path $refBackupFilePath) {
                (Get-Content -Path $refBackupFilePath -Raw | ConvertFrom-Json).psobject.properties | ForEach-Object { $refBackup[$_.Name] = $_.Value }
            }

            foreach ($idiRef in $idiProjectRefs) {
                if ($idiRef.Version.Pre -notmatch "local") {
                    Write-Verbose "Tracking existing package reference $($idiRef.Name) $($idiRef.Version.Version)"
                    $refBackup[$idiRef.Name] = $idiRef.Version.Version
                }

                $verMaj = $idiRef.Version.Major
                $verMin = $idiRef.Version.Minor
                $verPatch = $idiRef.Version.Patch
                $verPre = "local"
                $verBuild = Get-Date -Format "mmss"

                if ($verPatch -eq "*") {
                    $verPatch = [int]::MaxValue
                }
                if ($verMin -eq "*") {
                    $verMin = [int]::MaxValue
                }

                $projectRefPath = Join-Path $libraryRoot $idiRef.Name -Resolve

                Write-Verbose "Removing existing local packages..."
                if (Test-Path $packageFolder) {
                    Get-ChildItem $packageFolder |
                            Where-Object{ $_.Name -Match "^$($idiRef.Name -replace "\.","\." -replace "\-","\-")\.[\.\d]*-local.*\.nupkg$"} |
                            Remove-Item -WhatIf:$WhatIfPreference
                }

                $oldVersion = Get-ProjectVersion $projectRefPath
                $tempVersion = "$verMaj.$verMin.$verPatch-$verPre+$verBuild"
                Write-Verbose "Building and referencing $($idiRef.Name) as version $tempVersion"

                Set-ProjectVersion -Version $tempVersion -Path $projectRefPath -WhatIf:$WhatIfPreference

                Invoke-Package -Path $projectRefPath -WhatIf:$WhatIfPreference

                Set-ProjectVersion -Version $oldVersion.Version -Path $projectRefPath -WhatIf:$WhatIfPreference

                if ($PSCmdlet.ShouldProcess("Adding project reference for $($idiRef.Name) to $projectFile", "TARGET", "OPERATION")) {
                    dotnet add $projectFile package $idiRef.Name --source $packageFolder --prerelease --interactive
                    #$originalProjectFileContents | Set-Content -Path $projectFile
                }
            }

            if ($PSCmdlet.ShouldProcess("Tracking which referenced package versions are being replaced for $projectFile", "TARGET", "OPERATION")) {
                New-Item $refBackupFilePath -ItemType File -Force -ErrorAction Ignore | Out-Null
                $refBackup | ConvertTo-Json | Set-Content -Path $refBackupFilePath -Encoding UTF8 -Force
            }

        }
    } finally {
        Pop-Location
    }
}
