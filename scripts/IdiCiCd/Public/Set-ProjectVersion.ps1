<#
    .SYNOPSIS
        Updates all project files with the provided version number.
#>
function Set-ProjectVersion {
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string[]]$Path='.',
        [Parameter(Mandatory)]$Version
    )

    try {
        Push-Location (Resolve-Path $Path)

        $parsedVersion = Expand-VersionNumber $Version
        $nugetVersion = $parsedVersion.NugetVersion
        $safeVersion = $parsedVersion.AssemblyVersion

        Write-Verbose "Setting project version $Version (safe assembly version $safeVersion, compatible nuget version $nugetVersion)..."

        $files = Get-ChildItem "package.json" -Recurse | Where-Object { $_.FullName -notmatch "[\\/](node_modules)[\\/]" }
        foreach ($file in $files) {
            Write-Verbose "Updating version number in project file $file"

            $content = ((Get-Content $file.FullName -Raw) -Replace '"version":\s*"[^"]*"',"""version"": ""$Version""").Trim()
            $content | Set-Content -Encoding ASCII -Path $file.FullName -WhatIf:$WhatIfPreference
        }

        $files = Get-ChildItem "AssemblyInfo.cs" -Recurse | Where-Object { $_.FullName -notmatch "[\\/](bin|obj)[\\/]" }
        foreach ($file in $files) {
            Write-Verbose "Updating version number in project file $file"
            $fileContent = (Get-Content $file.FullName -Raw).Trim()

            if ($fileContent -match "(?m)^\[assembly\:\s*AssemblyVersion\(""[^""]*""\)\]") {
                $fileContent = $fileContent -replace "(?m)^\[assembly\:\s*AssemblyVersion\(""[^""]*""\)\]","[assembly: AssemblyVersion(""$safeVersion"")]"
            } else {
                $fileContent += "`n[assembly: AssemblyVersion(""$safeVersion"")]"
            }
            if ($fileContent -match "(?m)^\[assembly\:\s*AssemblyFileVersion\(""[^""]*""\)\]") {
                $fileContent = $fileContent -replace "(?m)^\[assembly\:\s*AssemblyFileVersion\(""[^""]*""\)\]","[assembly: AssemblyFileVersion(""$safeVersion"")]"
            } else {
                $fileContent += "`n[assembly: AssemblyFileVersion(""$safeVersion"")]"
            }
            if ($fileContent -match "(?m)^\[assembly\:\s*AssemblyInformationalVersion\(""[^""]*""\)\]") {
                $fileContent = $fileContent -replace "(?m)^\[assembly\:\s*AssemblyInformationalVersion\(""[^""]*""\)\]","[assembly: AssemblyInformationalVersion(""$nugetVersion"")]"
            } else {
                $fileContent += "`n[assembly: AssemblyInformationalVersion(""$nugetVersion"")]"
            }

            $fileContent | Set-Content -Encoding UTF8 -Path $file.FullName -WhatIf:$WhatIfPreference
        }

        $files = Get-ChildItem "*.csproj" -Recurse | Where-Object { $_.FullName -notmatch "[\\/](bin|obj)[\\/]" }
        foreach ($file in $files) {
            Write-Verbose "Updating version number in project file $file"
            $xml = [xml](Get-Content $file.FullName)

            $propertyGroupNode = $xml.SelectSingleNode("/Project/PropertyGroup")

            $versionNode = $propertyGroupNode.SelectSingleNode("Version")
            if ($versionNode -eq $null) {
                $versionNode = $xml.CreateElement("Version")
                $propertyGroupNode.AppendChild($versionNode) | Out-Null
            }
            $versionNode.InnerText = $nugetVersion

            $assemblyVersionNode = $propertyGroupNode.SelectSingleNode("AssemblyVersion")
            if ($assemblyVersionNode -eq $null) {
                $assemblyVersionNode = $xml.CreateElement("AssemblyVersion")
                $propertyGroupNode.AppendChild($assemblyVersionNode) | Out-Null
            }
            $assemblyVersionNode.InnerText = $safeVersion

            $fileVersionNode = $propertyGroupNode.SelectSingleNode("FileVersion")
            if ($fileVersionNode -eq $null) {
                $fileVersionNode = $xml.CreateElement("FileVersion")
                $propertyGroupNode.AppendChild($fileVersionNode) | Out-Null
            }
            $fileVersionNode.InnerText = $safeVersion

            if ($PSCmdlet.ShouldProcess($file.FullName, "Save")) {
                $xml.Save($file.FullName)
            }
        }

        if (Test-Path "version.tf") {
            $file = Get-Item "version.tf"
        } elseif (Test-Path "main.tf") {
            $file = Get-Item "main.tf"
        } else {
            $file = $null
        }
        if ($null -ne $file) {
            Write-Verbose "Updating version number in project file $file"
            $fileContent = (Get-Content $file.FullName -Raw).Trim()
            
            if ($fileContent -match "(?ms)^(output\s+""version""\s+{.*?value\s*=\s*"")(?<version>[^""]*)("".*?})") {
                $fileContent = $fileContent -replace "(?ms)^(output\s+""version""\s+{.*?value\s*=\s*"")(?<version>[^""]*)("".*?})","`${1}$nugetVersion`${2}"
            } else {
                $fileContent += "`noutput ""version"" {`n  value = ""$nugetVersion""`n}`n`n"
            }

            $fileContent | Set-Content -Encoding ASCII -Path $file.FullName -WhatIf:$WhatIfPreference
        }
    } finally {
        Pop-Location
    }
}
